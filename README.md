This project is a MVP of distributed e-shop built in rust

# Running e-shop
1. Install sqlite3
2. Change directory to e-shop
3. Run `cargo b` and wait for all dependencies to install and compile
4. Open directories `http` and `web` in separate terminals
5. Run `cargo r` in `http` directory, and then in `web` directory
6. Open browser and go to localhost:3000

# Features
- Separate frontend and backend services communicating via remote actors
- Products in categories
- Product, catalog and category pages
- User registration, login and purchase history
- Product search
- Admin account (default login is admin, password is 12345)
- Admin panel (available at /manage URL)
- Home page with latest products and top categories
- Cart and "checkout"
