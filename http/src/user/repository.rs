use async_trait::async_trait;
use e_shop_core::access::model::UserCredentials;
use e_shop_core::user::model::User;
use rusqlite::{params, Row};
use std::sync::Arc;
use tokio_rusqlite::Connection;
use typesafe_repository::prelude::*;
use typesafe_repository::GetIdentity;
use typesafe_repository::{ValidIdentity, ValidIdentityProvider};
use typesafe_repository::async_ops::{Add, Get};

pub trait UserRepository: Repository<User> + Add<User> + Get<User> {}

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub struct SqliteUserRepository {
    conn: Arc<Connection>,
}

impl SqliteUserRepository {
    pub async fn new(conn: Arc<Connection>) -> Result<Self, Error> {
        conn.call(|conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS users (
                    id UUID PRIMARY KEY,
                    credentials UUID NOT NULL
                );",
                (),
            )
        })
        .await?;
        Ok(Self { conn })
    }
}

#[async_trait]
impl Add<User> for SqliteUserRepository {
    async fn add(&self, u: User) -> Result<(), Self::Error> {
        let id = u.id();
        let credentials = u.credentials;
        self.conn
            .call(move |conn| {
                conn.execute(
                    "INSERT INTO users (id, credentials) VALUES (?1, ?2)",
                    params![id, credentials.into_inner()],
                )
            })
            .await?;
        Ok(())
    }
}

struct IndexedRow<'a>(&'a Row<'a>, usize);

impl<'a> ValidIdentityProvider<UserCredentials> for IndexedRow<'a> {
    fn get(&self) -> IdentityOf<UserCredentials> {
        self.0.get(self.1).unwrap()
    }
}

fn user_from_row(row: &Row) -> Result<User, rusqlite::Error> {
    Ok(User::new(
        row.get(0)?,
        ValidIdentity::from(&IndexedRow(row, 1)),
        vec![],
    ))
}

#[async_trait]
impl Get<User> for SqliteUserRepository {
    async fn get_one(&self, k: &IdentityOf<User>) -> Result<Option<User>, Self::Error> {
        let k = *k;
        let result = self
            .conn
            .call(move |conn| {
                conn.query_row(
                    "SELECT id, credentials FROM users WHERE id = ?1",
                    [k],
                    user_from_row,
                )
            })
            .await;
        if let Err(err) = &result {
            log::error!("Unable to get user:\n{err:?}");
        }
        Ok(result.ok())
    }
}

impl Repository<User> for SqliteUserRepository {
    type Error = Error;
}

impl UserRepository for SqliteUserRepository {}
