use super::repository::UserRepository;
use actix::{Actor, Context, Handler, Message, ResponseFuture};
use actix_broker::{Broker, SystemBroker};
use actix_service::{messages, ServiceError};

use e_shop_core::user::{messages::*, model::User};
use remote::user::messages::*;
use std::sync::Arc;
use typesafe_repository::IdentityOf;

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub struct UserService {
    repository: Box<Arc<dyn UserRepository<Error = Error>>>,
}

impl UserService {
    pub fn new<T>(r: Arc<T>) -> Self
    where
        T: UserRepository<Error = Error> + 'static,
    {
        Self {
            repository: Box::new(r),
        }
    }
}

impl Actor for UserService {
    type Context = Context<Self>;
}

impl Handler<ExternalGetMessage> for UserService {
    type Result = ResponseFuture<()>;
    fn handle(&mut self, msg: ExternalGetMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let res = repository.get_one(&msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalGetResponse>(
                (msg.0, GetResponse(res)).into(),
            );
        })
    }
}

impl Handler<ExternalAddMessage> for UserService {
    type Result = ResponseFuture<()>;
    fn handle(&mut self, msg: ExternalAddMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            repository.add(msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalAddResponse>((msg.0, AddResponse).into());
        })
    }
}

pub mod messages {
    use super::*;

    messages! {
        UserService,
        mod user -> User,
        Get(IdentityOf<User>) -> Option<T> {
            fn handle(&mut self, Get(id): Get, _ctx: &mut Self::Context) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.get_one(&id).await?)
                })
            }
        },
        Add(User) {
            fn handle(&mut self, Add(user): Add, _: &mut Self::Context) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.add(user).await?)
                })
            }
        }
    }
}
