use super::repository::CategoryRepository;
use actix::{Actor, Context, Handler, Message, ResponseFuture};
use actix_broker::{Broker, SystemBroker};
use actix_service::{messages, ServiceError};
use e_shop_core::catalog::messages::{
    AddResponse, GetResponse, ListResponse, RemoveResponse, UpdateResponse,
};
use e_shop_core::catalog::model::Category;
use remote::catalog::messages::*;
use std::sync::Arc;
use typesafe_repository::IdentityOf;

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub struct CategoryService {
    repository: Box<Arc<dyn CategoryRepository<Error = Error>>>,
}

impl CategoryService {
    pub fn new<T>(r: Arc<T>) -> Self
    where
        T: CategoryRepository<Error = Error> + 'static,
    {
        Self {
            repository: Box::new(r),
        }
    }
}

impl Actor for CategoryService {
    type Context = Context<Self>;
}

impl Handler<ExternalGetMessage> for CategoryService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalGetMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let category = repository.get_one(&msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalGetResponse>(
                (msg.0, GetResponse(category)).into(),
            );
        })
    }
}

impl Handler<ExternalListMessage> for CategoryService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalListMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let categories = repository.list().await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalListResponse>(
                (msg.0, ListResponse(categories)).into(),
            );
        })
    }
}

impl Handler<ExternalUpdateMessage> for CategoryService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalUpdateMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            repository.update(msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalUpdateResponse>(
                (msg.0, UpdateResponse()).into(),
            );
        })
    }
}

impl Handler<ExternalAddMessage> for CategoryService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalAddMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            repository.add(msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalAddResponse>(
                (msg.0, AddResponse()).into(),
            );
        })
    }
}

impl Handler<ExternalRemoveMessage> for CategoryService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalRemoveMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            repository.take(&msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalRemoveResponse>(
                (msg.0, RemoveResponse()).into(),
            );
        })
    }
}

pub mod messages {
    use super::*;

    messages! {
        CategoryService,
        mod category -> Category,
        Get(IdentityOf<Category>) -> Option<T> {
            fn handle(&mut self, Get(id): Get, _: &mut Self::Context) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.get_one(&id).await?)
                })
            }
        }
        List -> Vec<T> {
            fn handle(&mut self, _: List, _: &mut Self::Context) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.list().await?)
                })
            }
        }

    }
}
