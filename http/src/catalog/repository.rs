use async_trait::async_trait;
use e_shop_core::catalog::model::Category;
use rusqlite::{params, Row};
use std::sync::Arc;
use tokio_rusqlite::Connection;
use typesafe_repository::prelude::*;
use typesafe_repository::async_ops::{Add, List, Get, Take, Update};

pub trait CategoryRepository:
    Repository<Category>
    + Add<Category>
    + List<Category>
    + Get<Category>
    + Take<Category>
    + Update<Category>
{
}

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub struct SqliteCategoryRepository {
    conn: Arc<Connection>,
}

impl SqliteCategoryRepository {
    pub async fn new(conn: Arc<Connection>) -> Result<Self, Error> {
        conn.call(|conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS categories (
                    id UUID PRIMARY KEY,
                    name TEXT NOT NULL,
                    description TEXT NOT NULL,
                    image TEXT
                );",
                (),
            )
        })
        .await?;
        Ok(Self { conn })
    }
}

#[async_trait]
impl Add<Category> for SqliteCategoryRepository {
    async fn add(&self, c: Category) -> Result<(), Self::Error> {
        let image = c.image.and_then(|i| i.to_str().map(|s| s.to_string()));
        self.conn
            .call(move |conn| {
                conn.execute(
                    "INSERT INTO categories (id, name, description, image) VALUES (?1, ?2, ?3, ?4)",
                    params![c.id, c.name.into_inner(), c.description.into_inner(), image],
                )
            })
            .await?;
        Ok(())
    }
}
pub fn category_from_row(row: &Row) -> Result<Category, rusqlite::Error> {
    Ok(Category::new(
        row.get(0)?,
        row.get::<_, String>(1)?.try_into().map_err(|err| {
            rusqlite::Error::FromSqlConversionFailure(1, rusqlite::types::Type::Text, Box::new(err))
        })?,
        row.get::<_, String>(2)?.try_into().map_err(|err| {
            rusqlite::Error::FromSqlConversionFailure(2, rusqlite::types::Type::Text, Box::new(err))
        })?,
        vec![],
        None,
        vec![],
        row.get::<_, Option<String>>(3)?.map(|s| s.into()),
    ))
}

#[async_trait]
impl List<Category> for SqliteCategoryRepository {
    async fn list(&self) -> Result<Vec<Category>, Self::Error> {
        Ok(self
            .conn
            .call(move |conn| {
                conn.prepare("SELECT id, name, description, image FROM categories")?
                    .query_map([], category_from_row)?
                    .collect::<Result<_, rusqlite::Error>>()
                    .map_err(Box::new)
            })
            .await?)
    }
}

#[async_trait]
impl Get<Category> for SqliteCategoryRepository {
    async fn get_one(&self, k: &IdentityOf<Category>) -> Result<Option<Category>, Self::Error> {
        let k = *k;
        Ok(self
            .conn
            .call(move |conn| {
                conn.query_row(
                    "SELECT id, name, description, image FROM categories WHERE id = ?1",
                    [k],
                    category_from_row,
                )
            })
            .await
            .ok())
    }
}

#[async_trait]
impl Take<Category> for SqliteCategoryRepository {
    async fn take(&self, k: &IdentityOf<Category>) -> Result<Option<Category>, Self::Error> {
        let k = *k;
        self.conn
            .call(move |conn| conn.execute("DELETE FROM categories WHERE id = ?1", [k]))
            .await?;
        Ok(None)
    }
}

#[async_trait]
impl Update<Category> for SqliteCategoryRepository {
    async fn update(&self, _c: Category) -> Result<(), Self::Error> {
        Ok(())
    }
}

impl Repository<Category> for SqliteCategoryRepository {
    type Error = Error;
}

impl CategoryRepository for SqliteCategoryRepository {}
