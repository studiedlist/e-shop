use actix::Actor;
use actix_telepathy::Cluster;
use env_logger::{Builder, Env};
use http::access::{repository::SqliteUserCredentialsRepository, service::UserCredentialsService};
use http::cart::{
    repository::{SqliteCartRepository, SqlitePurchaseRepository},
    service::{CartService, PurchaseService},
};
use http::catalog::{repository::SqliteCategoryRepository, service::CategoryService};
use http::product::{repository::SqliteProductRepository, service::ProductService};
use http::user::{repository::SqliteUserRepository, service::UserService};
use std::error::Error;
use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4};
use std::sync::Arc;

#[actix_web::main]
async fn main() -> Result<(), Box<dyn Error>> {
    #[allow(unused_mut)]
    let mut builder = &mut Builder::from_env(Env::default().default_filter_or("info"));
    // Write logs to file in release builds.
    #[cfg(not(debug_assertions))]
    {
        use env_logger::fmt::Target;
        use std::fs::File;
        let file = Box::new(File::create(".log")?);
        builder = builder.target(Target::Pipe(file));
    }
    builder.init();
    // Parse variables from .env file or environment
    let (_host, _user, _dbname, _password) = {
        match envmnt::parse_file(".env") {
            Ok(env) => (
                env.get("DB_HOST").unwrap().clone(),
                env.get("DB_USER").unwrap().clone(),
                env.get("DB_NAME").unwrap().clone(),
                env.get("DB_PASSWORD").unwrap().clone(),
            ),
            Err(err) => {
                log::warn!("Error opening .env file\n{:?}", err);
                (
                    envmnt::get_or_panic("DB_HOST"),
                    envmnt::get_or_panic("DB_USER"),
                    envmnt::get_or_panic("DB_NAME"),
                    envmnt::get_or_panic("DB_PASSWORD"),
                )
            }
        }
    };

    // Postgres configuration and connection. Will be used later instead of Sqlite
    /*
    let mut pg_config = tokio_postgres::Config::new();
    pg_config.host(&host);
    pg_config.user(&user);
    pg_config.dbname(&dbname);
    pg_config.password(&password);
    let (client, conn) = pg_config.connect(NoTls).await?;
    let client = Arc::new(client);
    tokio::spawn(async move {
        if let Err(e) = conn.await {
            eprintln!("connection error: {}", e);
        }
    });
    */
    // Sqlite db is opened in memory for the sake of simplicity
    let path = "database.db";
    let connection = Arc::new(tokio_rusqlite::Connection::open(path).await?);
    //let connection = Arc::new(tokio_rusqlite::Connection::open_in_memory().await?);
    let product_repository = Arc::new(
        SqliteProductRepository::new(connection.clone())
            .await
            .unwrap(),
    );
    let category_repository = Arc::new(
        SqliteCategoryRepository::new(connection.clone())
            .await
            .unwrap(),
    );
    let user_credentials_repository = Arc::new(
        SqliteUserCredentialsRepository::new(connection.clone())
            .await
            .unwrap(),
    );
    let user_repository = Arc::new(SqliteUserRepository::new(connection.clone()).await.unwrap());
    let cart_repository = Arc::new(SqliteCartRepository::new(connection.clone()).await.unwrap());
    let purchase_repository = Arc::new(
        SqlitePurchaseRepository::new(connection.clone())
            .await
            .unwrap(),
    );

    let product_service = Arc::new(ProductService::new(product_repository.clone()).start());
    let category_service = Arc::new(CategoryService::new(category_repository.clone()).start());
    let user_credentials_service =
        Arc::new(UserCredentialsService::new(user_credentials_repository.clone()).start());
    let user_service = Arc::new(UserService::new(user_repository.clone()).start());
    let cart_service = Arc::new(CartService::new(cart_repository.clone()).start());
    let purchase_service = Arc::new(PurchaseService::new(purchase_repository.clone()).start());
    //use typesafe_repository::Add;
    // Generate categories and products
    /*
    for i in (0..4).rev() {
        let (name, description) = match i {
            0 => ("Start from zero", "Here you will find courses suitable for those who want to start their way in UI/UX and design in general"),
            1 => ("For beginners", ""),
            2 => ("Professional development", ""),
            3 => ("Mastering your skills", ""),
            _ => panic!(),
        };
        let image = match i {
            0 => Some("/static/images/category/from_zero.png"),
            1 => Some("/static/images/category/for_beginners.png"),
            2 => Some("/static/images/category/professional_development.png"),
            3 => Some("/static/images/category/mastering.jpeg"),
            _ => None,
        }
        .map(|i| i.into());
        let category = Category::new(
            uuid::Uuid::new_v4(),
            name.try_into().unwrap(),
            description.try_into().unwrap(),
            vec![],
            None,
            vec![],
            image,
        );
        for y in 0..5 {
            let (name, description) = match (i, y) {
                (0, 0) => (
                    "Zero to hero in UX",
                    "We will make you professional UX designer in 12 months",
                ),
                (0, 1) => ("First steps with UX", ""),
                (0, 2) => ("UX for non-designers", ""),
                (0, 3) => ("Introduction to UI/UX design", ""),
                (0, 4) => ("Your way in UX", ""),
                (0, _) => continue,
                (1, 0) => ("UX in production", ""),
                (1, 1) => ("Responsive UI design", ""),
                (1, 2) => ("Reusable UI", ""),
                (1, 3) => ("Effective UX", ""),
                (1, 4) => ("UI/UX workflows", ""),
                (1, _) => continue,
                (2, 0) => ("Advanced topics in UX", ""),
                (2, 1) => ("UX pro tips", ""),
                (2, 2) => ("UI/UX best practices", ""),
                (2, _) => continue,
                (3, 0) => ("Top-level UX", ""),
                (3, 1) => ("Crossplatform UI", ""),
                (3, 2) => ("Unified UX", ""),
                (3, _) => continue,
                (x, _) if x != 0 => ("abc", "cde"),
                _ => panic!(),
            };
            let images = match (i, y) {
                (0, 0) => vec!["/static/images/product/zero_to_hero.jpeg"],
                (0, 1) => vec!["/static/images/product/first_steps.jpeg"],
                (0, 2) => vec!["/static/images/product/non_designers.jpeg"],
                (0, 3) => vec!["/static/images/product/introduction.jpeg"],
                (0, 4) => vec!["/static/images/product/your_way.jpeg"],
                _ => vec![],
            };
            let images = images.into_iter().map(|i| i.into()).collect();
            let product = Product::new(
                uuid::Uuid::new_v4(),
                name.try_into().unwrap(),
                description.try_into().unwrap(),
                chrono::Utc::now().naive_local(),
                Some(
                    rust_decimal::Decimal::from_str_exact("99.9")
                        .unwrap()
                        .into(),
                ),
                None.into(),
                ValidIdentity::of(&category),
                images,
            );
            product_repository.add(product).await.unwrap();
        }
        category_repository.add(category).await.unwrap();
    }
    */

    let self_addr = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(127, 0, 0, 1), 8081));
    let _cluster = Cluster::new(self_addr, vec![]);

    let addr = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(127, 0, 0, 1), 8081));
    let _remote_product_service = Arc::new(
        remote::product::RemoteProductRequestService::new(product_service.clone(), addr).start(),
    );

    let _remote_category_service = Arc::new(
        remote::catalog::RemoteCategoryRequestService::new(category_service.clone(), addr).start(),
    );

    let _remote_user_credentials_service = Arc::new(
        remote::access::RemoteUserCredentialsRequestService::new(
            user_credentials_service.clone(),
            addr,
        )
        .start(),
    );

    let _remote_user_service =
        Arc::new(remote::user::RemoteUserRequestService::new(user_service.clone(), addr).start());
    let _remote_cart_service =
        Arc::new(remote::cart::RemoteCartRequestService::new(cart_service.clone(), addr).start());
    let _remote_purchase_service = Arc::new(
        remote::purchase::RemotePurchaseRequestService::new(purchase_service.clone(), addr).start(),
    );

    http::start_http_server(product_service).await;
    Ok(())
}
