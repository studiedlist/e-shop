use async_trait::async_trait;
use e_shop_core::access::{
    model::{Role, UserCredentials, UserCredentialsRole},
    service::generate_salt,
    types::{Login, Password},
};
use rusqlite::{params, Row};
use std::sync::Arc;
use tokio_rusqlite::Connection;
use typesafe_repository::prelude::*;
use uuid::Uuid;
use typesafe_repository::async_ops::{Add, GetBy, List, Get, Take, Update};

pub trait UserCredentialsRepository:
    Repository<UserCredentials, Error = Error>
    + Add<UserCredentials>
    + GetBy<UserCredentials, Login>
    + List<UserCredentials>
    + Get<UserCredentials>
    + Take<UserCredentials>
    + Update<UserCredentials>
    + Repository<Role, Error = Error>
    + Get<Role>
{
    type Error;
}

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub struct SqliteUserCredentialsRepository {
    conn: Arc<Connection>,
}

impl SqliteUserCredentialsRepository {
    pub async fn new(conn: Arc<Connection>) -> Result<Self, Error> {
        const ADMIN_LOGIN: &str = "admin";
        const ADMIN_EMAIL: &str = "admin@example.com";
        const ADMIN_PASSWORD: &str = "12345";
        const ADMIN_ROLE: &str = "admin";

        let password = Password::generate(ADMIN_PASSWORD.try_into()?, generate_salt())?;
        conn.call(move |conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS user_credentials (
                    id UUID PRIMARY KEY,
                    login TEXT NOT NULL,
                    email TEXT NOT NULL,
                    phone_number TEXT,
                    password TEXT,
                    salt TEXT,
                    role_id UUID
                );",
                (),
            )?;
            conn.execute(
                "CREATE TABLE IF NOT EXISTS roles (
                    id UUID PRIMARY KEY,
                    name TEXT NOT NULL,
                    access_level INTEGER NOT NULL
                );",
                (),
            )?;
            let admin_role = conn.query_row(
                "SELECT id FROM roles WHERE name = ?1",
                [ADMIN_ROLE],
                |r| Ok(r.get::<_, Uuid>(0))
            );
            if let Err(rusqlite::Error::QueryReturnedNoRows) = &admin_role {
                conn.execute(
                    "INSERT INTO roles (id, name, access_level) VALUES (?1, ?2, ?3);",
                    (Uuid::new_v4(), ADMIN_ROLE, 10),
                )?;
            }
            let admin_role_id = conn.query_row(
                "SELECT id FROM roles WHERE name = ?1",
                [ADMIN_ROLE],
                |r| Ok(r.get::<_, Uuid>(0))
            ).unwrap().unwrap();
            let admin_creds = conn.query_row(
                "SELECT id, role_id FROM user_credentials WHERE login = ?1",
                [ADMIN_LOGIN],
                |r| Ok((r.get::<_, Uuid>(0), r.get::<_, Option<Uuid>>(1))));

            match &admin_creds {
                Err(rusqlite::Error::QueryReturnedNoRows) => {
                    conn.execute(
                        "INSERT INTO user_credentials (id, login, email, password, salt, role_id) VALUES (?1, ?2, ?3, ?4, ?5, ?6);", 
                        (Uuid::new_v4(), ADMIN_LOGIN, ADMIN_EMAIL, password.password().to_string(), password.salt(), admin_role_id),
                    ).map(|_| ())
                },
                Err(err) => {
                    log::error!("Unable to get admin credentials:\n{err:?}");
                    Ok(())
                },
                Ok((Ok(id), Ok(role_id))) if role_id.map(|id| id != admin_role_id).unwrap_or(true) => {
                    conn.execute(
                        "UPDATE user_credentials SET role_id = ?1 WHERE id = ?2",
                        (admin_role_id, id)
                    ).map(|_| ())
                },
                _ => Ok(())
            }
        })
        .await?;
        Ok(Self { conn })
    }
}

#[async_trait]
impl Add<UserCredentials> for SqliteUserCredentialsRepository {
    async fn add(&self, c: UserCredentials) -> Result<(), Self::Error> {
        let id = c.id();
        let UserCredentials {
            login,
            email,
            phone_number,
            password,
            ..
        } = c;
        let (password, salt) = (password.password().clone(), *password.salt());
        let phone_number: Option<String> = phone_number.map(|x| x.into());
        let role_id = match c.role {
            UserCredentialsRole::Role(id) => Some(id),
            UserCredentialsRole::Default => None,
        };
        self.conn
            .call(move |conn| {
                conn.execute(
                    "INSERT INTO user_credentials (id, login, email, phone_number, password, salt, role_id) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",
                    params![
                        id,
                        **login,
                        *email,
                        phone_number,
                        *password,
                        salt,
                        role_id,
                    ],
                )
            }).await?;
        Ok(())
    }
}

pub fn user_credentials_from_row(row: &Row) -> Result<UserCredentials, rusqlite::Error> {
    let password = row.get::<_, String>(4)?.try_into().map_err(|err| {
        rusqlite::Error::FromSqlConversionFailure(4, rusqlite::types::Type::Text, Box::new(err))
    })?;
    let salt = row.get(5)?;
    let password = Password::new(password, salt).map_err(|err| {
        rusqlite::Error::FromSqlConversionFailure(5, rusqlite::types::Type::Text, Box::new(err))
    })?;
    let phone_num: Option<String> = row.get(3)?;
    let phone_num = phone_num
        .map(|x| {
            x.try_into().map_err(|err| {
                rusqlite::Error::FromSqlConversionFailure(
                    3,
                    rusqlite::types::Type::Text,
                    Box::new(err),
                )
            })
        })
        .transpose()?;
    let role_id: Option<IdentityOf<Role>> = row.get(6)?;
    let role_id = role_id
        .map(UserCredentialsRole::Role)
        .unwrap_or(UserCredentialsRole::Default);
    Ok(UserCredentials::new(
        row.get(0)?,
        row.get::<_, String>(1)?.try_into().map_err(|err| {
            rusqlite::Error::FromSqlConversionFailure(1, rusqlite::types::Type::Text, Box::new(err))
        })?,
        row.get::<_, String>(2)?.try_into().map_err(|err| {
            rusqlite::Error::FromSqlConversionFailure(2, rusqlite::types::Type::Text, Box::new(err))
        })?,
        phone_num,
        password,
        role_id,
    ))
}

#[async_trait]
impl List<UserCredentials> for SqliteUserCredentialsRepository {
    async fn list(&self) -> Result<Vec<UserCredentials>, Self::Error> {
        Ok(self
            .conn
            .call(move |conn| {
                conn.prepare(
                    "SELECT id, login, email, phone_number, password, salt, role_id FROM user_credentials",
                )?
                .query_map([], user_credentials_from_row)?
                .collect::<Result<_, rusqlite::Error>>()
                .map_err(Box::new)
            })
            .await?)
    }
}

#[async_trait]
impl Get<UserCredentials> for SqliteUserCredentialsRepository {
    async fn get_one(
        &self,
        k: &IdentityOf<UserCredentials>,
    ) -> Result<Option<UserCredentials>, Self::Error> {
        let k = *k;
        let result = self
            .conn
            .call(move |conn| {
                conn.query_row(
                    "SELECT id, login, email, phone_number, password, salt, role_id FROM user_credentials WHERE id = ?1",
                    [k],
                    user_credentials_from_row,
                )
            })
            .await;
        if let Err(err) = &result {
            log::error!("Unable to get user credentials:\n{err:?}");
        }
        Ok(result.ok())
    }
}

#[async_trait]
impl Take<UserCredentials> for SqliteUserCredentialsRepository {
    async fn take(
        &self,
        k: &IdentityOf<UserCredentials>,
    ) -> Result<Option<UserCredentials>, Self::Error> {
        let k = *k;
        self.conn
            .call(move |conn| conn.execute("DELETE FROM user_credentials WHERE id = ?1", [k]))
            .await?;
        Ok(None)
    }
}

#[async_trait]
impl Update<UserCredentials> for SqliteUserCredentialsRepository {
    async fn update(&self, _c: UserCredentials) -> Result<(), Self::Error> {
        unimplemented!()
    }
}

#[async_trait]
impl GetBy<UserCredentials, Login> for SqliteUserCredentialsRepository {
    async fn get_by(&self, l: &Login) -> Result<Option<UserCredentials>, Self::Error> {
        let l: String = l.clone().into();
        let result = self
            .conn
            .call(move |conn| {
                conn.query_row(
                    "SELECT id, login, email, phone_number, password, salt, role_id FROM user_credentials WHERE login = ?1",
                    [l],
                    user_credentials_from_row,
                )
            })
            .await;
        if let Err(err) = &result {
            log::error!("Unable to find user credentials:\n{err:?}");
        }
        Ok(result.ok())
    }
}

impl Repository<UserCredentials> for SqliteUserCredentialsRepository {
    type Error = Error;
}

fn role_from_row(row: &Row) -> Result<Role, rusqlite::Error> {
    let name = row.get::<_, String>(1)?.try_into().map_err(|err| {
        rusqlite::Error::FromSqlConversionFailure(1, rusqlite::types::Type::Text, Box::new(err))
    })?;
    Ok(Role::new(row.get(0)?, name, row.get(2)?))
}

#[async_trait]
impl Get<Role> for SqliteUserCredentialsRepository {
    async fn get_one(&self, id: &IdentityOf<Role>) -> Result<Option<Role>, Self::Error> {
        let id = *id;
        let result = self
            .conn
            .call(move |conn| {
                conn.query_row(
                    "SELECT id, name, access_level FROM roles WHERE id = ?1",
                    [id],
                    role_from_row,
                )
            })
            .await;
        if let Err(err) = &result {
            log::error!("Unable to get role:\n{err:?}");
        }
        Ok(result.ok())
    }
}

impl Repository<Role> for SqliteUserCredentialsRepository {
    type Error = Error;
}

impl UserCredentialsRepository for SqliteUserCredentialsRepository {
    type Error = Error;
}
