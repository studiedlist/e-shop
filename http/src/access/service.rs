use super::repository::UserCredentialsRepository;
use actix::{Actor, Context, Handler, Message, ResponseFuture};
use actix_broker::{Broker, SystemBroker};
use actix_service::{messages, ServiceError};
use e_shop_core::access::{
    messages::*,
    model::{UserCredentials, UserCredentialsRole},
};
use remote::access::messages::*;
use std::sync::Arc;
use typesafe_repository::IdentityOf;

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub struct UserCredentialsService {
    repository: Box<Arc<dyn UserCredentialsRepository<Error = Error>>>,
}

impl UserCredentialsService {
    pub fn new(repository: Arc<dyn UserCredentialsRepository<Error = Error>>) -> Self {
        let repository = Box::new(repository);
        Self { repository }
    }
}

impl Actor for UserCredentialsService {
    type Context = Context<Self>;
}

impl Handler<ExternalGetMessage> for UserCredentialsService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalGetMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let res = repository.get_one(&msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalGetResponse>(
                (msg.0, GetResponse(res)).into(),
            );
        })
    }
}

impl Handler<ExternalGetByLoginMessage> for UserCredentialsService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalGetByLoginMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let res = repository.get_by(&msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalGetByLoginResponse>(
                (msg.0, GetByLoginResponse(res)).into(),
            );
        })
    }
}

impl Handler<ExternalAddMessage> for UserCredentialsService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalAddMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            repository.add(msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalAddResponse>((msg.0, AddResponse).into());
        })
    }
}

impl Handler<ExternalGetRoleByLoginMessage> for UserCredentialsService {
    type Result = ResponseFuture<()>;

    fn handle(
        &mut self,
        msg: ExternalGetRoleByLoginMessage,
        _: &mut Self::Context,
    ) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let user = repository.get_by(&msg.1 .0).await.unwrap().unwrap();
            let res = match user.role {
                UserCredentialsRole::Role(id) => {
                    Some(repository.get_one(&id).await.unwrap().unwrap())
                }
                UserCredentialsRole::Default => None,
            };
            Broker::<SystemBroker>::issue_async::<InternalGetRoleByLoginResponse>(
                (msg.0, GetRoleByLoginResponse(res)).into(),
            );
        })
    }
}

pub mod messages {
    use super::*;

    messages! {
        UserCredentialsService,
        mod user_credentials -> UserCredentials,
        Get(IdentityOf<UserCredentials>) -> Option<T> {
            fn handle(&mut self, Get(id): Get, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.get_one(&id).await?)
                })
            }
        },
        List -> Vec<T> {
            fn handle(&mut self, _: List, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.list().await?)
                })
            }
        },
        Add(UserCredentials) -> () {
            fn handle(&mut self, Add(c): Add, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    repository.add(c).await?;
                    Ok(())
                })
            }
        },
        Remove(IdentityOf<UserCredentials>) -> () {
            fn handle(&mut self, Remove(id): Remove, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    repository.take(&id).await?;
                    Ok(())
                })
            }
        }
    }
}
