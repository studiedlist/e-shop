use async_trait::async_trait;
use e_shop_core::catalog::model::Category;
use e_shop_core::product::model::Product;
use rusqlite::{params, Row};
use rust_decimal::Decimal;

use std::sync::Arc;
//use tokio_postgres::Client;
use tokio_rusqlite::Connection;
use typesafe_repository::prelude::*;
use typesafe_repository::{ValidIdentity, ValidIdentityProvider};
use typesafe_repository::async_ops::{Add, List, Get, Take, Update, ListBy};

pub trait ProductRepository:
    Repository<Product>
    + Add<Product>
    + List<Product>
    + Get<Product>
    + Take<Product>
    + Update<Product>
    + ListBy<Product, ValidIdentity<Category>>
{
}

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

/*
pub struct PostgresProductRepository {
    _conn: Arc<Client>,
    cache: Mutex<HashMap<Uuid, Product>>,
}

impl PostgresProductRepository {
    pub fn new(conn: Arc<Client>) -> Self {
        Self {
            _conn: conn,
            cache: Mutex::new(HashMap::new()),
        }
    }
}

impl ProductRepository for PostgresProductRepository {}

impl Repository<Product> for PostgresProductRepository {
    type Error = Error;
}

#[async_trait]
impl Add<Product> for PostgresProductRepository {
    async fn add(&self, p: Product) -> Result<(), Error> {
        self.cache.lock().await.insert(p.id(), p);
        Ok(())
    }
}

#[async_trait]
impl List<Product> for PostgresProductRepository {
    async fn list(&self) -> Result<Vec<Product>, Error> {
        let cache = self.cache.lock().await;
        Ok(cache.values().cloned().collect())
    }
}

#[async_trait]
impl Update<Product> for PostgresProductRepository {
    async fn update(&self, p: Product) -> Result<(), Error> {
        self.cache.lock().await.insert(p.id(), p);
        Ok(())
    }
}

#[async_trait]
impl Remove<Product> for PostgresProductRepository {
    async fn remove(&self, id: &Uuid) -> Result<Option<Product>, Error> {
        Ok(self.cache.lock().await.remove(id))
    }
}

#[async_trait]
impl Get<Product> for PostgresProductRepository {
    async fn get_one(&self, _id: &Uuid) -> Result<Option<Product>, Error> {
        unimplemented!()
    }
}

#[async_trait]
impl Subset<Product, ValidIdentity<Category>> for PostgresProductRepository {
    async fn subset(&self, _id: &ValidIdentity<Category>) -> Result<Vec<Product>, Error> {
        unimplemented!()
    }
}
*/

pub struct SqliteProductRepository {
    conn: Arc<Connection>,
}

impl SqliteProductRepository {
    pub async fn new(conn: Arc<Connection>) -> Result<Self, Error> {
        conn.call(|conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS products (
                    id UUID PRIMARY KEY,
                    name TEXT NOT NULL,
                    description TEXT NOT NULL,
                    category UUID NOT NULL,
                    price DECIMAL(10, 5),
                    added INTEGER,
                    images TEXT
                );",
                (),
            )
        })
        .await?;
        Ok(Self { conn })
    }
}

#[async_trait]
impl Add<Product> for SqliteProductRepository {
    async fn add(&self, c: Product) -> Result<(), Self::Error> {
        let id = c.id();
        let Product {
            name,
            description,
            category,
            price,
            added,
            images,
            ..
        } = c;
        let images = images
            .iter()
            .filter_map(|p| p.to_str().map(|s| s.to_string()))
            .collect::<Vec<_>>()
            .pop();
        let price = price.map(|p| p.to_string()).unwrap_or("".to_string());
        self.conn
            .call(move |conn| {
                conn.execute(
                    "INSERT INTO products (id, name, description, category, price, added, images) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",
                    params![id, name.into_inner(), description.into_inner(), category.into_inner(), price, added, images],
                )
            }).await?;
        Ok(())
    }
}

pub fn product_from_row(row: &Row) -> Result<Product, rusqlite::Error> {
    let images = match row.get::<_, Option<String>>(6)? {
        Some(p) => vec![p.into()],
        None => vec![],
    };
    Ok(Product::new(
        row.get(0)?,
        row.get::<_, String>(1)?.try_into().map_err(|err| {
            rusqlite::Error::FromSqlConversionFailure(1, rusqlite::types::Type::Text, Box::new(err))
        })?,
        row.get::<_, String>(2)?.try_into().map_err(|err| {
            rusqlite::Error::FromSqlConversionFailure(2, rusqlite::types::Type::Text, Box::new(err))
        })?,
        row.get(5)?,
        Decimal::from_f64_retain(row.get::<_, f64>(4)?).map(|d| d.into()),
        None.into(),
        ValidIdentity::from(&IndexedRow(row, 3)),
        images,
    ))
}

struct IndexedRow<'a>(&'a Row<'a>, usize);

impl<'a> ValidIdentityProvider<Category> for IndexedRow<'a> {
    fn get(&self) -> IdentityOf<Category> {
        self.0.get(self.1).unwrap()
    }
}

#[async_trait]
impl List<Product> for SqliteProductRepository {
    async fn list(&self) -> Result<Vec<Product>, Self::Error> {
        Ok(self
            .conn
            .call(move |conn| {
                conn.prepare(
                    "SELECT id, name, description, category, price, added, images FROM products",
                )?
                .query_map([], product_from_row)?
                .collect::<Result<_, rusqlite::Error>>()
                .map_err(Box::new)
            })
            .await?)
    }
}

#[async_trait]
impl Get<Product> for SqliteProductRepository {
    async fn get_one(&self, k: &IdentityOf<Product>) -> Result<Option<Product>, Self::Error> {
        let k = *k;
        let result = self
            .conn
            .call(move |conn| {
                conn.query_row(
                    "SELECT id, name, description, category, price, added, images FROM products WHERE id = ?1",
                    [k],
                    product_from_row,
                )
            })
            .await;
        if let Err(err) = &result {
            log::error!("Unable to get product:\n{err:?}");
        }
        Ok(result.ok())
    }
}

#[async_trait]
impl Take<Product> for SqliteProductRepository {
    async fn take(&self, k: &IdentityOf<Product>) -> Result<Option<Product>, Self::Error> {
        let k = *k;
        self.conn
            .call(move |conn| conn.execute("DELETE FROM products WHERE id = ?1", [k]))
            .await?;
        Ok(None)
    }
}

#[async_trait]
impl Update<Product> for SqliteProductRepository {
    async fn update(&self, _p: Product) -> Result<(), Self::Error> {
        unimplemented!()
    }
}

#[async_trait]
impl ListBy<Product, ValidIdentity<Category>> for SqliteProductRepository {
    async fn list_by(&self, k: &ValidIdentity<Category>) -> Result<Vec<Product>, Self::Error> {
        let k = k.clone();
        Ok(self
            .conn
            .call(move |conn| {
                conn.prepare("SELECT id, name, description, category, price, added, images FROM products WHERE category = ?1")?
                    .query_map([k.into_inner()], product_from_row)?
                    .collect::<Result<_, rusqlite::Error>>()
                    .map_err(Box::new)
            })
            .await?)
    }
}

impl Repository<Product> for SqliteProductRepository {
    type Error = Error;
}

impl ProductRepository for SqliteProductRepository {}
