use super::repository::ProductRepository;
use actix::{Actor, Context, Handler, Message, ResponseFuture};
use actix_broker::{Broker, SystemBroker};
use actix_service::{messages, ServiceError};

use e_shop_core::product::{messages::*, model::Product};
use remote::product::{messages::*, InternalProductEvent};
use std::sync::Arc;
use typesafe_repository::{GetIdentity, IdentityOf};

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub struct ProductService {
    repository: Box<Arc<dyn ProductRepository<Error = Error>>>,
}

impl ProductService {
    pub fn new<T>(r: Arc<T>) -> Self
    where
        T: ProductRepository<Error = Error> + 'static,
    {
        Self {
            repository: Box::new(r),
        }
    }
}

impl Actor for ProductService {
    type Context = Context<Self>;
}

impl Handler<ExternalGetMessage> for ProductService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalGetMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let res = repository.get_one(&msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalGetResponse>(
                (msg.0, GetResponse(res)).into(),
            );
        })
    }
}

impl Handler<ExternalListMessage> for ProductService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalListMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let products = repository.list().await.unwrap();
            Broker::<SystemBroker>::issue_async::<InternalListResponse>(
                (msg.0, ListResponse(products)).into(),
            );
        })
    }
}

impl Handler<ExternalListTopMessage> for ProductService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: ExternalListTopMessage, _: &mut Self::Context) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let mut products = repository.list().await.unwrap();
            products.sort_by_key(|p| p.added);
            if products.len() > 5 {
                let len = products.len();
                products = products.into_iter().skip(len - 5).collect();
            }
            Broker::<SystemBroker>::issue_async::<InternalListTopResponse>(
                (msg.0, ListTopResponse(products)).into(),
            );
        })
    }
}

impl Handler<ExternalListByCategoryIdMessage> for ProductService {
    type Result = ResponseFuture<()>;

    fn handle(
        &mut self,
        msg: ExternalListByCategoryIdMessage,
        _: &mut Self::Context,
    ) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let products = repository
                .list()
                .await
                .unwrap()
                .into_iter()
                .filter(|p| p.category == msg.1 .0)
                .collect();
            log::debug!("{products:?}");
            let res = ListByCategoryIdResponse(products);
            Broker::<SystemBroker>::issue_async::<InternalListByCategoryIdResponse>(
                (msg.0, res).into(),
            );
        })
    }
}

pub mod messages {
    use super::*;

    messages! {
        ProductService,
        mod product -> Product,
        Get(IdentityOf<Product>) -> Option<T> {
            fn handle(&mut self, Get(id): Get, _ctx: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.get_one(&id).await?)
                })
            }
        },
        List -> Vec<T> {
            fn handle(&mut self, _: List, _ctx: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.list().await?)
                })
            }
        },
        Update(Product) -> () {
            fn handle(&mut self, Update(p): Update, _ctx: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let id = p.id();
                    repository.update(p).await?;
                    let event : InternalProductEvent = ProductEvent::Updated(id).into();
                    Broker::<SystemBroker>::issue_async(event);
                    Ok(())
                })
            }
        },
        Add(Product) -> () {
            fn handle(&mut self, Add(p): Add, _ctx: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let id = p.id();
                    repository.add(p).await?;
                    let event : InternalProductEvent = ProductEvent::Created(id).into();
                    Broker::<SystemBroker>::issue_async(event);
                    Ok(())
                })
            }
        },
        Remove(IdentityOf<Product>) -> () {
            fn handle(&mut self, Remove(id): Remove, _ctx: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    repository.take(&id).await?;
                    let event : InternalProductEvent = ProductEvent::Removed(id).into();
                    Broker::<SystemBroker>::issue_async(event);
                    Ok(())
                })
            }
        }
    }
}
