use super::service::messages;
use super::service::ProductService;
use crate::catalog::service::{messages::category as category_messages, CategoryService};
use actix::Addr;
use actix_controllers::prelude::*;
use actix_web::web::Data;
use chrono::NaiveDateTime;
use e_shop_core::{
    catalog::model::Category,
    product::model::{InStock, Product},
    Price, Text,
};
use serde::{Deserialize, Serialize};
use typesafe_repository::{GetIdentity, IdentityOf, ValidIdentity};
use uuid::Uuid;

#[derive(Serialize, Deserialize)]
pub struct ProductDto {
    pub name: Text<64>,
    pub description: Text<4096>,
    pub price: Option<Price>,
    pub in_stock: InStock,
    pub added: NaiveDateTime,
    pub category: IdentityOf<Category>,
}

impl ProductDto {
    pub fn into_product(self, id: ValidIdentity<Category>) -> Product {
        Product::new(
            Uuid::new_v4(),
            self.name,
            self.description,
            self.added,
            self.price,
            self.in_stock,
            id,
            vec![],
        )
    }
}

impl From<Product> for ProductDto {
    fn from(
        Product {
            name,
            description,
            price,
            in_stock,
            category,
            added,
            ..
        }: Product,
    ) -> Self {
        Self {
            name,
            description,
            price,
            in_stock,
            category: category.into_inner(),
            added,
        }
    }
}

pub struct ProductDtoWithIdentity(ProductDto, IdentityOf<Product>);

impl ProductDtoWithIdentity {
    pub fn into_product(self, category_id: ValidIdentity<Category>) -> Product {
        let p = self.0;
        let id = self.1;
        Product::new(
            id,
            p.name,
            p.description,
            p.added,
            p.price,
            p.in_stock,
            category_id,
            vec![],
        )
    }
}

#[get("/{id}")]
pub async fn get(id: Path<IdentityOf<Product>>, service: Data<Addr<ProductService>>) -> Response {
    let product = service.send(messages::product::Get(*id)).await??;
    Ok(match product {
        Some(p) => {
            let p: ProductDto = p.into();
            HttpResponse::Ok().json(p)
        }
        None => HttpResponse::NotFound().body(()),
    })
}

#[post("/new")]
pub async fn new(
    q: Query<ProductDto>,
    product_service: Data<Addr<ProductService>>,
    catalog_service: Data<Addr<CategoryService>>,
) -> Response {
    let category = catalog_service
        .send(category_messages::Get(q.category))
        .await??
        .ok_or(ControllerError::NotFound)?;
    let product: Product = q.into_inner().into_product(ValidIdentity::of(&category));
    let id = product.id();
    product_service
        .send(messages::product::Add(product))
        .await??;
    Ok(HttpResponse::Ok().json(id))
}

#[post("/{id}")]
pub async fn update(
    id: Path<IdentityOf<Product>>,
    product_service: Data<Addr<ProductService>>,
    category_service: Data<Addr<CategoryService>>,
    q: Query<ProductDto>,
) -> Response {
    let category = category_service
        .send(category_messages::Get(q.category))
        .await??
        .ok_or(ControllerError::NotFound)?;
    let dto = ProductDtoWithIdentity(q.into_inner(), *id);
    let product: Product = dto.into_product(ValidIdentity::of(&category));
    product_service
        .send(messages::product::Update(product))
        .await??;
    Ok(HttpResponse::Ok().body(()))
}

#[delete("/{id}")]
pub async fn delete(
    id: Path<IdentityOf<Product>>,
    service: Data<Addr<ProductService>>,
) -> Response {
    service
        .send(messages::product::Remove(id.into_inner()))
        .await??;
    Ok(HttpResponse::Ok().body(()))
}

#[get("/")]
pub async fn list(service: Data<Addr<ProductService>>) -> Response {
    let products = service.send(messages::product::List).await??;
    Ok(HttpResponse::Ok().json(products))
}
