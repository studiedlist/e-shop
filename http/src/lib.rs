pub mod access;
pub mod cart;
pub mod catalog;
pub mod controllers;
pub mod product;
pub mod user;

use actix::Addr;
use actix_web::{web, App, HttpServer};
use std::sync::Arc;
use tracing_actix_web::TracingLogger;

pub async fn start_http_server(product_service: Arc<Addr<product::service::ProductService>>) {
    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(product_service.clone()))
            .wrap(actix_web::middleware::Compress::default())
            .wrap(TracingLogger::default())
            .service(
                actix_web::web::scope("/product")
                    .service(product::controllers::new)
                    .service(product::controllers::get)
                    .service(product::controllers::list)
                    .service(product::controllers::delete),
            )
    })
    .bind(("127.0.0.1", 8080))
    .unwrap()
    .run()
    .await
    .unwrap();
}
