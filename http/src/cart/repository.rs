use async_trait::async_trait;
use e_shop_core::access::model::UserCredentials;
use e_shop_core::cart::model::{Cart, ContactInfo, Purchase};
use e_shop_core::Address;
use rusqlite::{params, Row};
use std::sync::Arc;
use tokio_rusqlite::Connection;
use typesafe_repository::prelude::*;
use typesafe_repository::{ValidIdentity, ValidIdentityProvider};
use typesafe_repository::async_ops::{Add, Get, ListBy};

pub trait CartRepository: Repository<Cart> + Add<Cart> + Get<Cart> {}

pub trait PurchaseRepository:
    Repository<Purchase>
    + Add<Purchase>
    + Get<Purchase>
    + ListBy<Purchase, Option<ValidIdentity<UserCredentials>>>
{
}

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub struct SqliteCartRepository {
    conn: Arc<Connection>,
}

impl SqliteCartRepository {
    pub async fn new(conn: Arc<Connection>) -> Result<Self, Error> {
        conn.call(|conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS carts (
                    id UUID PRIMARY KEY
                );
                ",
                (),
            )?;
            conn.execute(
                "
                CREATE TABLE IF NOT EXISTS cart_entries (
                    cart_id UUID NOT NULL,
                    product_id UUID NOT NULL,
                    count INTEGER NOT NULL
                );",
                (),
            )
        })
        .await?;
        Ok(Self { conn })
    }
}

#[async_trait]
impl Add<Cart> for SqliteCartRepository {
    async fn add(&self, c: Cart) -> Result<(), Self::Error> {
        self.conn
            .call(move |conn| {
                let id = c.id();
                conn.execute("INSERT INTO carts (id) VALUES (?1)", params![id])?;
                for (item, count) in c.items {
                    conn.execute(
                        "INSERT INTO cart_entries (cart_id, product_id, count) VALUES (?1, ?2, ?3)",
                        params![id, item, count],
                    )?;
                }
                Result::<_, rusqlite::Error>::Ok(())
            })
            .await?;
        Ok(())
    }
}

#[async_trait]
impl Get<Cart> for SqliteCartRepository {
    async fn get_one(&self, id: &IdentityOf<Cart>) -> Result<Option<Cart>, Self::Error> {
        let id = *id;
        let items = self
            .conn
            .call(move |conn| {
                conn.prepare("SELECT product_id, count FROM cart_entries WHERE cart_id = ?1")?
                    .query_map([id], |r| Ok((r.get(0)?, r.get(1)?)))?
                    .collect::<Result<_, rusqlite::Error>>()
                    .map_err(Box::new)
            })
            .await?;
        Ok(Some(Cart { id, items }))
    }
}

impl Repository<Cart> for SqliteCartRepository {
    type Error = Error;
}

impl CartRepository for SqliteCartRepository {}

pub struct SqlitePurchaseRepository {
    conn: Arc<Connection>,
}

impl SqlitePurchaseRepository {
    pub async fn new(conn: Arc<Connection>) -> Result<Self, Error> {
        conn.call(|conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS purchases (
                    id UUID PRIMARY KEY,
                    user_id UUID NOT NULL,
                    cart_id UUID NOT NULL,
                    date INTERGER
                );
                ",
                (),
            )?;
            conn.execute(
                "CREATE TABLE IF NOT EXISTS contact_info (
                    purchase_id UUID NOT NULL,
                    phone_number TEXT,
                    first_name TEXT NOT NULL,
                    last_name TEXT NOT NULL,
                    email TEXT NOT NULL,
                    address_city TEXT NOT NULL,
                    address_street TEXT NOT NULL
                );",
                (),
            )
        })
        .await?;
        Ok(Self { conn })
    }
}

#[async_trait]
impl Add<Purchase> for SqlitePurchaseRepository {
    async fn add(&self, p: Purchase) -> Result<(), Self::Error> {
        self.conn
            .call(move |conn| {
                let id = p.id();
                conn.execute(
                    "INSERT INTO purchases (id, user_id, cart_id, date) 
                    VALUES (?1, ?2, ?3, ?4)",
                    params![
                        id,
                        p.user_id.map(|x| x.into_inner()),
                        p.cart.into_inner(),
                        p.date
                    ],
                )?;
                let ContactInfo {
                    phone_number,
                    first_name,
                    last_name,
                    email,
                    address,
                } = p.contact_info;
                let Address {
                    city,
                    street,
                } = address;
                conn.execute(
                    "INSERT INTO contact_info 
                    (purchase_id, phone_number, first_name, last_name, email, address_city, address_street) 
                    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",
                    params![
                        id,
                        phone_number.map(|x| x.to_string()),
                        first_name.to_string(),
                        last_name.to_string(),
                        email.to_string(),
                        city,
                        street,
                    ],
                )?;
                Result::<_, rusqlite::Error>::Ok(())
            })
            .await?;
        Ok(())
    }
}

pub fn contact_info_from_row(r: &Row) -> Result<ContactInfo, rusqlite::Error> {
    let (city, street) = (r.get(5)?, r.get(6)?);
    let addr = Address::new(city, street);
    Ok(ContactInfo::new(
        r.get::<_, Option<String>>(1)?
            .map(|x| x.try_into())
            .transpose()
            .map_err(|err| {
                rusqlite::Error::FromSqlConversionFailure(
                    0,
                    rusqlite::types::Type::Text,
                    Box::new(err),
                )
            })?,
        r.get::<_, String>(2)?.try_into().map_err(|err| {
            rusqlite::Error::FromSqlConversionFailure(1, rusqlite::types::Type::Text, Box::new(err))
        })?,
        r.get::<_, String>(3)?.try_into().map_err(|err| {
            rusqlite::Error::FromSqlConversionFailure(2, rusqlite::types::Type::Text, Box::new(err))
        })?,
        r.get::<_, String>(4)?.try_into().map_err(|err| {
            rusqlite::Error::FromSqlConversionFailure(3, rusqlite::types::Type::Text, Box::new(err))
        })?,
        addr,
    ))
}

struct IndexedRow<'a>(&'a Row<'a>, usize);

impl<'a> ValidIdentityProvider<Cart> for IndexedRow<'a> {
    fn get(&self) -> IdentityOf<Cart> {
        self.0.get(self.1).unwrap()
    }
}

impl<'a> ValidIdentityProvider<UserCredentials> for IndexedRow<'a> {
    fn get(&self) -> IdentityOf<UserCredentials> {
        self.0.get(self.1).unwrap()
    }
}

pub fn purchase_from_row(r: &Row, contact_info: ContactInfo) -> Result<Purchase, rusqlite::Error> {
    Ok(Purchase::new(
        r.get(0)?,
        Some(ValidIdentity::from(&IndexedRow(r, 1))),
        ValidIdentity::from(&IndexedRow(r, 2)),
        contact_info,
        r.get(3)?,
    ))
}

#[async_trait]
impl Get<Purchase> for SqlitePurchaseRepository {
    async fn get_one(&self, id: &IdentityOf<Purchase>) -> Result<Option<Purchase>, Self::Error> {
        let id = *id;
        let contact_info : ContactInfo = self
            .conn
            .call(move |conn| {
                conn.query_row(
                    "SELECT purchase_id, phone_number, first_name, last_name, email, address_city, address_street FROM contact_info WHERE purchase_id = ?1",
                    [id],
                    contact_info_from_row)
            })
            .await?;
        let purchase = self
            .conn
            .call(move |conn| {
                conn.query_row(
                    "SELECT id, user_id, cart_id, date FROM purchases WHERE id = ?1",
                    [id],
                    |r| purchase_from_row(r, contact_info),
                )
            })
            .await?;
        Ok(Some(purchase))
    }
}

#[async_trait]
impl ListBy<Purchase, Option<ValidIdentity<UserCredentials>>> for SqlitePurchaseRepository {
    async fn list_by(
        &self,
        id: &Option<ValidIdentity<UserCredentials>>,
    ) -> Result<Vec<Purchase>, Self::Error> {
        let id = id.clone().map(|id| id.into_inner());
        let purchase_ids = self
            .conn
            .call(move |conn| {
                conn.prepare("SELECT id FROM purchases WHERE user_id = ?1")?
                    .query_map([id], |r| r.get::<_, IdentityOf<Purchase>>(0))?
                    .collect::<Result<Vec<IdentityOf<Purchase>>, rusqlite::Error>>()
            })
            .await?;
        let mut purchases = vec![];
        for id in purchase_ids {
            purchases.push(self.get_one(&id).await?.unwrap());
        }
        Ok(purchases)
    }
}

impl Repository<Purchase> for SqlitePurchaseRepository {
    type Error = Error;
}

impl PurchaseRepository for SqlitePurchaseRepository {}
