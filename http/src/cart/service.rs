use super::repository::{CartRepository, PurchaseRepository};
use actix::{Actor, Context, Handler, Message, ResponseFuture};
use actix_broker::{Broker, SystemBroker};
use actix_service::{messages, ServiceError};

use e_shop_core::cart::{
    messages::cart as cart_messages, messages::purchase as purchase_messages, model::Cart,
};
use remote::cart::messages as remote_cart_messages;
use remote::purchase::messages as remote_purchase_messages;
use std::sync::Arc;
use typesafe_repository::IdentityOf;

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub struct CartService {
    repository: Box<Arc<dyn CartRepository<Error = Error>>>,
}

impl CartService {
    pub fn new<T>(r: Arc<T>) -> Self
    where
        T: CartRepository<Error = Error> + 'static,
    {
        Self {
            repository: Box::new(r),
        }
    }
}

impl Actor for CartService {
    type Context = Context<Self>;
}

impl Handler<remote_cart_messages::ExternalGetMessage> for CartService {
    type Result = ResponseFuture<()>;

    fn handle(
        &mut self,
        msg: remote_cart_messages::ExternalGetMessage,
        _: &mut Self::Context,
    ) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let res = repository.get_one(&msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<remote_cart_messages::InternalGetResponse>(
                (msg.0, cart_messages::GetResponse(res)).into(),
            );
        })
    }
}

impl Handler<remote_cart_messages::ExternalAddMessage> for CartService {
    type Result = ResponseFuture<()>;

    fn handle(
        &mut self,
        msg: remote_cart_messages::ExternalAddMessage,
        _: &mut Self::Context,
    ) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            repository.add(msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<remote_cart_messages::InternalAddResponse>(
                (msg.0, cart_messages::AddResponse).into(),
            );
        })
    }
}

pub mod messages {
    use super::*;

    messages! {
        CartService,
        mod cart -> Cart,
        Get(IdentityOf<Cart>) -> Option<T> {
            fn handle(&mut self, Get(id): Get, _: &mut Self::Context) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.get_one(&id).await?)
                })
            }
        },
        Add(Cart) -> () {
            fn handle(&mut self, Add(c): Add, _: &mut Self::Context) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.add(c).await?)
                })
            }
        }
    }
}

pub struct PurchaseService {
    repository: Box<Arc<dyn PurchaseRepository<Error = Error>>>,
}

impl PurchaseService {
    pub fn new<T>(r: Arc<T>) -> Self
    where
        T: PurchaseRepository<Error = Error> + 'static,
    {
        Self {
            repository: Box::new(r),
        }
    }
}

impl Actor for PurchaseService {
    type Context = Context<Self>;
}

impl Handler<remote_purchase_messages::ExternalGetMessage> for PurchaseService {
    type Result = ResponseFuture<()>;

    fn handle(
        &mut self,
        msg: remote_purchase_messages::ExternalGetMessage,
        _: &mut Self::Context,
    ) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let res = repository.get_one(&msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<remote_purchase_messages::InternalGetResponse>(
                (msg.0, purchase_messages::GetResponse(res)).into(),
            );
        })
    }
}

impl Handler<remote_purchase_messages::ExternalAddMessage> for PurchaseService {
    type Result = ResponseFuture<()>;

    fn handle(
        &mut self,
        msg: remote_purchase_messages::ExternalAddMessage,
        _: &mut Self::Context,
    ) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            repository.add(msg.1 .0).await.unwrap();
            Broker::<SystemBroker>::issue_async::<remote_purchase_messages::InternalAddResponse>(
                (msg.0, purchase_messages::AddResponse).into(),
            );
        })
    }
}

impl Handler<remote_purchase_messages::ExternalListByUserIdMessage> for PurchaseService {
    type Result = ResponseFuture<()>;

    fn handle(
        &mut self,
        msg: remote_purchase_messages::ExternalListByUserIdMessage,
        _: &mut Self::Context,
    ) -> Self::Result {
        let repository = self.repository.clone();
        Box::pin(async move {
            let res = repository.list_by(&Some(msg.1 .0)).await.unwrap();
            Broker::<SystemBroker>::issue_async::<
                remote_purchase_messages::InternalListByUserIdResponse,
            >((msg.0, purchase_messages::ListByUserIdResponse(res)).into());
        })
    }
}
