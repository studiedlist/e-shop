use actix::{Actor, AsyncContext};
use actix_session::{storage::CookieSessionStore, SessionMiddleware};
use actix_telepathy::Cluster;
use actix_web::{cookie::Key, guard, web::Data, App, HttpServer};
use env_logger::Env;
use tokio::sync::Mutex;

use remote::cart::RemoteCartResponseService;
use remote::purchase::RemotePurchaseResponseService;
use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4};
use std::sync::Arc;
use tracing_actix_web::TracingLogger;
use web::{
    access::{self, repository::RemoteUserCredentialsRepository, service::UserCredentialsService},
    cart::{
        self,
        repository::{RemoteCartRepository, RemotePurchaseRepository},
        service::{CartService, PurchaseService},
    },
    catalog,
    catalog::{repository::RemoteCategoryRepository, service::CatalogService},
    controllers,
    product::{self, repository::RemoteProductRepository, service::ProductService},
    user::{self, repository::RemoteUserRepository, service::UserService},
};

pub fn read_key() -> Vec<u8> {
    Vec::from(
        match envmnt::parse_file(".env") {
            Ok(env) => env.get("COOKIES_KEY").unwrap().clone(),
            Err(err) => {
                log::warn!("Error opening .env file\n{:?}", err);
                std::env::var("COOKIES_KEY")
                    .expect("Unable to read secret key from COOKIES_KEY env variable")
            }
        }
        .as_bytes(),
    )
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(Env::default().default_filter_or("info"));
    let self_addr = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(127, 0, 0, 1), 8082));
    // Start cluster for remote actor communication
    let _cluster = Cluster::new(
        self_addr,
        vec![SocketAddr::V4(SocketAddrV4::new(
            Ipv4Addr::new(127, 0, 0, 1),
            8081,
        ))],
    );
    // Set response error handler
    actix_controllers::RESPONSE_ERROR
        .set(Box::new(|err| {
            use actix_controllers::ControllerError::*;
            use actix_web::HttpResponse;
            use askama::Template;
            match err {
                NotFound => {
                    actix_controllers::render_template(web::controllers::NotFound::new(None))
                        .unwrap()
                }
                Unauthorized => HttpResponse::Unauthorized().body(
                    crate::controllers::Unauthorized::new(None)
                        .render()
                        .unwrap(),
                ),
                InternalServerError(err) => {
                    HttpResponse::InternalServerError().body(err.to_string())
                }
                CorruptedData(err) => HttpResponse::InternalServerError().body(err.to_string()),
                InvalidInput { field, msg } => {
                    HttpResponse::BadRequest().body(format!("{field}\n{msg}"))
                }
            }
        }))
        .map_err(|_| ())
        .unwrap();
    let product_service = Arc::new(ProductService::create(|ctx| {
        let remote_product_service = Arc::new(
            remote::product::RemoteProductResponseService::new(Arc::new(ctx.address()), self_addr)
                .start(),
        );
        let repository = Arc::new(Mutex::new(RemoteProductRepository::new(
            remote_product_service,
        )));
        ProductService::new(Box::new(repository))
    }));
    let category_service = Arc::new(CatalogService::create(|ctx| {
        let remote_category_service = Arc::new(
            remote::catalog::RemoteCategoryResponseService::new(Arc::new(ctx.address()), self_addr)
                .start(),
        );
        let repository = Arc::new(Mutex::new(RemoteCategoryRepository::new(
            remote_category_service,
        )));
        CatalogService::new(Box::new(repository))
    }));
    let user_credentials_service = Arc::new(UserCredentialsService::create(|ctx| {
        let remote_service = Arc::new(
            remote::access::RemoteUserCredentialsResponseService::new(
                Arc::new(ctx.address()),
                self_addr,
            )
            .start(),
        );
        let repository = Arc::new(RemoteUserCredentialsRepository::new(remote_service));
        UserCredentialsService::new(Box::new(repository))
    }));
    let user_service = Arc::new(UserService::create(|ctx| {
        let remote_service = Arc::new(
            remote::user::RemoteUserResponseService::new(Arc::new(ctx.address()), self_addr)
                .start(),
        );
        let repository = Arc::new(RemoteUserRepository::new(remote_service));
        UserService::new(Box::new(repository))
    }));
    let cart_service = Arc::new(CartService::create(|_| {
        let remote_service = RemoteCartResponseService::new(self_addr).start().into();
        let repository = Arc::new(RemoteCartRepository::new(remote_service));
        CartService::new(Box::new(repository))
    }));
    let purchase_service = Arc::new(PurchaseService::create(|_| {
        let remote_service = RemotePurchaseResponseService::new(self_addr).start().into();
        let repository = Arc::new(RemotePurchaseRepository::new(remote_service));
        PurchaseService::new(repository)
    }));
    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(product_service.clone()))
            .app_data(Data::new(category_service.clone()))
            .app_data(Data::new(user_credentials_service.clone()))
            .app_data(Data::new(user_service.clone()))
            .app_data(Data::new(cart_service.clone()))
            .app_data(Data::new(purchase_service.clone()))
            .wrap(actix_web::middleware::Compress::default())
            .wrap(TracingLogger::default())
            .wrap(actix_session_identity::SessionMiddlewareFactory {})
            .wrap(
                // Initialize session middleware with cookies key read from environment
                SessionMiddleware::builder(CookieSessionStore::default(), Key::from(&read_key()))
                    .cookie_http_only(false)
                    .cookie_secure(false)
                    .build(),
            )
            // Register static files middleware
            .service(actix_files::Files::new("/static", "static").show_files_listing())
            .service(actix_web::web::scope("/product").service(product::controllers::info))
            .service(
                actix_web::web::scope("/catalog")
                    .service(catalog::controllers::catalog)
                    .service(catalog::controllers::category_page),
            )
            .service(
                actix_web::web::scope("/cart")
                    .service(cart::controllers::cart_page)
                    .service(cart::controllers::add_to_cart)
                    .service(cart::controllers::confirm_cart)
                    .service(cart::controllers::remove_from_cart)
                    .service(cart::controllers::success)
                    .service(cart::controllers::user_details)
                    .service(cart::controllers::cart_info),
            )
            .service(access::controllers::register)
            .service(access::controllers::register_page)
            .service(access::controllers::log_in)
            .service(access::controllers::logout)
            .service(access::controllers::login_page)
            .service(product::controllers::add)
            .service(product::controllers::delete)
            .service(product::controllers::list)
            .service(product::controllers::edit)
            .service(user::controllers::user_page)
            .service(user::controllers::manage_application)
            .service(user::controllers::add_category)
            .service(user::controllers::delete_category)
            .service(user::controllers::list_categories)
            .service(catalog::controllers::news_page)
            .service(controllers::index)
            // Show not found page if no controllers handled request
            .default_service(
                actix_web::web::route()
                    .guard(guard::Not(guard::Get()))
                    .to(web::controllers::not_found),
            )
    })
    .bind(("0.0.0.0", 3000))?
    .run()
    .await
}
