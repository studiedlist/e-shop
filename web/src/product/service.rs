use super::repository::ProductRepository;
use actix::{Actor, Context, Handler, Message, ResponseFuture};
use actix_broker::BrokerSubscribe;
use actix_service::{messages, ServiceError};
use derive_new::new;
use e_shop_core::catalog::model::Category;
use e_shop_core::product::model::{Latest, Product};
use remote::product::messages::*;
use std::sync::Arc;
use tokio::sync::{Mutex, Notify};
use typesafe_repository::prelude::*;
use typesafe_repository::ValidIdentity;

#[derive(new)]
pub struct ProductService {
    repository: Box<Arc<Mutex<dyn ProductRepository + Send + Sync + 'static>>>,
}

impl Actor for ProductService {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.subscribe_system_async::<SystemProductEvent>(ctx);
        self.subscribe_system_async::<NotifyEvent<_>>(ctx);
    }
}

impl Handler<ExternalGetResponse> for ProductService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _msg: ExternalGetResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

impl Handler<ExternalListResponse> for ProductService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _msg: ExternalListResponse, _: &mut Self::Context) -> Self::Result {
        log::debug!("Got external list response");
        Box::pin(async move {})
    }
}

impl Handler<ExternalListTopResponse> for ProductService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _msg: ExternalListTopResponse, _: &mut Self::Context) -> Self::Result {
        log::debug!("Got external list response");
        Box::pin(async move {})
    }
}

impl Handler<ExternalListByCategoryIdResponse> for ProductService {
    type Result = ResponseFuture<()>;

    fn handle(
        &mut self,
        _msg: ExternalListByCategoryIdResponse,
        _: &mut Self::Context,
    ) -> Self::Result {
        Box::pin(async move {})
    }
}

pub mod messages {
    use super::*;

    messages! {
        ProductService,
        mod product -> Product,
        Get(IdentityOf<Product>) -> Option<T> {
            fn handle(&mut self, Get(id): Get, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let res = repository.lock().await.get_one(&id).await?;
                    Ok(res)
                })
            }
        },
        List() -> Vec<T> {
            fn handle(&mut self, _: List, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let res = repository.lock().await.list().await?;
                    Ok(res)
                })
            }
        }
        ListTop -> Vec<T> {
            fn handle(&mut self, _: ListTop, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let res = repository.lock().await.select(&Latest).await?;
                    Ok(res)
                })
            }
        }
        ListByCategoryId(ValidIdentity<Category>) -> Vec<T> {
            fn handle(&mut self, msg: ListByCategoryId, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let res = repository.lock().await.list_by(&msg.0).await?;
                    Ok(res)
                })
            }
        }
    }
}

#[derive(Message, Clone, Debug)]
#[rtype(result = "()")]
pub enum SystemProductEvent {
    Created(Product),
    Updated(Product),
    Removed(IdentityOf<Product>),
}

#[derive(Debug, Clone)]
pub struct NotifyEvent<T>(T, Arc<Notify>)
where
    T: Message;

impl<T> Message for NotifyEvent<T>
where
    T: Message,
{
    type Result = ();
}

impl Handler<NotifyEvent<SystemProductEvent>> for ProductService {
    type Result = ResponseFuture<()>;
    fn handle(
        &mut self,
        msg: NotifyEvent<SystemProductEvent>,
        ctx: &mut Self::Context,
    ) -> Self::Result {
        let res = <Self as Handler<SystemProductEvent>>::handle(self, msg.0, ctx);
        Box::pin(async move {
            res.await;
            msg.1.notify_waiters();
        })
    }
}

impl Handler<SystemProductEvent> for ProductService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: SystemProductEvent, _ctx: &mut Self::Context) -> Self::Result {
        use SystemProductEvent::*;
        let repository = self.repository.clone();
        Box::pin(async move {
            let repository = repository.lock().await;
            match msg {
                Created(p) => {
                    repository.add(p).await.unwrap();
                }
                Updated(p) => {
                    repository.save(p).await.unwrap();
                }
                Removed(id) => {
                    repository.take(&id).await.unwrap();
                }
            }
        })
    }
}
