use super::service::{messages::*, ProductService};
use crate::catalog::service::messages::category as category_messages;
use crate::catalog::service::CatalogService;
use actix::Addr;
use actix_controllers::prelude::*;
use actix_session_identity::Identity as WebIdentity;
use askama::Template;
use derive_new::new;
use e_shop_core::catalog::model::Category;
use e_shop_core::product::model::Product;
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use typesafe_repository::GetIdentity;
use uuid::Uuid;

#[derive(Template, new)]
#[template(path = "product/info.html")]
struct Info {
    pub login: Option<String>,
    pub product: Product,
    pub category: Category,
}

#[get("/{id}")]
pub async fn info(
    product_service: Data<Arc<Addr<ProductService>>>,
    catalog_service: Data<Arc<Addr<CatalogService>>>,
    id: Path<String>,
    identity: Option<WebIdentity>,
) -> Response {
    let id = Uuid::try_parse(&id).map_err(|_| ControllerError::NotFound)?;
    let product = product_service
        .send(product::Get(id))
        .await??
        .ok_or(ControllerError::NotFound)?;
    let category = catalog_service
        .send(category_messages::GetByProductId(
            product.category.clone().into_inner(),
        ))
        .await??
        .ok_or(ControllerError::NotFound)?;
    let login = identity.map(|i| i.login);
    render_template(Info::new(login, product, category))
}

#[derive(Template, new)]
#[template(path = "product/add.html")]
struct AddProduct {
    pub login: Option<String>,
    pub categories: Vec<Category>,
}

#[get("/manage/product/add")]
pub async fn add(
    identity: WebIdentity,
    catalog_service: Data<Arc<Addr<CatalogService>>>,
) -> Response {
    let categories = catalog_service.send(category_messages::List()).await??;
    render_template(AddProduct::new(Some(identity.login), categories))
}

#[derive(Template, new)]
#[template(path = "product/delete.html")]
struct DeleteProduct {
    pub login: Option<String>,
    pub product: Product,
}

#[get("/manage/product/delete/{id}")]
pub async fn delete(
    service: Data<Arc<Addr<ProductService>>>,
    id: Path<String>,
    identity: Option<WebIdentity>,
) -> Response {
    let id = Uuid::try_parse(&id).map_err(|_| ControllerError::NotFound)?;
    let product = service.send(product::Get(id)).await??;
    match product {
        Some(p) => render_template(DeleteProduct::new(identity.map(|i| i.login), p)),
        None => Ok(HttpResponse::NotFound().body(())),
    }
}

#[derive(Template, new)]
#[template(path = "product/edit.html")]
struct EditProduct {
    pub login: Option<String>,
    pub product: Product,
    pub categories: Vec<Category>,
}

#[get("/manage/product/edit/{id}")]
pub async fn edit(
    product_service: Data<Arc<Addr<ProductService>>>,
    catalog_service: Data<Arc<Addr<CatalogService>>>,
    id: Path<Uuid>,
    identity: Option<WebIdentity>,
) -> Response {
    let login = identity.map(|i| i.login);
    let product = product_service
        .send(product::Get(*id))
        .await??
        .ok_or(ControllerError::NotFound)?;
    let categories = catalog_service.send(category_messages::List()).await??;
    render_template(EditProduct::new(login, product, categories))
}

#[derive(Serialize, Deserialize)]
pub struct EditProductDto {
    pub name: String,
    pub description: String,
}

#[post("/manage/product/edit/{id}")]
pub async fn edit_post(
    _product_service: Data<Arc<Addr<ProductService>>>,
    _: WebIdentity,
    id: Path<Uuid>,
    _dto: Query<EditProductDto>,
) -> Response {
    let id = id.into_inner();

    see_other(&format!("/product/{id}"), ())
}

#[derive(Template, new)]
#[template(path = "product/list.html")]
struct ListProducts {
    pub login: Option<String>,
    pub products: Vec<(Product, Category)>,
    pub prompt: Option<String>,
    pub sort_by: Option<String>,
    pub ordering: Option<String>,
    pub total_items: usize,
}

#[derive(Deserialize, Clone)]
pub struct ListProductsQuery {
    prompt: Option<String>,
    sort_by: String,
    ordering: String,
}

#[get("/manage/product/list")]
pub async fn list(
    product_service: Data<Arc<Addr<ProductService>>>,
    identity: WebIdentity,
    catalog_service: Data<Arc<Addr<CatalogService>>>,
    query: Option<Query<ListProductsQuery>>,
) -> Response {
    let login = Some(identity.login);
    let query = query.map(|q| q.into_inner());
    let products = product_service.send(product::List()).await??;
    let total_items = products.len();
    let products = match query.clone().and_then(|x| x.prompt) {
        Some(prompt) => products
            .into_iter()
            .filter(|p| p.name.contains(&prompt) || p.description.contains(&prompt))
            .collect(),
        None => products,
    };
    let mut res = vec![];
    for product in products {
        let category = catalog_service
            .send(category_messages::Get(
                product.category.clone().into_inner(),
            ))
            .await??
            .ok_or(ControllerError::NotFound)?;
        res.push((product, category));
    }
    if let Some(sort_by) = query.clone().map(|x| x.sort_by) {
        match sort_by.as_str() {
            "name" => res.sort_by_key(|(p, _)| p.name.clone()),
            "price" => res.sort_by_key(|(p, _)| p.price.clone()),
            "description" => res.sort_by_key(|(p, _)| p.description.clone()),
            "category" => res.sort_by_key(|(_, c)| c.name.clone()),
            "in_stock" => res.sort_by_key(|(p, _)| p.in_stock.clone()),
            "added" => res.sort_by_key(|(p, _)| p.added),
            _ => {
                return Err(ControllerError::InvalidInput {
                    field: "sort_by".to_string(),
                    msg: "unknown field".to_string(),
                })
            }
        };
    }
    let (sort_by, ordering) = query.clone().map(|x| (x.sort_by, x.ordering)).unzip();
    let prompt = query.clone().and_then(|x| x.prompt);
    render_template(ListProducts::new(
        login,
        res,
        prompt,
        sort_by,
        ordering,
        total_items,
    ))
}
