use super::service::ProductService;
use actix::Addr;
use async_trait::async_trait;
use derive_new::new;
use e_shop_core::catalog::model::Category;
use e_shop_core::product::messages as product_messages;
use e_shop_core::product::model::{Latest, Product};
use remote::product::{messages::*, RemoteProductResponseService};
use std::sync::Arc;
use typesafe_repository::prelude::*;
use typesafe_repository::ValidIdentity;
use typesafe_repository::async_ops::{Add, Update, Take, List, Get, Save, ListBy, Select};

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub trait ProductRepository:
    Add<Product>
    + Update<Product>
    + Take<Product>
    + List<Product>
    + Get<Product>
    + Save<Product>
    + Repository<Product, Error = Error>
    + ListBy<Product, ValidIdentity<Category>>
    + Select<Product, Latest>
{
}

#[derive(new)]
pub struct RemoteProductRepository {
    remote_service: Arc<Addr<RemoteProductResponseService<ProductService>>>,
}

impl Repository<Product> for RemoteProductRepository {
    type Error = Error;
}

#[async_trait]
impl Add<Product> for RemoteProductRepository {
    async fn add(&self, _: Product) -> Result<(), Error> {
        Ok(())
    }
}

#[async_trait]
impl Update<Product> for RemoteProductRepository {
    async fn update(&self, _: Product) -> Result<(), Error> {
        Ok(())
    }
}

#[async_trait]
impl Take<Product> for RemoteProductRepository {
    async fn take(&self, _: &IdentityOf<Product>) -> Result<Option<Product>, Error> {
        Ok(None)
    }
}

#[async_trait]
impl List<Product> for RemoteProductRepository {
    async fn list(&self) -> Result<Vec<Product>, Error> {
        Ok(self
            .remote_service
            .send::<InternalListMessage>(product_messages::List().into())
            .await??)
    }
}

#[async_trait]
impl Get<Product> for RemoteProductRepository {
    async fn get_one(&self, id: &IdentityOf<Product>) -> Result<Option<Product>, Error> {
        Ok(self
            .remote_service
            .send::<InternalGetMessage>(product_messages::Get(*id).into())
            .await??)
    }
}

#[async_trait]
impl Save<Product> for RemoteProductRepository {
    async fn save(&self, _: Product) -> Result<(), Error> {
        Ok(())
    }
}

#[async_trait]
impl ListBy<Product, ValidIdentity<Category>> for RemoteProductRepository {
    async fn list_by(&self, id: &ValidIdentity<Category>) -> Result<Vec<Product>, Error> {
        Ok(self
            .remote_service
            .send::<InternalListByCategoryIdMessage>(
                product_messages::ListByCategoryId(id.clone()).into(),
            )
            .await??)
    }
}

#[async_trait]
impl Select<Product, Latest> for RemoteProductRepository {
    async fn select(&self, _: &Latest) -> Result<Vec<Product>, Error> {
        Ok(self
            .remote_service
            .send::<InternalListTopMessage>(product_messages::ListTop(5).into())
            .await??)
    }
}

impl ProductRepository for RemoteProductRepository {}
