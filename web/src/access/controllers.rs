use crate::access::service::{messages, UserCredentialsService};
use actix::Addr;
use actix_controllers::prelude::*;
use actix_session::Session;
use actix_session_identity::Identity as WebIdentity;
use anyhow::Context;
use askama::Template;
use derive_more::Display;
use derive_new::new;
use e_shop_core::access::{
    model::{UserCredentials, UserCredentialsRole},
    service::generate_salt,
    types::{Login, Password},
};
use e_shop_core::NotEmpty;
use serde::Deserialize;
use std::sync::Arc;
use uuid::Uuid;

#[derive(Template, new)]
#[template(path = "register.html")]
struct Register {
    login: Option<String>,
    error: Option<String>,
}

#[derive(Deserialize)]
pub struct RegisterQuery {
    error: Option<RegisterError>,
}

#[derive(Deserialize, Display, Clone)]
#[serde(rename_all(deserialize = "snake_case"))]
pub enum RegisterError {
    #[display(fmt = "Login already in use")]
    LoginAlreadyInUse,
    #[display(fmt = "Password too short")]
    PasswordTooShort,
    #[display(fmt = "Login too short")]
    LoginTooShort,
    #[display(fmt = "Invalid email")]
    InvalidEmail,
    #[display(fmt = "Invalid phone_number")]
    InvalidPhoneNumber,
}

impl RegisterError {
    pub fn as_str(self) -> &'static str {
        match self {
            Self::LoginAlreadyInUse => "login_already_in_use",
            Self::PasswordTooShort => "password_too_short",
            Self::LoginTooShort => "login_too_short",
            Self::InvalidEmail => "invalid_email",
            Self::InvalidPhoneNumber => "invalid_phone_number",
        }
    }
}

#[get("/register")]
pub async fn register_page(
    q: Option<Query<RegisterQuery>>,
    identity: Option<WebIdentity>,
) -> Response {
    let error = q.and_then(|q| q.error.clone()).map(|err| format!("{err}"));
    let login = identity.map(|i| i.login);
    render_template(Register::new(login, error))
}

#[derive(Deserialize)]
pub struct UserCredentialsDto {
    pub login: String,
    pub password: String,
    pub email: String,
    pub phone_number: Option<String>,
}

impl TryInto<UserCredentials> for UserCredentialsDto {
    type Error = anyhow::Error;
    fn try_into(self) -> Result<UserCredentials, Self::Error> {
        let UserCredentialsDto {
            login,
            password,
            email,
            phone_number,
        } = self;
        let phone_number = match phone_number.as_ref().map_or(true, |n| n.is_empty()) {
            true => None,
            false => phone_number,
        };
        Ok(UserCredentials::new(
            Uuid::new_v4(),
            login.try_into().context("Failed to parse login")?,
            email.try_into().context("Failed to parse email")?,
            phone_number
                .map(|n| n.try_into())
                .transpose()
                .context("Failed to parse phone number")?,
            Password::generate(
                password.try_into().context("Failed to parse password")?,
                generate_salt(),
            )?,
            UserCredentialsRole::Default,
        ))
    }
}

#[post("/register")]
pub async fn register(
    form: Form<UserCredentialsDto>,
    service: Data<Arc<Addr<UserCredentialsService>>>,
) -> Response {
    let UserCredentialsDto {
        login,
        password,
        email,
        phone_number,
    } = form.into_inner();
    let login: Login = match login.try_into() {
        Ok(login) => login,
        Err(err) => {
            log::error!("Cannot parse login:\n{err:?}");
            return see_other(
                &format!("/register?error={}", RegisterError::LoginTooShort.as_str()),
                (),
            );
        }
    };
    if service
        .send(messages::user_credentials::GetByLogin(login.clone()))
        .await??
        .is_some()
    {
        return see_other("/register?error=login_already_in_use", "");
    }
    let phone_number = match phone_number.as_ref().map_or(true, |n| n.is_empty()) {
        true => None,
        false => phone_number,
    };
    let phone_number = match phone_number.map(|n| n.try_into()).transpose() {
        Ok(num) => num,
        Err(err) => {
            log::error!("Cannot parse phone number:\n{err:?}");
            return see_other(
                &format!(
                    "/register?error={}",
                    RegisterError::InvalidPhoneNumber.as_str()
                ),
                (),
            );
        }
    };
    let email = match email.try_into() {
        Ok(email) => email,
        Err(err) => {
            log::error!("Cannot parse email:\n{err:?}");
            return see_other(
                &format!("/register?error={}", RegisterError::InvalidEmail.as_str(),),
                (),
            );
        }
    };
    let password = Password::generate(
        password.try_into().context("Failed to parse password")?,
        generate_salt(),
    )
    .context("Cannot generate password")?;
    let c = UserCredentials::new(
        Uuid::new_v4(),
        login,
        email,
        phone_number,
        password,
        UserCredentialsRole::Default,
    );
    service.send(messages::user_credentials::Add(c)).await??;
    see_other("/login", "")
}

#[derive(Template, new)]
#[template(path = "login.html")]
struct LoginPage {
    err: Option<String>,
    login: Option<String>,
}

#[derive(Deserialize)]
pub struct LoginQuery {
    err: Option<String>,
}

#[get("/login")]
pub async fn login_page(q: Query<LoginQuery>, identity: Option<WebIdentity>) -> Response {
    let err = match &q.err {
        Some(msg) => match msg.is_empty() {
            true => Some("invalid login or password".to_string()),
            false => Some(msg.clone()),
        },
        None => None,
    };
    let login = identity.map(|x| x.login);
    render_template(LoginPage::new(err, login))
}

#[get("/logout")]
pub async fn logout(session: Session, identity: Option<WebIdentity>) -> Response {
    if identity.is_some() {
        session.remove("login");
    }
    see_other("/", ())
}

#[derive(Deserialize)]
pub struct LoginDto {
    pub login: String,
    pub password: NotEmpty,
}

#[post("/login")]
pub async fn log_in(
    service: Data<Arc<Addr<UserCredentialsService>>>,
    form: Form<LoginDto>,
    session: Session,
) -> Response {
    let login = form.login.clone().try_into();
    let login: Login = match login {
        Ok(login) => login,
        Err(_) => return see_other("/login?err=Invalid login format", ()),
    };
    let res = service
        .send(messages::user_credentials::GetByLogin(login.clone()))
        .await??;
    let c = match res {
        Some(c) => c,
        None => return see_other("/login?err", ()),
    };
    match c.password.check(&form.password) {
        Ok(true) => {
            let login: String = login.into();
            session.insert("login", login)?;
            see_other("/", ())
        }
        _ => see_other("/login?err", ()),
    }
}
