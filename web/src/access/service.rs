use super::repository::UserCredentialsRepository;
use actix::{Actor, Context, Handler, Message, ResponseFuture};
use actix_service::{messages, ServiceError};
use derive_new::new;
use e_shop_core::access::{
    model::{Role, UserCredentials},
    types::Login,
};
use remote::access::messages::*;
use std::sync::Arc;
use typesafe_repository::prelude::*;

#[derive(new)]
pub struct UserCredentialsService {
    repository: Box<Arc<dyn UserCredentialsRepository + Send + Sync + 'static>>,
}

impl Actor for UserCredentialsService {
    type Context = Context<Self>;
}

impl Handler<ExternalGetResponse> for UserCredentialsService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _: ExternalGetResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

impl Handler<ExternalGetByLoginResponse> for UserCredentialsService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _: ExternalGetByLoginResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

impl Handler<ExternalAddResponse> for UserCredentialsService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _: ExternalAddResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

impl Handler<ExternalGetRoleByLoginResponse> for UserCredentialsService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _: ExternalGetRoleByLoginResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

pub mod messages {
    use super::*;

    messages! {
        UserCredentialsService,
        mod user_credentials -> UserCredentials,
        Get(IdentityOf<UserCredentials>) -> Option<T> {
            fn handle(&mut self, Get(id): Get, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let res = repository.get_one(&id).await?;
                    Ok(res)
                })
            }
        },
        GetByLogin(Login) -> Option<T> {
            fn handle(&mut self, GetByLogin(login): GetByLogin, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let res = repository.get_by(&login).await?;
                    Ok(res)
                })
            }
        },
        Add(UserCredentials) -> () {
            fn handle(&mut self, Add(c): Add, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    repository.add(c).await?;
                    Ok(())
                })
            }
        },
        GetRoleByLogin(Login) -> Option<Role> {
            fn handle(&mut self, GetRoleByLogin(login): GetRoleByLogin, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let res = repository.find(&login).await?;
                    Ok(res)
                })
            }
        }
    }
}
