use super::service::UserCredentialsService;
use actix::Addr;
use async_trait::async_trait;
use derive_new::new;
use e_shop_core::access::messages as user_credentials_messages;
use e_shop_core::access::{
    model::{Role, UserCredentials},
    types::Login,
};
use remote::access::{messages::*, RemoteUserCredentialsResponseService};
use std::sync::Arc;
use typesafe_repository::prelude::*;
use typesafe_repository::Find;
use typesafe_repository::async_ops::{Get, GetBy, Add};

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub trait UserCredentialsRepository:
    Repository<UserCredentials, Error = Error>
    + Get<UserCredentials>
    + GetBy<UserCredentials, Login>
    + Add<UserCredentials>
    + Repository<Role, Error = Error>
    + Find<Role, Login>
{
}

#[derive(new)]
pub struct RemoteUserCredentialsRepository {
    remote_service: Arc<Addr<RemoteUserCredentialsResponseService<UserCredentialsService>>>,
}

impl Repository<UserCredentials> for RemoteUserCredentialsRepository {
    type Error = Error;
}

#[async_trait]
impl Get<UserCredentials> for RemoteUserCredentialsRepository {
    async fn get_one(
        &self,
        id: &IdentityOf<UserCredentials>,
    ) -> Result<Option<UserCredentials>, Error> {
        Ok(self
            .remote_service
            .send::<InternalGetMessage>(user_credentials_messages::Get(*id).into())
            .await??)
    }
}

#[async_trait]
impl GetBy<UserCredentials, Login> for RemoteUserCredentialsRepository {
    async fn get_by(&self, login: &Login) -> Result<Option<UserCredentials>, Error> {
        Ok(self
            .remote_service
            .send::<InternalGetByLoginMessage>(
                user_credentials_messages::GetByLogin(login.clone()).into(),
            )
            .await??)
    }
}

#[async_trait]
impl Add<UserCredentials> for RemoteUserCredentialsRepository {
    async fn add(&self, c: UserCredentials) -> Result<(), Error> {
        Ok(self
            .remote_service
            .send::<InternalAddMessage>(user_credentials_messages::Add(c).into())
            .await??)
    }
}

#[async_trait]
impl Find<Role, Login> for RemoteUserCredentialsRepository {
    async fn find(&self, l: &Login) -> Result<Option<Role>, Error> {
        Ok(self
            .remote_service
            .send::<InternalGetRoleByLoginMessage>(
                user_credentials_messages::GetRoleByLogin(l.clone()).into(),
            )
            .await??)
    }
}

impl Repository<Role> for RemoteUserCredentialsRepository {
    type Error = Error;
}

impl UserCredentialsRepository for RemoteUserCredentialsRepository {}
