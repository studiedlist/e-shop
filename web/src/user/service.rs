use super::repository::UserRepository;
use actix::Message;
use actix::{Actor, Context, Handler, ResponseFuture};
use actix_service::{messages, ServiceError};
use derive_new::new;
use e_shop_core::user::model::User;
use remote::user::messages::*;
use std::sync::Arc;
use typesafe_repository::IdentityOf;

#[derive(new)]
pub struct UserService {
    repository: Box<Arc<dyn UserRepository + Send + Sync + 'static>>,
}

impl Actor for UserService {
    type Context = Context<Self>;
}

impl Handler<ExternalGetResponse> for UserService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _: ExternalGetResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

impl Handler<ExternalAddResponse> for UserService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _: ExternalAddResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

pub mod messages {
    use super::*;

    messages! {
        UserService,
        mod user -> User,
        Get(IdentityOf<User>) -> Option<T> {
            fn handle(&mut self, Get(id): Get, _: &mut Self::Context) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let res = repository.get_one(&id).await?;
                    Ok(res)
                })
            }
        },
        Add(User) -> () {
            fn handle(&mut self, Add(u): Add, _: &mut Self::Context) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    repository.add(u).await?;
                    Ok(())
                })
            }
        }

    }
}
