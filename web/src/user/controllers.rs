use crate::access::service::{
    messages::user_credentials as user_credentials_messages, UserCredentialsService,
};
use crate::cart::service::{
    purchase::messages::purchase as purchase_messages, purchase::PurchaseService,
};
use crate::catalog::service::{messages::category as category_messages, CatalogService};
use crate::product::service::{messages::product as product_messages, ProductService};
use actix::Addr;
use actix_controllers::prelude::*;
use actix_session_identity::Identity as WebIdentity;
use anyhow::Context;
use askama::Template;
use derive_new::new;
use e_shop_core::cart::model::Purchase;
use e_shop_core::catalog::model::Category;
use e_shop_core::product::model::Product;
use std::sync::Arc;
use typesafe_repository::{GetIdentity, ValidIdentity};
use uuid::Uuid;

#[derive(Template, new)]
#[template(path = "me.html")]
pub struct UserPage {
    user_login: String,
    login: Option<String>,
    purchase_history: Vec<Purchase>,
}

#[get("/me")]
pub async fn user_page(
    identity: WebIdentity,
    user_credentials_service: Data<Arc<Addr<UserCredentialsService>>>,
    purchase_service: Data<Arc<Addr<PurchaseService>>>,
) -> Response {
    let login = identity.login;
    let user_credentials = user_credentials_service
        .send(user_credentials_messages::GetByLogin(
            login
                .clone()
                .try_into()
                .context("Cannot convert to login")?,
        ))
        .await??
        .ok_or(ControllerError::NotFound)?;
    let purchase_history = purchase_service
        .send(purchase_messages::ListByUserId(ValidIdentity::of(
            &user_credentials,
        )))
        .await??;
    render_template(UserPage::new(login.clone(), Some(login), purchase_history))
}

#[derive(Template, new)]
#[template(path = "manage.html")]
pub struct Manage {
    login: Option<String>,
}

#[get("/manage")]
pub async fn manage_application(
    identity: WebIdentity,
    user_credentials_service: Data<Arc<Addr<UserCredentialsService>>>,
) -> Response {
    let login = identity.login;
    let role = user_credentials_service
        .send(user_credentials_messages::GetRoleByLogin(
            login
                .clone()
                .try_into()
                .context("Cannot convert to login")?,
        ))
        .await??;
    println!("{role:?}");
    let res = role.map(|r| r.name.to_string() == "admin").unwrap_or(false);
    if !res {
        return Err(ControllerError::Unauthorized);
    }
    let login = Some(login);
    render_template(Manage::new(login))
}

#[derive(Template, new)]
#[template(path = "manage/categories.html")]
pub struct ManageCategories {
    login: Option<String>,
    categories: Vec<Category>,
}

#[get("/manage/category/list")]
pub async fn list_categories(
    identity: WebIdentity,
    catalog_service: Data<Arc<Addr<CatalogService>>>,
) -> Response {
    let login = Some(identity.login);
    let categories = catalog_service.send(category_messages::List()).await??;
    render_template(ManageCategories::new(login, categories))
}

#[derive(Template, new)]
#[template(path = "category/add.html")]
struct CategoryAddTemplate {
    pub login: Option<String>,
    pub categories: Vec<Category>,
}

#[get("/manage/category/add")]
pub async fn add_category(
    identity: WebIdentity,
    catalog_service: Data<Arc<Addr<CatalogService>>>,
) -> Response {
    let login = Some(identity.login);
    let categories = catalog_service.send(category_messages::List()).await??;
    render_template(CategoryAddTemplate::new(login, categories))
}

#[derive(Template, new)]
#[template(path = "category/delete.html")]
struct CategoryDeleteTemplate {
    pub login: Option<String>,
    pub category: Category,
    pub categories: Vec<Category>,
    pub items: Vec<Product>,
}

#[get("/manage/category/delete/{id}")]
pub async fn delete_category(
    id: Path<Uuid>,
    identity: WebIdentity,
    catalog_service: Data<Arc<Addr<CatalogService>>>,
    product_service: Data<Arc<Addr<ProductService>>>,
) -> Response {
    let login = Some(identity.login);
    let id = id.into_inner();
    let category = catalog_service
        .send(category_messages::Get(id))
        .await??
        .ok_or(ControllerError::NotFound)?;
    let id = ValidIdentity::of(&category);
    let categories = catalog_service
        .send(category_messages::List())
        .await??
        .into_iter()
        .filter(|c| c.id != category.id)
        .collect();
    let products = product_service
        .send(product_messages::ListByCategoryId(id))
        .await??;
    render_template(CategoryDeleteTemplate::new(
        login, category, categories, products,
    ))
}
