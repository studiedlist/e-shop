use super::service::UserService;
use actix::Addr;
use async_trait::async_trait;
use derive_new::new;
use e_shop_core::user::messages as user_messages;
use e_shop_core::user::model::User;
use remote::user::{messages::*, RemoteUserResponseService};
use std::sync::Arc;
use typesafe_repository::async_ops::{Add, Get};
use typesafe_repository::prelude::*;

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub trait UserRepository: Repository<User, Error = Error> + Add<User> + Get<User> {}

#[derive(new)]
pub struct RemoteUserRepository {
    remote_service: Arc<Addr<RemoteUserResponseService<UserService>>>,
}

impl Repository<User> for RemoteUserRepository {
    type Error = Error;
}

#[async_trait]
impl Add<User> for RemoteUserRepository {
    async fn add(&self, _u: User) -> Result<(), Error> {
        Ok(())
    }
}

#[async_trait]
impl Get<User> for RemoteUserRepository {
    async fn get_one(&self, id: &IdentityOf<User>) -> Result<Option<User>, Error> {
        Ok(self
            .remote_service
            .send::<InternalGetMessage>(user_messages::Get(*id).into())
            .await??)
    }
}

impl UserRepository for RemoteUserRepository {}
