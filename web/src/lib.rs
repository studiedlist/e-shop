pub mod access;
pub mod cart;
pub mod catalog;
pub mod controllers;
pub mod product;
pub mod service;
pub mod user;

use serde::Deserialize;

#[derive(Deserialize)]
pub struct Sort {
    field: String,
    order: SortOrder,
}

#[derive(Deserialize)]
pub enum SortOrder {
    Ascending,
    Descending,
}
