use super::service::CatalogService;
use actix::Addr;
use async_trait::async_trait;
use derive_new::new;
use e_shop_core::catalog::{
    messages::{
        Add as AddMessage, Get as GetMessage, List as ListMessage, Remove as RemoveMessage,
        Update as UpdateMessage,
    },
    model::Category,
};
use remote::catalog::{
    messages::{
        InternalAddMessage, InternalGetMessage, InternalListMessage, InternalRemoveMessage,
        InternalUpdateMessage,
    },
    RemoteCategoryResponseService,
};
use std::sync::Arc;
use typesafe_repository::prelude::*;
use typesafe_repository::{Add, Update, Take, List, Get, Save};

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub trait CategoryRepository:
    Add<Category>
    + Update<Category>
    + Take<Category>
    + List<Category>
    + Get<Category>
    + Save<Category>
    + Repository<Category, Error = Error>
{
}

#[derive(new)]
pub struct RemoteCategoryRepository {
    remote_service: Arc<Addr<RemoteCategoryResponseService<CatalogService>>>,
}

impl Repository<Category> for RemoteCategoryRepository {
    type Error = Error;
}

#[async_trait]
impl Add<Category> for RemoteCategoryRepository {
    async fn add(&self, c: Category) -> Result<(), Error> {
        Ok(self
            .remote_service
            .send::<InternalAddMessage>(AddMessage(c).into())
            .await??)
    }
}

#[async_trait]
impl Update<Category> for RemoteCategoryRepository {
    async fn update(&self, c: Category) -> Result<(), Error> {
        Ok(self
            .remote_service
            .send::<InternalUpdateMessage>(UpdateMessage(c).into())
            .await??)
    }
}

#[async_trait]
impl Take<Category> for RemoteCategoryRepository {
    async fn take(&self, id: &IdentityOf<Category>) -> Result<Option<Category>, Error> {
        self.remote_service
            .send::<InternalRemoveMessage>(RemoveMessage(*id).into())
            .await??;
        Ok(None)
    }
}

#[async_trait]
impl List<Category> for RemoteCategoryRepository {
    async fn list(&self) -> Result<Vec<Category>, Error> {
        Ok(self
            .remote_service
            .send::<InternalListMessage>(ListMessage().into())
            .await??)
    }
}

#[async_trait]
impl Get<Category> for RemoteCategoryRepository {
    async fn get_one(&self, id: &IdentityOf<Category>) -> Result<Option<Category>, Error> {
        Ok(self
            .remote_service
            .send::<InternalGetMessage>(GetMessage(*id).into())
            .await??)
    }
}

#[async_trait]
impl Save<Category> for RemoteCategoryRepository {
    async fn save(&self, _: Category) -> Result<(), Error> {
        Ok(())
    }
}

impl CategoryRepository for RemoteCategoryRepository {}
