use super::service::CatalogService;
use crate::product::service::ProductService;
use actix_controllers::prelude::*;
use askama::Template;
use e_shop_core::catalog::{
    acl::Product,
    model::{Category, NewsEntry},
};

use crate::catalog::service::messages::category as category_messages;
use crate::product::service::messages::product as product_messages;
use actix::Addr;
use actix_session_identity::Identity as WebIdentity;
use derive_new::new;
use std::sync::Arc;
use typesafe_repository::ValidIdentity;
use uuid::Uuid;

#[derive(Template, new)]
#[template(path = "catalog.html")]
struct Catalog {
    pub login: Option<String>,
    pub categories: Vec<Category>,
}

#[get("")]
pub async fn catalog(
    catalog_service: Data<Arc<Addr<CatalogService>>>,
    identity: Option<WebIdentity>,
) -> Response {
    let categories = catalog_service
        .send(super::service::messages::category::List())
        .await??;
    let login = identity.map(|i| i.login);
    render_template(Catalog::new(login, categories))
}

#[derive(Template, new)]
#[template(path = "category.html")]
struct CategoryTemplate {
    pub login: Option<String>,
    pub category: Category,
    pub products: Vec<Product>,
}

#[get("/{name:.*}")]
pub async fn category_page(
    name: Either<Path<Uuid>, Path<String>>,
    product_service: Data<Arc<Addr<ProductService>>>,
    catalog_service: Data<Arc<Addr<CatalogService>>>,
    identity: Option<WebIdentity>,
) -> Response {
    match name {
        Either::Left(id) => {
            let login = identity.map(|i| i.login);
            let id = id.into_inner();
            let category = catalog_service
                .send(category_messages::Get(id))
                .await??
                .ok_or(ControllerError::NotFound)?;
            let products = product_service
                .send(product_messages::ListByCategoryId(ValidIdentity::of(
                    &category,
                )))
                .await??
                .into_iter()
                .map(|x| (x, vec![]).into())
                .collect();
            render_template(CategoryTemplate::new(login, category, products))
        }
        Either::Right(name) => {
            let categories: Vec<_> = name.split('/').collect();
            Ok(HttpResponse::Ok().json(categories))
        }
    }
}

#[derive(Template, new)]
#[template(path = "news.html")]
struct NewsTemplate {
    pub login: Option<String>,
    pub news: Vec<NewsEntry>,
}

#[get("/news")]
pub async fn news_page(identity: Option<WebIdentity>) -> Response {
    let login = identity.map(|i| i.login);
    render_template(NewsTemplate::new(
        login,
        vec![NewsEntry {
            name: "abc".try_into().unwrap(),
            description: "cde".try_into().unwrap(),
        }],
    ))
}
