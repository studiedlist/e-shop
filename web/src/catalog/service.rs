use super::repository::CategoryRepository;
use actix::{Actor, Context, Handler, Message, ResponseFuture};
use actix_service::{messages, ServiceError};
use derive_new::new;
use e_shop_core::catalog::model::Category;
use e_shop_core::product::model::Product;
use remote::catalog::messages::*;
use std::sync::Arc;
use tokio::sync::Mutex;
use typesafe_repository::IdentityOf;

#[derive(new)]
pub struct CatalogService {
    repository: Box<Arc<Mutex<dyn CategoryRepository>>>,
}

impl Actor for CatalogService {
    type Context = Context<Self>;
}

impl Handler<ExternalGetResponse> for CatalogService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _msg: ExternalGetResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

impl Handler<ExternalListResponse> for CatalogService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _msg: ExternalListResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

impl Handler<ExternalUpdateResponse> for CatalogService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _: ExternalUpdateResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

impl Handler<ExternalAddResponse> for CatalogService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _: ExternalAddResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

impl Handler<ExternalRemoveResponse> for CatalogService {
    type Result = ResponseFuture<()>;

    fn handle(&mut self, _: ExternalRemoveResponse, _: &mut Self::Context) -> Self::Result {
        Box::pin(async move {})
    }
}

pub mod messages {

    use super::*;

    messages! {
        CatalogService,
        mod category -> Category,
        Add(Category) -> () {
            fn handle(&mut self, Add(category): Add, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.lock().await.add(category).await?)
                })
            }
        },
        Get(IdentityOf<Category>) -> Option<T> {
            fn handle(&mut self, Get(id): Get, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    Ok(repository.lock().await.get_one(&id).await?)
                })
            }
        },
        List() -> Vec<T> {
            fn handle(&mut self, _: List, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                Box::pin(async move {
                    let res = repository.lock().await.list().await?;
                    Ok(res)
                })
            }
        }
        GetByProductId(IdentityOf<Product>) -> Option<T> {
            fn handle(&mut self, GetByProductId(id): GetByProductId, _: &mut Context<Self>) -> Self::Result {
                let repository = self.repository.clone();
                //todo
                Box::pin(async move {
                    let res = repository.lock().await.get_one(&id).await?;
                    Ok(res)
                })
            }
        },
        FindByHierarchy(Vec<String>) -> Option<T> {
            fn handle(&mut self, FindByHierarchy(_seq): FindByHierarchy, _: &mut Context<Self>) -> Self::Result {
                Box::pin(async move {

                    Ok(None)
                })
            }
        }
    }
}
