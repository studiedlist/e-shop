use actix::Addr;
use async_trait::async_trait;
use derive_new::new;
use std::sync::Arc;
use typesafe_repository::prelude::*;
use typesafe_repository::{Add, Get};

type Error = Box<dyn std::error::Error + Send + Sync + 'static>;

pub mod cart {
    use super::*;
    use e_shop_core::cart::{
        messages::cart::{Add as AddMessage, Get as GetMessage},
        model::Cart,
    };
    use remote::cart::{
        messages::{InternalAddMessage, InternalGetMessage},
        RemoteCartResponseService,
    };

    pub trait CartRepository: Add<Cart> + Get<Cart> + Repository<Cart, Error = Error> {}

    #[derive(new)]
    pub struct RemoteCartRepository {
        remote_service: Arc<Addr<RemoteCartResponseService>>,
    }

    impl Repository<Cart> for RemoteCartRepository {
        type Error = Error;
    }

    #[async_trait]
    impl Add<Cart> for RemoteCartRepository {
        async fn add(&self, c: Cart) -> Result<(), Error> {
            Ok(self
                .remote_service
                .send::<InternalAddMessage>(AddMessage(c).into())
                .await??)
        }
    }

    #[async_trait]
    impl Get<Cart> for RemoteCartRepository {
        async fn get_one(&self, id: &IdentityOf<Cart>) -> Result<Option<Cart>, Error> {
            Ok(self
                .remote_service
                .send::<InternalGetMessage>(GetMessage(*id).into())
                .await??)
        }
    }

    impl CartRepository for RemoteCartRepository {}
}

pub mod purchase {
    use super::*;
    use e_shop_core::access::model::UserCredentials;
    use e_shop_core::cart::{
        messages::purchase::{Add as AddMessage, Get as GetMessage, ListByUserId},
        model::Purchase,
    };
    use remote::purchase::{messages::*, RemotePurchaseResponseService};
    use typesafe_repository::{ListBy, ValidIdentity};

    pub trait PurchaseRepository:
        Repository<Purchase, Error = Error>
        + Add<Purchase>
        + Get<Purchase>
        + ListBy<Purchase, Option<ValidIdentity<UserCredentials>>>
    {
    }

    #[derive(new)]
    pub struct RemotePurchaseRepository {
        remote_service: Arc<Addr<RemotePurchaseResponseService>>,
    }

    impl Repository<Purchase> for RemotePurchaseRepository {
        type Error = Error;
    }

    impl PurchaseRepository for RemotePurchaseRepository {}

    #[async_trait]
    impl Add<Purchase> for RemotePurchaseRepository {
        async fn add(&self, p: Purchase) -> Result<(), Self::Error> {
            Ok(self
                .remote_service
                .send::<InternalAddMessage>(AddMessage(p).into())
                .await??)
        }
    }

    #[async_trait]
    impl Get<Purchase> for RemotePurchaseRepository {
        async fn get_one(
            &self,
            id: &IdentityOf<Purchase>,
        ) -> Result<Option<Purchase>, Self::Error> {
            Ok(self
                .remote_service
                .send::<InternalGetMessage>(GetMessage(*id).into())
                .await??)
        }
    }

    #[async_trait]
    impl ListBy<Purchase, Option<ValidIdentity<UserCredentials>>> for RemotePurchaseRepository {
        async fn list_by(
            &self,
            id: &Option<ValidIdentity<UserCredentials>>,
        ) -> Result<Vec<Purchase>, Self::Error> {
            Ok(self
                .remote_service
                .send::<InternalListByUserIdMessage>(ListByUserId(id.clone().unwrap()).into())
                .await??)
        }
    }
}

pub use cart::{CartRepository, RemoteCartRepository};
pub use purchase::{PurchaseRepository, RemotePurchaseRepository};
