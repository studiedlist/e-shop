use crate::access::service::{
    messages::user_credentials as user_credentials_messages, UserCredentialsService,
};
use crate::cart::service::{
    cart::messages::cart as cart_messages, purchase::messages::purchase as purchase_messages,
};
use crate::cart::service::{CartService, PurchaseService};
use crate::product::service::{messages::product as product_messages, ProductService};
use actix::Addr;
use actix_controllers::prelude::*;
use actix_session::Session;
use actix_session_identity::Identity as WebIdentity;
use anyhow::{anyhow, Context};
use askama::Template;
use chrono::Utc;
use derive_new::new;
use e_shop_core::cart::{
    self,
    model::{ContactInfo, Purchase},
};
use e_shop_core::{Address, TypeCreationError};
use serde::Deserialize;
use std::collections::HashMap;
use std::sync::Arc;
use typesafe_repository::prelude::*;
use typesafe_repository::ValidIdentity;
use uuid::{Error, Uuid};

#[derive(Template, new)]
#[template(path = "cart/cart.html")]
struct Cart {
    pub login: Option<String>,
    pub cart: Option<cart::model::EnrichedCart>,
}

impl Cart {
    pub fn cart_is_empty(&self) -> bool {
        self.cart
            .as_ref()
            .map(|c| c.items.is_empty())
            .unwrap_or(true)
    }
}

#[derive(Template, new)]
#[template(path = "cart/success.html")]
struct Success {
    pub login: Option<String>,
}

#[derive(Deserialize)]
pub struct AddToCart {
    id: Uuid,
}

#[post("/add")]
pub async fn add_to_cart(s: Session, q: Query<AddToCart>) -> Response {
    match s.get::<Vec<(Uuid, usize)>>("cart_products")? {
        Some(mut vec) => {
            let vec = match vec.iter().find(|(x, _)| *x == q.id) {
                Some(_) => vec
                    .into_iter()
                    .map(|(x, c)| if x == q.id { (x, c + 1) } else { (x, c) })
                    .collect(),
                None => {
                    vec.push((q.id, 1));
                    vec
                }
            };
            s.insert("cart_products", vec)?;
        }
        None => {
            s.insert("cart_products", vec![(q.id, 1)])?;
        }
    }
    see_other("/cart", ())
}

#[post("/remove")]
pub async fn remove_from_cart(s: Session, q: Query<AddToCart>) -> Response {
    if let Some(vec) = s.get::<Vec<(Uuid, usize)>>("cart_products")? {
        let vec: Vec<_> = vec.iter().filter(|(id, _)| *id != q.id).collect();
        s.insert("cart_products", vec)?;
    }
    see_other("/cart", ())
}

#[derive(Deserialize)]
pub struct ConfirmationForm {
    first_name: String,
    last_name: String,
    email: String,
    phone_number: Option<String>,
    // address_street: String,
    // address_city: String,
}

impl TryInto<ContactInfo> for ConfirmationForm {
    type Error = TypeCreationError;

    fn try_into(self) -> Result<ContactInfo, Self::Error> {
        Ok(ContactInfo::new(
            self.phone_number
                .map(|x| x.try_into())
                .transpose()
                .ok()
                .flatten(),
            self.first_name.try_into()?,
            self.last_name.try_into()?,
            self.email.try_into()?,
            // Address::new(self.address_city, self.address_street),
            Address::new("".to_string(), "".to_string()),
        ))
    }
}

#[post("/confirm")]
pub async fn confirm_cart(
    s: Session,
    form: Form<ConfirmationForm>,
    i: Option<WebIdentity>,
    purchase_service: Data<Arc<Addr<PurchaseService>>>,
    cart_service: Data<Arc<Addr<CartService>>>,
    user_credentials_service: Data<Arc<Addr<UserCredentialsService>>>,
) -> Response {
    if let Some(cart_id) = s
        .get::<IdentityOf<e_shop_core::cart::model::Cart>>("cart_id")
        .map_err(|e| anyhow!(e))?
    {
        let contact_info: ContactInfo = form
            .into_inner()
            .try_into()
            .context("Cannot parse contact info")?;
        let cart = cart_service
            .send(cart_messages::Get(cart_id))
            .await??
            .ok_or(ControllerError::NotFound)?;
        let user_id = match i.map(|i| i.login) {
            Some(login) => {
                let user = user_credentials_service
                    .send(user_credentials_messages::GetByLogin(
                        login.try_into().context("Unable to parse login")?,
                    ))
                    .await??
                    .ok_or(ControllerError::NotFound)?;
                Some(ValidIdentity::of(&user))
            }
            None => None,
        };
        let purchase = Purchase::new(
            Uuid::new_v4(),
            user_id,
            ValidIdentity::of(&cart),
            contact_info,
            Utc::now().naive_utc(),
        );
        purchase_service
            .send(purchase_messages::Add(purchase))
            .await??;
    } else {
        return Err(ControllerError::NotFound);
    }
    s.remove("cart_products");
    see_other("/cart/success", ())
}

#[derive(Template, new)]
#[template(path = "cart/info.html")]
pub struct CartInfo {
    login: Option<String>,
    cart: Option<cart::model::EnrichedCart>,
}

#[get("/{id}")]
pub async fn cart_info(
    id: Path<Uuid>,
    cart_service: Data<Arc<Addr<CartService>>>,
    product_service: Data<Arc<Addr<ProductService>>>,
    identity: Option<WebIdentity>,
) -> Response {
    let cart = cart_service
        .send(cart_messages::Get(id.into_inner()))
        .await??;
    let mut items = vec![];
    for (id, _) in cart.as_ref().map(|c| c.items.clone()).unwrap_or(vec![]) {
        let r = product_service
            .send(product_messages::Get(id))
            .await??
            .map(|p| p.try_into().unwrap())
            .ok_or(ControllerError::NotFound)
            .context("product not found")?;
        items.push(r);
    }
    let cart = cart
        .map(|c| c.enrich(items))
        .transpose()
        .map_err(|err| anyhow::anyhow!(err))?;
    let login = identity.map(|i| i.login);
    render_template(CartInfo::new(login, cart))
}

#[get("/success")]
pub async fn success(identity: Option<WebIdentity>) -> Response {
    render_template(Success::new(identity.map(|i| i.login)))
}

#[get("")]
pub async fn cart_page(
    s: Session,
    i: Option<WebIdentity>,
    service: Data<Arc<Addr<ProductService>>>,
) -> Response {
    let ids: Option<Vec<(Uuid, usize)>> = s.get("cart_products").map_err(|e| anyhow!(e))?;
    let cart = ids.as_ref().map(|items| cart::model::Cart {
        id: Uuid::new_v4(),
        items: items.clone(),
    });
    let items = match ids {
        Some(ids) => {
            let mut res = vec![];
            for (id, _) in ids {
                let r = service
                    .send(product_messages::Get(id))
                    .await??
                    .map(|p| p.try_into().unwrap());
                if let Some(r) = r {
                    res.push(r);
                }
            }
            res
        }
        None => vec![],
    };
    let cart = cart
        .map(|c| c.enrich(items))
        .transpose()
        .map_err(|err| ControllerError::InternalServerError(anyhow::anyhow!(err)))?;
    render_template(Cart {
        login: i.map(|i| i.login),
        cart,
    })
}

#[derive(Template, new)]
#[template(path = "cart/user_details.html")]
pub struct UserDetails {
    pub login: Option<String>,
    pub cart: Option<cart::model::EnrichedCart>,
}

pub type UserDetailsForm = HashMap<String, String>;

#[post("/user_details")]
pub async fn user_details(
    s: Session,
    form: Form<UserDetailsForm>,
    product_service: Data<Arc<Addr<ProductService>>>,
    cart_service: Data<Arc<Addr<CartService>>>,
    identity: Option<WebIdentity>,
) -> Response {
    pub fn parse_user_details_form(f: UserDetailsForm) -> Result<Vec<Uuid>, Error> {
        f.iter()
            .filter(|(x, _)| x.contains("items"))
            .map(|(_, x)| Uuid::parse_str(x))
            .collect()
    }
    let form = parse_user_details_form(form.into_inner()).context("Uuid cannot be parsed")?;
    let session_products = s
        .get::<Vec<(Uuid, usize)>>("cart_products")?
        .ok_or(ControllerError::NotFound)
        .context("cart_products not found")?;
    let items = form
        .iter()
        .map(|id| {
            let count = session_products
                .iter()
                .find(|(i, _)| i == id)
                .map(|(_, c)| *c)?;
            Some((*id, count))
        })
        .collect::<Option<Vec<_>>>();
    let cart = items.map(|items| cart::model::Cart {
        id: Uuid::new_v4(),
        items,
    });
    let mut items = vec![];
    for id in form {
        let r = product_service
            .send(product_messages::Get(id))
            .await??
            .map(|p| p.try_into().unwrap())
            .ok_or(ControllerError::NotFound)
            .context("product not found")?;
        items.push(r);
    }

    if let Some(cart) = &cart {
        let msg = cart_messages::Add(cart.clone());
        cart_service.send(msg).await??;
        s.insert("cart_id", cart.id())?;
    }

    let cart = cart
        .map(|c| c.enrich(items))
        .transpose()
        .map_err(|err| ControllerError::InternalServerError(anyhow::anyhow!(err)))?;
    let login = identity.map(|i| i.login);
    render_template(UserDetails::new(login, cart))
}
