use super::repository::{CartRepository, PurchaseRepository};
use actix::{Actor, Context, Handler, Message, ResponseFuture};
use actix_service::{messages, ServiceError};
use derive_new::new;
use e_shop_core::cart::model::Cart;
use std::sync::Arc;
use typesafe_repository::IdentityOf;

pub mod cart {
    use super::*;

    #[derive(new)]
    pub struct CartService {
        repository: Box<Arc<dyn CartRepository>>,
    }

    impl Actor for CartService {
        type Context = Context<Self>;
    }

    pub mod messages {
        use super::*;

        messages! {
            CartService,
            mod cart -> Cart,
            Add(Cart) -> () {
                fn handle(&mut self, Add(cart): Add, _: &mut Self::Context) -> Self::Result {
                    let repository = self.repository.clone();
                    Box::pin(async move {
                        Ok(repository.add(cart).await?)
                    })
                }
            },
            Get(IdentityOf<Cart>) -> Option<T> {
                fn handle(&mut self, Get(id): Get, _: &mut Self::Context) -> Self::Result {
                    let repository = self.repository.clone();
                    Box::pin(async move {
                        Ok(repository.get_one(&id).await?)
                    })
                }
            },
        }
    }
}

pub use cart::CartService;
pub use purchase::PurchaseService;

pub mod purchase {
    use super::*;
    use e_shop_core::access::model::UserCredentials;
    use e_shop_core::cart::model::Purchase;
    use typesafe_repository::ValidIdentity;

    #[derive(new)]
    pub struct PurchaseService {
        repository: Arc<dyn PurchaseRepository>,
    }

    impl Actor for PurchaseService {
        type Context = Context<Self>;
    }

    pub mod messages {
        use super::*;

        messages! {
            PurchaseService,
            mod purchase -> Purchase,
            Get(IdentityOf<Purchase>) -> Option<T> {
                fn handle(&mut self, Get(id): Get, _: &mut Self::Context) -> Self::Result {
                    let repository = self.repository.clone();
                    Box::pin(async move {
                        Ok(repository.get_one(&id).await?)
                    })
                }
            },
            Add(Purchase) -> () {
                fn handle(&mut self, Add(purchase): Add, _: &mut Self::Context) -> Self::Result {
                    let repository = self.repository.clone();
                    Box::pin(async move {
                        Ok(repository.add(purchase).await?)
                    })
                }
            },
            ListByUserId(ValidIdentity<UserCredentials>) -> Vec<Purchase> {
                fn handle(&mut self, ListByUserId(id): ListByUserId, _: &mut Self::Context) -> Self::Result {
                    let repository = self.repository.clone();
                    Box::pin(async move {
                        Ok(repository.list_by(&Some(id)).await?)
                    })
                }
            }
        }
    }
}
