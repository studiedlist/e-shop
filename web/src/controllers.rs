use crate::catalog::{self, service::CatalogService};
use crate::product::{self, service::ProductService};
use actix::Addr;
use actix_controllers::prelude::*;
use actix_session_identity::Identity as WebIdentity;
use askama::Template;
use derive_new::new;
use e_shop_core::catalog::model::Category;
use e_shop_core::product::model::Product;
use std::sync::Arc;
use typesafe_repository::GetIdentity;

#[derive(Template, new)]
#[template(path = "index.html")]
struct Index {
    pub products: Vec<Product>,
    pub categories: Vec<Category>,
    pub login: Option<String>,
}

#[get("/")]
pub async fn index(
    product_service: Data<Arc<Addr<ProductService>>>,
    catalog_service: Data<Arc<Addr<CatalogService>>>,
    identity: Option<WebIdentity>,
) -> Response {
    let products = product_service
        .send(product::service::messages::product::ListTop)
        .await??;
    let categories = catalog_service
        .send(catalog::service::messages::category::List())
        .await??;
    let login = identity.map(|i| i.login);
    render_template(Index::new(products, categories, login))
}

#[derive(Template, new)]
#[template(path = "not_found.html")]
pub struct NotFound {
    pub login: Option<String>,
}

pub async fn not_found(
    identity: Option<WebIdentity>,
) -> Result<HttpResponse, Box<dyn std::error::Error>> {
    let login = identity.map(|i| i.login);
    Ok(render_template(NotFound::new(login))?)
}

#[derive(Template, new)]
#[template(path = "unauthorized.html")]
pub struct Unauthorized {
    pub login: Option<String>,
}
