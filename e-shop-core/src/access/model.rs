use super::types::{Login, Password};
use crate::{Email, NotEmpty, PhoneNumber};
use derive_more::Constructor;
use derive_new::new;
use serde::{Deserialize, Serialize};
use typesafe_repository::macros::Id;
use typesafe_repository::{GetIdentity, Identity, IdentityBy, IdentityOf, RefIdentity, SelectBy};
use uuid::Uuid;

#[derive(Id, Serialize, Deserialize, Constructor, Clone, Debug, PartialEq)]
#[Id(get_id, ref_id)]
pub struct UserCredentials {
    pub id: Uuid,
    pub login: Login,
    pub email: Email,
    pub phone_number: Option<PhoneNumber>,
    pub password: Password,
    pub role: UserCredentialsRole,
}

impl IdentityBy<Login> for UserCredentials {
    fn id_by(&self) -> Login {
        self.login.clone()
    }
}

impl SelectBy<Login> for Role {}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum UserCredentialsRole {
    Default,
    Role(IdentityOf<Role>),
}

#[derive(Id, Clone, Debug, Serialize, Deserialize, PartialEq, Eq, new)]
#[Id(get_id, ref_id)]
pub struct Role {
    pub id: Uuid,
    pub name: NotEmpty,
    pub access_level: usize,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum AccessLevel {
    Admin,
    Moderator,
    User,
}
