use super::model::{Role, UserCredentials};
use super::types::Login;
use serde::{Deserialize, Serialize};
use typesafe_repository::IdentityOf;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum UserCredentialsEvent {
    Created(IdentityOf<UserCredentials>),
    Updated(IdentityOf<UserCredentials>),
    Removed(IdentityOf<UserCredentials>),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Get(pub IdentityOf<UserCredentials>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetByLogin(pub Login);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Add(pub UserCredentials);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetResponse(pub Option<UserCredentials>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetByLoginResponse(pub Option<UserCredentials>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AddResponse;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetRoleByLogin(pub Login);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetRoleByLoginResponse(pub Option<Role>);
