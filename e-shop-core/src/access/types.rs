use crate::{NotEmpty, TypeCreationError};
use argon2::{ThreadMode, Variant, Version};
use derive_more::Deref;
use serde::{Deserialize, Serialize};
use typesafe_repository::Selector;

#[derive(Clone, Debug, Deref, Serialize, Deserialize, PartialEq)]
pub struct Password {
    #[deref]
    password: NotEmpty,
    #[serde(with = "serde_arrays")]
    salt: Salt,
}

pub const PASSWORD_LENGTH: u32 = 64;
pub const MIN_PASSWORD_LENGTH: u32 = 5;
pub const DEFAULT_ARGON_CONFIG: argon2::Config = argon2::Config {
    variant: Variant::Argon2i,
    version: Version::Version13,
    mem_cost: 65536,
    time_cost: 10,
    lanes: 4,
    thread_mode: ThreadMode::Parallel,
    secret: &[],
    ad: &[],
    hash_length: PASSWORD_LENGTH,
};

impl Password {
    pub fn new(password: NotEmpty, salt: Salt) -> Result<Password, TypeCreationError> {
        if password.len() < MIN_PASSWORD_LENGTH as usize {
            return Err(TypeCreationError::TooShort);
        }
        Ok(Self { password, salt })
    }
    pub fn check(&self, input: &NotEmpty) -> Result<bool, TypeCreationError> {
        Ok(argon2::verify_encoded(&self.password, input.as_bytes())?)
    }
    pub fn generate(input: NotEmpty, salt: Salt) -> Result<Password, TypeCreationError> {
        let password: NotEmpty =
            argon2::hash_encoded(input.as_bytes(), &salt, &DEFAULT_ARGON_CONFIG)?
                .try_into()
                .unwrap();
        Ok(Self { password, salt })
    }
    pub fn salt(&self) -> &Salt {
        &self.salt
    }
    pub fn password(&self) -> &NotEmpty {
        &self.password
    }
}

impl From<argon2::Error> for TypeCreationError {
    fn from(e: argon2::Error) -> Self {
        use argon2::Error::*;
        match e {
            PwdTooShort => TypeCreationError::TooShort,
            PwdTooLong => TypeCreationError::TooLong,
            _ => panic!(),
        }
    }
}

pub type Salt = [u8; 512];

#[derive(Deref, Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct Login {
    login: NotEmpty,
}

impl Selector for Login {}

impl TryFrom<NotEmpty> for Login {
    type Error = TypeCreationError;
    fn try_from(login: NotEmpty) -> Result<Self, Self::Error> {
        match login.len() {
            1..=2 => return Err(TypeCreationError::TooShort),
            a if a > 20 => return Err(TypeCreationError::TooLong),
            _ => (),
        }
        Ok(Self { login })
    }
}

impl TryFrom<String> for Login {
    type Error = TypeCreationError;
    fn try_from(t: String) -> Result<Self, Self::Error> {
        let not_empty: NotEmpty = t.try_into()?;
        not_empty.try_into()
    }
}

impl From<Login> for String {
    fn from(l: Login) -> Self {
        l.login.into()
    }
}
