use super::types::Salt;
use rand::rngs::StdRng;
use rand::{RngCore, SeedableRng};

pub fn generate_salt() -> Salt {
    let mut salt = [0; 512];
    StdRng::from_entropy().fill_bytes(&mut salt);
    salt
}
