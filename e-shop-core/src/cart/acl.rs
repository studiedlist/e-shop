use crate::{product, Price, Text};
use derive_more::{Constructor, Display, Error};
use serde::{Deserialize, Serialize};
use typesafe_repository::macros::Id;
use typesafe_repository::prelude::*;
use uuid::Uuid;

#[derive(Debug, Display, Error)]
pub enum ConversionError {
    #[error(ignore)]
    ProductWithoutPrice(Uuid),
}

#[derive(Id, Constructor, Serialize, Deserialize, Clone, Debug)]
#[Id(get_id, ref_id)]
pub struct Product {
    pub id: IdentityOf<product::model::Product>,
    pub name: Text<64>,
    pub price: Price,
}

impl TryFrom<product::model::Product> for Product {
    type Error = ConversionError;
    fn try_from(p: crate::product::model::Product) -> Result<Self, Self::Error> {
        let price = p
            .price
            .clone()
            .ok_or_else(|| ConversionError::ProductWithoutPrice(p.id()))?;
        Ok(Product::new(p.id(), p.name, price))
    }
}
