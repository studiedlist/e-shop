use super::model::{Cart, Purchase};
use crate::access::model::UserCredentials;
use serde::{Deserialize, Serialize};
use typesafe_repository::{IdentityOf, ValidIdentity};

pub mod cart {
    use super::*;

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct Get(pub IdentityOf<Cart>);

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct Add(pub Cart);

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct Remove(pub IdentityOf<Cart>);

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct GetResponse(pub Option<Cart>);

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddResponse;
}

pub mod purchase {
    use super::*;

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct Get(pub IdentityOf<Purchase>);

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct Add(pub Purchase);

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct ListByUserId(pub ValidIdentity<UserCredentials>);

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct GetResponse(pub Option<Purchase>);

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct AddResponse;

    #[derive(Deserialize, Serialize, Debug, Clone)]
    pub struct ListByUserIdResponse(pub Vec<Purchase>);
}
