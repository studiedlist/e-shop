use super::acl::Product;
use crate::access::model::UserCredentials;
use crate::{Address, Email, PhoneNumber, Text};
use chrono::NaiveDateTime;
use derive_more::{Display, Error};
use derive_new::new;
use rust_decimal::Decimal;
use serde::{Deserialize, Serialize};
use typesafe_repository::macros::Id;
use typesafe_repository::{
    GetIdentity, Identity, IdentityBy, IdentityOf, RefIdentity, ValidIdentity,
};
use uuid::Uuid;

#[derive(Id, new, Serialize, Deserialize, Clone, Debug, PartialEq)]
#[Id(get_id, ref_id)]
pub struct Cart {
    pub id: Uuid,
    pub items: Vec<(IdentityOf<Product>, usize)>,
}

#[derive(new)]
pub struct EnrichedCart {
    pub id: ValidIdentity<Cart>,
    pub items: Vec<(Product, usize)>,
}

#[derive(Error, Display, Debug, Clone)]
pub enum EnrichmentError {
    EntityNotProvided,
}

#[derive(new, Serialize, Deserialize, Clone, Debug, PartialEq)]
pub struct ContactInfo {
    pub phone_number: Option<PhoneNumber>,
    pub first_name: Text<256>,
    pub last_name: Text<256>,
    pub email: Email,
    pub address: Address,
}

impl Cart {
    pub fn enrich(self, products: Vec<Product>) -> Result<EnrichedCart, EnrichmentError> {
        let id = ValidIdentity::of(&self);
        self.items
            .into_iter()
            .map(|(id, count)| {
                products
                    .iter()
                    .find(|x| x.id_ref() == &id)
                    .cloned()
                    .map(|p| (p, count))
            })
            .collect::<Option<Vec<_>>>()
            .ok_or(EnrichmentError::EntityNotProvided)
            .map(|items| EnrichedCart { id, items })
    }
}

impl EnrichedCart {
    pub fn total(&self) -> Decimal {
        self.items
            .iter()
            .map(|(p, count)| *p.price * Decimal::from(*count))
            .sum()
    }
}

#[derive(Id, new, Serialize, Deserialize, Clone, Debug, PartialEq)]
#[Id(get_id, ref_id)]
pub struct Purchase {
    pub id: Uuid,
    pub user_id: Option<ValidIdentity<UserCredentials>>,
    pub cart: ValidIdentity<Cart>,
    pub contact_info: ContactInfo,
    pub date: NaiveDateTime,
}

impl IdentityBy<Option<ValidIdentity<UserCredentials>>> for Purchase {
    fn id_by(&self) -> Option<ValidIdentity<UserCredentials>> {
        self.user_id.clone()
    }
}

#[cfg(test)]
mod test {}
