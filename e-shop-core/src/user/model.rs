use crate::access::model::UserCredentials;
use crate::cart::model::Purchase;
use derive_new::new;
use serde::{Deserialize, Serialize};
use typesafe_repository::macros::Id;
use typesafe_repository::prelude::*;
use typesafe_repository::ValidIdentity;
use uuid::Uuid;

#[derive(Id, new, Serialize, Deserialize, Clone, Debug, PartialEq)]
#[Id(ref_id, get_id)]
pub struct User {
    pub id: Uuid,
    pub credentials: ValidIdentity<UserCredentials>,
    pub purchase_history: Vec<ValidIdentity<Purchase>>,
}

impl IdentityBy<ValidIdentity<UserCredentials>> for User {
    fn id_by(&self) -> ValidIdentity<UserCredentials> {
        self.credentials.clone()
    }
}
