use super::model::User;
use serde::{Deserialize, Serialize};
use typesafe_repository::IdentityOf;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Get(pub IdentityOf<User>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Add(pub User);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetResponse(pub Option<User>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AddResponse;
