use super::model::Product;
use crate::catalog::model::Category;
use serde::{Deserialize, Serialize};
use typesafe_repository::{IdentityOf, ValidIdentity};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum ProductEvent {
    Created(IdentityOf<Product>),
    Updated(IdentityOf<Product>),
    Removed(IdentityOf<Product>),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Get(pub IdentityOf<Product>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct List();

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ListTop(pub usize);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ListByCategoryId(pub ValidIdentity<Category>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Remove(pub IdentityOf<Product>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Update(pub Product);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Add(pub Product);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetResponse(pub Option<Product>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ListResponse(pub Vec<Product>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ListTopResponse(pub Vec<Product>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ListByCategoryIdResponse(pub Vec<Product>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct RemoveResponse();

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UpdateResponse();

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AddResponse();
