use crate::catalog::model::Category;
use crate::{Price, Text};
use chrono::NaiveDateTime;
use derive_more::Deref;
use derive_new::new;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use typesafe_repository::macros::Id;
use typesafe_repository::{
    GetIdentity, Identity, IdentityBy, RefIdentity, SelectBy, Selector, ValidIdentity,
};
use uuid::Uuid;

#[derive(Id, Serialize, Deserialize, Clone, Debug, new)]
#[Id(ref_id, get_id)]
pub struct Product {
    id: Uuid,
    pub name: Text<64>,
    pub description: Text<4096>,
    pub added: NaiveDateTime,
    pub price: Option<Price>,
    pub in_stock: InStock,
    pub category: ValidIdentity<Category>,
    pub images: Vec<PathBuf>,
}

impl Product {
    pub fn fmt_price(&self) -> String {
        self.price
            .clone()
            .map(|x| x.round_dp(2).to_string())
            .unwrap_or("-".to_string())
    }
    pub fn fmt_id(&self) -> String {
        let mut id = self
            .id
            .as_hyphenated()
            .to_string()
            .split_at(15)
            .0
            .to_string();
        id.push_str("..");
        id
    }
    pub fn fmt_in_stock(&self) -> String {
        self.in_stock
            .map(|x| x.to_string())
            .unwrap_or("None".to_string())
    }
    pub fn fmt_added(&self) -> String {
        self.added.date().to_string()
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Deref, Serialize, Deserialize, PartialOrd, Ord)]
pub struct InStock(Option<u64>);

impl IdentityBy<ValidIdentity<Category>> for Product {
    fn id_by(&self) -> ValidIdentity<Category> {
        self.category.clone()
    }
}

pub struct Latest;

impl Selector for Latest {}

impl SelectBy<Latest> for Product {}

impl From<u64> for InStock {
    fn from(c: u64) -> Self {
        Self(Some(c))
    }
}

impl From<Option<u64>> for InStock {
    fn from(c: Option<u64>) -> Self {
        Self(c)
    }
}
