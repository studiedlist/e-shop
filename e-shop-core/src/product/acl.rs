use crate::Text;
use typesafe_repository::macros::Id;
use typesafe_repository::prelude::*;
use uuid::Uuid;

#[derive(Id, Clone, Debug)]
#[Id(get_id, ref_id)]
pub struct Category {
    pub id: Uuid,
    pub name: Text<255>,
}

impl From<crate::catalog::model::Category> for Category {
    fn from(c: crate::catalog::model::Category) -> Self {
        Self {
            id: c.id,
            name: c.name,
        }
    }
}
