pub mod access;
pub mod cart;
pub mod catalog;
pub mod product;
pub mod user;

use derive_more::{Deref, Display, Error};
use derive_new::new;
use lazy_regex::regex;
use rust_decimal::Decimal;
use serde::{Deserialize, Serialize};

#[derive(Error, Display, Debug)]
pub enum TypeCreationError {
    TooLong,
    TooShort,
    Empty,
    #[error(ignore)]
    Invalid(String),
}

impl TypeCreationError {
    pub fn invalid<T>(s: T) -> Self
    where
        T: ToString,
    {
        Self::Invalid(s.to_string())
    }
}

#[derive(Deref, PartialEq, Clone, Debug, Serialize, Deserialize)]
pub struct Email {
    pub email: String,
}

impl TryFrom<String> for Email {
    type Error = TypeCreationError;
    fn try_from(email: String) -> Result<Self, Self::Error> {
        Ok(Self { email })
    }
}

#[derive(Deref, PartialEq, Eq, Clone, Debug, Serialize, Deserialize, Display, PartialOrd, Ord)]
pub struct NotEmpty(pub String);

impl From<NotEmpty> for String {
    fn from(n: NotEmpty) -> Self {
        n.0
    }
}

impl TryFrom<&str> for NotEmpty {
    type Error = TypeCreationError;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        s.to_string().try_into()
    }
}

#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize, Display, Deref, PartialOrd, Ord)]
pub struct Text<const MAX: usize>(String);

impl<const MAX: usize> Text<MAX> {
    pub fn into_inner(self) -> String {
        self.0
    }
}

impl<const MAX: usize> TryFrom<String> for Text<MAX> {
    type Error = TypeCreationError;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        if s.len() > MAX {
            Err(TypeCreationError::TooLong)
        } else {
            Ok(Self(s))
        }
    }
}

impl<const MAX: usize> TryFrom<&str> for Text<MAX> {
    type Error = TypeCreationError;
    fn try_from(s: &str) -> Result<Self, Self::Error> {
        if s.len() > MAX {
            Err(TypeCreationError::TooLong)
        } else {
            Ok(Self(s.to_string()))
        }
    }
}

impl TryFrom<String> for NotEmpty {
    type Error = TypeCreationError;
    fn try_from(v: String) -> Result<Self, Self::Error> {
        if v.is_empty() {
            Err(TypeCreationError::Empty)
        } else {
            Ok(NotEmpty(v))
        }
    }
}

#[derive(Deref, Clone, PartialEq, Eq, Debug, Serialize, Deserialize, Display, PartialOrd, Ord)]
pub struct PhoneNumber {
    phone_number: NotEmpty,
}

impl TryFrom<NotEmpty> for PhoneNumber {
    type Error = TypeCreationError;
    fn try_from(phone_number: NotEmpty) -> Result<Self, Self::Error> {
        if !check_phone_num(&phone_number) {
            Err(TypeCreationError::Invalid(
                "Incorrect phone number".to_string(),
            ))
        } else {
            Ok(Self { phone_number })
        }
    }
}

impl TryFrom<String> for PhoneNumber {
    type Error = TypeCreationError;
    fn try_from(phone_number: String) -> Result<Self, Self::Error> {
        let phone_number: NotEmpty = phone_number.try_into()?;
        phone_number.try_into()
    }
}

impl From<PhoneNumber> for String {
    fn from(p: PhoneNumber) -> Self {
        p.phone_number.into()
    }
}

pub fn check_phone_num(phone_num: &str) -> bool {
    regex!(r"^\+\d{1,3}[\(|-]?\d{2}[\)|-]?\d{3}(-?\d{2}){2}$").is_match(phone_num)
}

#[derive(new, Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct Address {
    pub city: String,
    pub street: String,
}

#[derive(Display, PartialEq, Eq, Deref, Clone, Debug, Serialize, Deserialize, PartialOrd, Ord)]
pub struct Price {
    #[deref]
    pub price: Decimal,
}

impl<T> From<T> for Price
where
    Decimal: From<T>,
{
    fn from(t: T) -> Self {
        Self {
            price: Decimal::from(t),
        }
    }
}
