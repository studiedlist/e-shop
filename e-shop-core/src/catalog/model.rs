use crate::{NotEmpty, Text, TypeCreationError};
use derive_more::Constructor;
use rust_decimal::Decimal;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use typesafe_repository::macros::Id;
use typesafe_repository::prelude::*;
use uuid::Uuid;

#[derive(Id, Constructor, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[Id(ref_id, get_id)]
pub struct Category {
    pub id: Uuid,
    pub name: Text<255>,
    pub description: Text<4096>,
    pub properties: Vec<Property>,
    pub parent: Option<IdentityOf<Category>>,
    pub children: Vec<IdentityOf<Category>>,
    pub image: Option<PathBuf>,
}

#[derive(Id, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[Id(ref_id, get_id)]
pub struct Property {
    pub id: Uuid,
    pub name: NotEmpty,
    pub t: PropertyType,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum PropertyType {
    Binary,
    Decimal,
    Int,
    UInt,
    Range,
}

pub enum PropertyValue {
    Binary(bool),
    Decimal(Decimal),
    Int(i64),
    UInt(u64),
    Range((i64, i64)),
}

#[derive(Id)]
pub struct ValidProperty {
    value: PropertyValue,
    id: Uuid,
    name: NotEmpty,
}

impl ValidProperty {
    pub fn new(
        Property { t, name, id }: Property,
        value: PropertyValue,
    ) -> Result<Self, TypeCreationError> {
        match (t, &value) {
            (PropertyType::Binary, PropertyValue::Binary(_)) => (),
            (PropertyType::Decimal, PropertyValue::Decimal(_)) => (),
            (PropertyType::Int, PropertyValue::Int(_)) => (),
            (PropertyType::UInt, PropertyValue::UInt(_)) => (),
            (PropertyType::Range, PropertyValue::Range(_)) => (),
            _ => {
                return Err(TypeCreationError::Invalid(
                    "Property type mismatch".to_string(),
                ))
            }
        }
        Ok(Self { value, name, id })
    }
    pub fn name(&self) -> &NotEmpty {
        &self.name
    }
    pub fn value(&self) -> &PropertyValue {
        &self.value
    }
}

pub struct NewsEntry {
    pub name: Text<64>,
    pub description: Text<4096>,
}
