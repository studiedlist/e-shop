use super::model::{Category, ValidProperty};
use crate::{Price, Text};
use derive_more::Constructor;
use std::path::PathBuf;
use typesafe_repository::macros::Id;
use typesafe_repository::prelude::*;
use typesafe_repository::ValidIdentity;
use uuid::Uuid;

#[derive(Constructor, Id)]
#[Id(get_id, ref_id)]
pub struct Product {
    pub id: Uuid,
    pub name: Text<64>,
    pub description: Text<4096>,
    pub category: ValidIdentity<Category>,
    pub price: Option<Price>,
    pub properties: Vec<ValidProperty>,
    pub images: Vec<PathBuf>,
}

use crate::product::model::Product as OriginalProduct;

impl From<(OriginalProduct, Vec<ValidProperty>)> for Product {
    fn from((p, properties): (OriginalProduct, Vec<ValidProperty>)) -> Self {
        Self::new(
            p.id(),
            p.name,
            p.description,
            p.category,
            p.price,
            properties,
            p.images,
        )
    }
}
