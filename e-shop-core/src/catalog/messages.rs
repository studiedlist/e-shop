use super::model::Category;
use crate::product::model::Product;
use serde::{Deserialize, Serialize};
use typesafe_repository::IdentityOf;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum CategoryEvent {
    Created(IdentityOf<Category>),
    Updated(IdentityOf<Category>),
    Removed(IdentityOf<Category>),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Get(pub IdentityOf<Category>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetResponse(pub Option<Category>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct List();

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ListResponse(pub Vec<Category>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetByProductId(pub IdentityOf<Product>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetByProductIdResponse(pub Option<Category>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Add(pub Category);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AddResponse();

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Update(pub Category);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct UpdateResponse();

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Remove(pub IdentityOf<Category>);

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct RemoveResponse();
