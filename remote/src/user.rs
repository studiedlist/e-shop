use super::prelude::*;
use actix::Addr;
use derive_new::new;
use std::any::{Any, TypeId};
use std::time::Duration;
use tokio::sync::oneshot::Sender;
use tokio::time::timeout;

use e_shop_core::user::{messages::*, model::User};

use actix_telepathy::ResponseSubscribe;

pub mod messages {
    use super::*;

    type Result<T> = std::result::Result<T, ServiceError>;

    messages_v2! {
        Get | GetResponse -> Result<Option<User>> {
            request: InternalGetMessage | ExternalGetMessage,
            response: InternalGetResponse | ExternalGetResponse,
        }
        Add | AddResponse -> Result<()> {
            request: InternalAddMessage | ExternalAddMessage,
            response: InternalAddResponse | ExternalAddResponse,
        }
    }
}

use messages::*;

pub trait RequestHandler:
    Actor<Context = Context<Self>>
    + Handler<ExternalGetMessage, Result = ResponseFuture<()>>
    + Handler<ExternalAddMessage, Result = ResponseFuture<()>>
{
}

impl<T> RequestHandler for T where
    T: Actor<Context = Context<Self>>
        + Handler<ExternalGetMessage, Result = ResponseFuture<()>>
        + Handler<ExternalAddMessage, Result = ResponseFuture<()>>
{
}

pub trait ResponseHandler:
    Actor<Context = Context<Self>>
    + Handler<ExternalGetResponse, Result = ResponseFuture<()>>
    + Handler<ExternalAddResponse, Result = ResponseFuture<()>>
{
}

impl<T> ResponseHandler for T where
    T: Actor<Context = Context<Self>>
        + Handler<ExternalGetResponse, Result = ResponseFuture<()>>
        + Handler<ExternalAddResponse, Result = ResponseFuture<()>>
{
}

pub trait ExternalRequest: ExternalMessage + Send + Sync + 'static {
    type Internal: InternalMessage;
}

impl ExternalRequest for ExternalGetMessage {
    type Internal = InternalGetResponse;
}

impl ExternalRequest for ExternalAddMessage {
    type Internal = InternalAddResponse;
}

pub trait ExternalResponse: Message<Result = ()> + Send + Sync + 'static {}

impl ExternalResponse for ExternalGetResponse {}
impl ExternalResponse for ExternalAddResponse {}

#[derive(RemoteActor)]
#[remote_messages(ExternalGetMessage, ExternalAddMessage, InitMessage)]
#[derive(new)]
pub struct RemoteUserRequestService<H: RequestHandler> {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    handler: Arc<Addr<H>>,
    addr: SocketAddr,
}

impl<H: RequestHandler> RemoteService for RemoteUserRequestService<H> {
    fn id() -> String {
        "RemoteUserRequestService".to_string()
    }
    fn remote_actor_id() -> &'static str {
        "RemoteUserResponseService"
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Backend
    }
}

impl<H: RequestHandler> Actor for RemoteUserRequestService<H> {
    type Context = Context<Self>;
    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
        self.subscribe_system_async::<InternalGetResponse>(ctx);
        self.subscribe_system_async::<InternalAddResponse>(ctx);
    }
}

impl<H, M> Handler<M> for RemoteUserRequestService<H>
where
    H: RequestHandler + Handler<M>,
    M: ExternalRequest + Message<Result = ()>,
    M::Internal: Message,
    Self: Handler<M::Internal>,
{
    type Result = ();

    fn handle(&mut self, msg: M, _ctx: &mut Self::Context) -> Self::Result {
        log::debug!("Got external message");
        self.handler.do_send(msg);
    }
}

impl<H: RequestHandler> Handler<InternalGetResponse> for RemoteUserRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalGetResponse, _ctx: &mut Self::Context) -> Self::Result {
        log::debug!("RemoteProductService");
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalAddResponse> for RemoteUserRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalAddResponse, _: &mut Self::Context) -> Self::Result {
        log::debug!("RemoteProductService");
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> ClusterListener for RemoteUserRequestService<H> {}

impl<H: RequestHandler> Handler<ClusterLog> for RemoteUserRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) -> Self::Result {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string())
    }
}

impl<H: RequestHandler> Handler<InitMessage> for RemoteUserRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}

#[derive(RemoteActor)]
#[remote_messages(ExternalGetResponse, ExternalAddResponse, InitMessage)]
#[derive(new)]
pub struct RemoteUserResponseService<H: ResponseHandler> {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    handler: Arc<Addr<H>>,
    addr: SocketAddr,
    #[new(default)]
    response_notify: HashMap<TypeId, Sender<Box<dyn Any + Send + 'static>>>,
}

impl<H: ResponseHandler> Actor for RemoteUserResponseService<H> {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
    }
}

impl<H: ResponseHandler> RemoteService for RemoteUserResponseService<H> {
    fn id() -> String {
        "RemoteUserResponseService".to_string()
    }
    fn remote_actor_id() -> &'static str {
        "RemoteUserRequestService"
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Frontend
    }
}

impl<H, M> Handler<M> for RemoteUserResponseService<H>
where
    H: ResponseHandler + Handler<M>,
    M: ExternalResponse + Clone,
{
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: M, _: &mut Self::Context) -> Self::Result {
        if let Some(sender) = self.response_notify.remove(&TypeId::of::<M>()) {
            if let Err(e) = sender.send(Box::new(msg.clone())) {
                log::error!("Failed to send {e:?} response via oneshot channel");
            }
        }
        let handler = self.handler.clone();
        Box::pin(async move { handler.send(msg).await.unwrap() })
    }
}

impl<H: ResponseHandler> ClusterListener for RemoteUserResponseService<H> {}

impl<H: ResponseHandler> Handler<ClusterLog> for RemoteUserResponseService<H> {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string());
    }
}

impl<H: ResponseHandler> Handler<InitMessage> for RemoteUserResponseService<H> {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}

impl<H> Handler<ResponseSubscribe> for RemoteUserResponseService<H>
where
    H: ResponseHandler,
{
    type Result = ();

    fn handle(
        &mut self,
        ResponseSubscribe(id, tx): ResponseSubscribe,
        _: &mut Self::Context,
    ) -> Self::Result {
        self.response_notify.insert(id, tx);
    }
}

impl<H> Handler<InternalGetMessage> for RemoteUserResponseService<H>
where
    H: ResponseHandler,
{
    type Result = ResponseFuture<Result<Option<User>, actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalGetMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get message {:?}", msg.0);
            for (m, _) in members {
                log::debug!("sent get message");
                let r = m.send::<_, ExternalGetResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalGetResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalGetResponse"),
                }
            }
            Ok(None)
        })
    }
}

impl<H> Handler<InternalAddMessage> for RemoteUserResponseService<H>
where
    H: ResponseHandler,
{
    type Result = ResponseFuture<Result<(), actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalAddMessage, ctx: &mut Self::Context) -> Self::Result {
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get message {:?}", msg.0);
            for (m, _) in members {
                log::debug!("sent get message");
                let r = m.send::<_, ExternalAddResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalAddResponse>() {
                    Ok(_) => return Ok(()),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalGetResponse"),
                }
            }
            Ok(())
        })
    }
}
