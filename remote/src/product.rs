use super::prelude::*;
use actix::Addr;
use derive_new::new;
use std::any::{Any, TypeId};
use std::time::Duration;
use tokio::sync::oneshot::Sender;
use tokio::time::timeout;

use e_shop_core::product::{messages::*, model::Product};

use actix_telepathy::ResponseSubscribe;

pub type InternalProductEvent = InternalEvent<ProductEvent>;
pub type ExternalProductEvent = ExternalEvent<ProductEvent>;

pub mod messages {
    use super::*;

    type Result<T> = std::result::Result<T, ServiceError>;

    messages_v2! {
        Get | GetResponse -> Result<Option<Product>> {
            request: InternalGetMessage | ExternalGetMessage,
            response: InternalGetResponse | ExternalGetResponse,
        }
        List | ListResponse -> Result<Vec<Product>> {
            request: InternalListMessage | ExternalListMessage,
            response: InternalListResponse | ExternalListResponse,
        }
        ListTop | ListTopResponse -> Result<Vec<Product>> {
            request: InternalListTopMessage | ExternalListTopMessage,
            response: InternalListTopResponse | ExternalListTopResponse,
        }
        ListByCategoryId | ListByCategoryIdResponse -> Result<Vec<Product>> {
            request: InternalListByCategoryIdMessage | ExternalListByCategoryIdMessage,
            response: InternalListByCategoryIdResponse | ExternalListByCategoryIdResponse,
        }
        Add | AddResponse -> Result<()> {
            request: InternalAddMessage | ExternalAddMessage,
            response: InternalAddResponse | ExternalAddResponse,
        }
        Update | UpdateResponse -> Result<()> {
            request: InternalUpdateMessage | ExternalUpdateMessage,
            response: InternalUpdateResponse | ExternalUpdateResponse,
        }
        Remove | RemoveResponse -> Result<()> {
            request: InternalRemoveMessage | ExternalRemoveMessage,
            response: InternalRemoveResponse | ExternalRemoveResponse,
        }
    }
}

use messages::*;

pub trait RequestHandler:
    Actor<Context = Context<Self>>
    + Handler<ExternalGetMessage, Result = ResponseFuture<()>>
    + Handler<ExternalListMessage, Result = ResponseFuture<()>>
    + Handler<ExternalListTopMessage, Result = ResponseFuture<()>>
    + Handler<ExternalListByCategoryIdMessage, Result = ResponseFuture<()>>
{
}

impl<T> RequestHandler for T where
    T: Actor<Context = Context<Self>>
        + Handler<ExternalGetMessage, Result = ResponseFuture<()>>
        + Handler<ExternalListMessage, Result = ResponseFuture<()>>
        + Handler<ExternalListTopMessage, Result = ResponseFuture<()>>
        + Handler<ExternalListByCategoryIdMessage, Result = ResponseFuture<()>>
{
}

pub trait ResponseHandler:
    Actor<Context = Context<Self>>
    + Handler<ExternalGetResponse, Result = ResponseFuture<()>>
    + Handler<ExternalListResponse, Result = ResponseFuture<()>>
    + Handler<ExternalListTopResponse, Result = ResponseFuture<()>>
    + Handler<ExternalListByCategoryIdResponse, Result = ResponseFuture<()>>
{
}

impl<T> ResponseHandler for T where
    T: Actor<Context = Context<Self>>
        + Handler<ExternalGetResponse, Result = ResponseFuture<()>>
        + Handler<ExternalListResponse, Result = ResponseFuture<()>>
        + Handler<ExternalListTopResponse, Result = ResponseFuture<()>>
        + Handler<ExternalListByCategoryIdResponse, Result = ResponseFuture<()>>
{
}

pub trait ExternalRequest: ExternalMessage + Send + Sync + 'static {
    type Internal: InternalMessage;
}

impl ExternalRequest for ExternalGetMessage {
    type Internal = InternalGetResponse;
}
impl ExternalRequest for ExternalListMessage {
    type Internal = InternalListResponse;
}
impl ExternalRequest for ExternalListTopMessage {
    type Internal = InternalListTopResponse;
}
impl ExternalRequest for ExternalListByCategoryIdMessage {
    type Internal = InternalListByCategoryIdResponse;
}

pub trait ExternalResponse: Message<Result = ()> + Send + Sync + 'static {}

impl ExternalResponse for ExternalGetResponse {}
impl ExternalResponse for ExternalListResponse {}
impl ExternalResponse for ExternalListTopResponse {}
impl ExternalResponse for ExternalListByCategoryIdResponse {}

#[derive(RemoteActor)]
#[remote_messages(
    ExternalGetMessage,
    ExternalListMessage,
    ExternalListTopMessage,
    ExternalListByCategoryIdMessage,
    InitMessage
)]
#[derive(new)]
pub struct RemoteProductRequestService<H: RequestHandler> {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    handler: Arc<Addr<H>>,
    addr: SocketAddr,
}

impl<H: RequestHandler> RemoteService for RemoteProductRequestService<H> {
    fn id() -> String {
        "RemoteProductRequestService".to_string()
    }
    fn remote_actor_id() -> &'static str {
        "RemoteProductResponseService"
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Backend
    }
}

impl<H: RequestHandler> Actor for RemoteProductRequestService<H> {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
        self.subscribe_system_async::<InternalListResponse>(ctx);
        self.subscribe_system_async::<InternalGetResponse>(ctx);
        self.subscribe_system_async::<InternalListByCategoryIdResponse>(ctx);
        self.subscribe_system_async::<InternalListTopResponse>(ctx);
    }
}

impl<H, M> Handler<M> for RemoteProductRequestService<H>
where
    H: RequestHandler + Handler<M>,
    M: ExternalRequest + Message<Result = ()>,
    M::Internal: Message,
    Self: Handler<M::Internal>,
{
    type Result = ();

    fn handle(&mut self, msg: M, _ctx: &mut Self::Context) -> Self::Result {
        log::debug!("Got external message");
        self.handler.do_send(msg);
    }
}

impl<H: RequestHandler> Handler<InternalGetResponse> for RemoteProductRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalGetResponse, _ctx: &mut Self::Context) -> Self::Result {
        log::debug!("RemoteProductService");
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalListResponse> for RemoteProductRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalListResponse, _ctx: &mut Self::Context) -> Self::Result {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalListTopResponse> for RemoteProductRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalListTopResponse, _ctx: &mut Self::Context) -> Self::Result {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalListByCategoryIdResponse>
    for RemoteProductRequestService<H>
{
    type Result = ();

    fn handle(
        &mut self,
        msg: InternalListByCategoryIdResponse,
        _: &mut Self::Context,
    ) -> Self::Result {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H> Handler<InternalGetMessage> for RemoteProductResponseService<H>
where
    H: ResponseHandler,
{
    type Result = ResponseFuture<Result<Option<Product>, actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalGetMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get message {:?}", msg.0);
            for (m, _) in members {
                log::debug!("sent get message");
                let r = m.send::<_, ExternalGetResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalGetResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalGetResponse"),
                }
            }
            Ok(None)
        })
    }
}

impl<H> Handler<InternalListMessage> for RemoteProductResponseService<H>
where
    H: ResponseHandler,
{
    type Result = ResponseFuture<Result<Vec<Product>, ServiceError>>;

    fn handle(&mut self, msg: InternalListMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::ExternalResponse;
        let msg: ExternalListMessage = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending list message");
            for (m, _) in members {
                log::debug!("sent list message");
                let r = m.send::<_, ExternalListResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r).await.unwrap().unwrap();
                match res.downcast::<ExternalListResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalListResponse"),
                }
            }
            Ok(vec![])
        })
    }
}

impl<H: ResponseHandler> Handler<InternalListTopMessage> for RemoteProductResponseService<H> {
    type Result = ResponseFuture<Result<Vec<Product>, ServiceError>>;

    fn handle(&mut self, msg: InternalListTopMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending list top message");
            for (m, _) in members {
                log::debug!("sent list top message");
                let r = m.send::<_, ExternalListTopResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r).await.unwrap().unwrap();
                match res.downcast::<ExternalListTopResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalListResponse"),
                }
            }
            Ok(vec![])
        })
    }
}

impl<H> Handler<InternalListByCategoryIdMessage> for RemoteProductResponseService<H>
where
    H: ResponseHandler,
{
    type Result = ResponseFuture<Result<Vec<Product>, ServiceError>>;

    fn handle(
        &mut self,
        msg: InternalListByCategoryIdMessage,
        ctx: &mut Self::Context,
    ) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending list by category id message");
            for (m, _) in members {
                log::debug!("sent list by category id message");
                let r = m.send::<_, ExternalListByCategoryIdResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r).await.unwrap().unwrap();
                match res.downcast::<ExternalListByCategoryIdResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalListResponse"),
                }
            }
            Ok(vec![])
        })
    }
}

impl<H> Handler<ResponseSubscribe> for RemoteProductResponseService<H>
where
    H: ResponseHandler,
{
    type Result = ();

    fn handle(
        &mut self,
        ResponseSubscribe(id, tx): ResponseSubscribe,
        _: &mut Self::Context,
    ) -> Self::Result {
        self.response_notify.insert(id, tx);
    }
}

impl<H: ResponseHandler> ClusterListener for RemoteProductResponseService<H> {}

impl<H: ResponseHandler> Handler<ClusterLog> for RemoteProductResponseService<H> {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) -> Self::Result {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string())
    }
}

impl<H: RequestHandler> Handler<InitMessage> for RemoteProductRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}

impl<H: RequestHandler> ClusterListener for RemoteProductRequestService<H> {}

impl<H: RequestHandler> Handler<ClusterLog> for RemoteProductRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string())
    }
}

#[derive(RemoteActor)]
#[remote_messages(
    ExternalGetResponse,
    ExternalListResponse,
    ExternalListTopResponse,
    ExternalListByCategoryIdResponse,
    InitMessage
)]
#[derive(new)]
pub struct RemoteProductResponseService<H: ResponseHandler> {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    handler: Arc<Addr<H>>,
    addr: SocketAddr,
    #[new(default)]
    response_notify: HashMap<TypeId, Sender<Box<dyn Any + Send + 'static>>>,
}

impl<H: ResponseHandler> Actor for RemoteProductResponseService<H> {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
    }
}

impl<H: ResponseHandler> RemoteService for RemoteProductResponseService<H> {
    fn id() -> String {
        "RemoteProductResponseService".to_string()
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Frontend
    }
    fn remote_actor_id() -> &'static str {
        "RemoteProductRequestService"
    }
}

impl<H, M> Handler<M> for RemoteProductResponseService<H>
where
    H: ResponseHandler + Handler<M>,
    M: ExternalResponse + Clone,
{
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: M, _: &mut Self::Context) -> Self::Result {
        if let Some(sender) = self.response_notify.remove(&TypeId::of::<M>()) {
            if let Err(e) = sender.send(Box::new(msg.clone())) {
                log::error!("Failed to send {e:?} response via oneshot channel");
            }
        }
        let handler = self.handler.clone();
        Box::pin(async move { handler.send(msg).await.unwrap() })
    }
}

impl<H: ResponseHandler> Handler<InitMessage> for RemoteProductResponseService<H> {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}
