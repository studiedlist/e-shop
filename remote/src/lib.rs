pub mod access;
pub mod cart;
pub mod catalog;
pub mod product;
pub mod purchase;
pub mod user;

use actix::{Addr, Handler, Message};

use actix_telepathy::prelude::*;
use derive_more::{Display, Error};
use derive_new::new;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use typesafe_repository::prelude::*;

use async_trait::async_trait;
use std::fmt::Debug;
use std::net::SocketAddr;
use tokio::sync::oneshot::Sender;
use typesafe_repository::IdentityOf;
use uuid::Uuid;

pub mod prelude {
    pub use super::{
        messages_v2, BroadcastRecipient, ExternalEvent, ExternalMessage, ExternalOperationMessage,
        ExternalOperationResponse, ExternalResponse, InitMessage, InternalEvent, InternalMessage,
        InternalOperationMessage, InternalOperationResponse, NodeClass, RegisterSender,
        RemoteMember, RemoteService,
    };
    pub use actix::{Actor, AsyncContext, Context, Handler, Message, ResponseFuture};
    pub use actix_broker::BrokerSubscribe;
    pub use actix_service::ServiceError;
    pub use actix_telepathy::prelude::*;
    pub use std::collections::HashMap;
    pub use std::net::SocketAddr;
    pub use std::sync::Arc;
    pub use tokio::sync::Notify;
}

#[macro_export]
macro_rules! messages_v2 {
    (
        $($s:ident | $r:ident -> $result:ty {
            request: $internal_req:ident | $external_req:ident,
            response: $internal_res:ident | $external_res:ident,
        })*
    ) => {
        use uuid::Uuid;
        use $crate::{IntoExternal, ExternalResponse};
        use serde::{Serialize, Deserialize};
        $(
            #[derive(Clone, Debug)]
            pub struct $internal_req($s);
            impl Message for $internal_req {
               type Result = $result;
            }
            impl IntoExternal for $internal_req {
                type External = $external_req;
            }
            impl From<$internal_req> for $external_req {
                fn from(req: $internal_req) -> Self {
                    $external_req(Uuid::new_v4(), req.0)
                }
            }
            impl From<$s> for $internal_req {
                fn from(s: $s) -> Self {
                    $internal_req(s)
                }
            }
            impl InternalMessage for $internal_req {
                type External = $external_req;
            }
            #[derive(RemoteMessage, Serialize, Deserialize, Clone, Debug)]
            pub struct $external_req(pub Uuid, pub $s);
            impl ExternalMessage for $external_req {}

            #[derive(Clone, Debug)]
            pub struct $internal_res(pub Uuid, pub $r);
            impl Message for $internal_res {
                type Result = ();
            }

            impl From<$internal_res> for $external_res {
                fn from(req: $internal_res) -> Self {
                    $external_res(req.0, req.1)
                }
            }
            impl InternalMessage for $internal_res {
                type External = $external_res;
            }
            impl IntoExternal for $internal_res {
                type External = $external_res;
            }
            impl From<(Uuid, $r)> for $internal_res {
                fn from((id, r): (Uuid, $r)) -> Self {
                    $internal_res(id, r)
                }
            }

            #[derive(RemoteMessage, Serialize, Deserialize, Clone, Debug)]
            pub struct $external_res(pub Uuid, $r);
            impl ExternalMessage for $external_res {}

            impl ExternalResponse<$r> for $external_res {
                fn into_inner(self) -> $r {
                    self.1
                }
            }
        )*
    }
}

#[async_trait]
pub trait RegisterSender<T: Identity> {
    async fn register_sender(&mut self, id: IdentityOf<T>, s: Sender<T>);
}

#[derive(Error, Display, Debug)]
pub enum RemoteServiceError {
    Timeout,
}

pub trait IntoExternal: Clone + Into<Self::External> {
    type External: ExternalMessage;
}

impl<T: ExternalMessage + Clone> IntoExternal for T {
    type External = Self;
}

pub trait InternalMessage: Clone + Into<Self::External> {
    type External: ExternalMessage;
    fn into_external(self) -> Self::External {
        self.into()
    }
}

pub trait ExternalMessage: RemoteMessage + Message<Result = ()> {}

pub trait ExternalResponse<T>: ExternalMessage {
    fn into_inner(self) -> T;
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Hash)]
pub enum NodeClass {
    Frontend,
    Backend,
    LoadBalancer,
}

#[derive(RemoteMessage, Debug, Clone, Serialize, Deserialize, new)]
pub struct InitMessage {
    addr: SocketAddr,
    class: NodeClass,
    id: String,
}

#[derive(Debug, Clone, Message, Serialize, Deserialize)]
#[rtype(result = "()")]
pub struct InternalEvent<T>(pub T)
where
    T: Debug + Clone + Serialize;

impl<T> From<T> for InternalEvent<T>
where
    T: Debug + Clone + Serialize,
{
    fn from(t: T) -> Self {
        Self(t)
    }
}

#[derive(Debug, Clone, RemoteMessage, Serialize, Deserialize)]
pub struct ExternalEvent<T>(pub Uuid, pub T)
where
    T: Debug + Clone + Serialize + Send;

impl<T> IntoExternal for InternalEvent<T>
where
    T: Debug + Clone + Serialize + Send,
{
    type External = ExternalEvent<T>;
}

impl<T> InternalMessage for InternalEvent<T>
where
    T: Debug + Clone + Serialize + Send,
{
    type External = ExternalEvent<T>;
}

impl<T> From<InternalEvent<T>> for ExternalEvent<T>
where
    T: Debug + Clone + Serialize + Send,
{
    fn from(e: InternalEvent<T>) -> Self {
        ExternalEvent::<T>(Uuid::new_v4(), e.0)
    }
}

impl<T> ExternalMessage for ExternalEvent<T> where T: Debug + Clone + Serialize + Send {}

pub trait OperationValue: Debug + Clone + Serialize + Send + 'static {}

impl<T> OperationValue for T where T: Debug + Clone + Serialize + Send + 'static {}

#[derive(Debug, Clone, Serialize, Deserialize, new)]
pub struct InternalOperationMessage<T: OperationValue>(pub T);

impl<T> IntoExternal for InternalOperationMessage<T>
where
    T: OperationValue,
{
    type External = ExternalOperationMessage<T>;
}

impl<T> InternalMessage for InternalOperationMessage<T>
where
    T: OperationValue,
{
    type External = ExternalOperationMessage<T>;
}

impl<T> From<InternalOperationMessage<T>> for ExternalOperationMessage<T>
where
    T: OperationValue,
{
    fn from(m: InternalOperationMessage<T>) -> Self {
        ExternalOperationMessage(Uuid::new_v4(), m.0)
    }
}

impl<T: OperationValue> From<T> for InternalOperationMessage<T> {
    fn from(t: T) -> Self {
        Self::new(t)
    }
}

#[derive(Debug, Clone, RemoteMessage, Serialize, Deserialize)]
pub struct ExternalOperationMessage<T: OperationValue>(pub Uuid, pub T);

impl<T: OperationValue> ExternalMessage for ExternalOperationMessage<T> {}

#[derive(Debug, Clone, RemoteMessage, Serialize, Deserialize)]
pub struct InternalOperationResponse<T: OperationValue>(pub Uuid, pub T);

impl<T: OperationValue> IntoExternal for InternalOperationResponse<T> {
    type External = ExternalOperationResponse<T>;
}

impl<T: OperationValue> InternalMessage for InternalOperationResponse<T> {
    type External = ExternalOperationResponse<T>;
}

impl<T: OperationValue> From<InternalOperationResponse<T>> for ExternalOperationResponse<T> {
    fn from(r: InternalOperationResponse<T>) -> Self {
        ExternalOperationResponse(r.0, r.1)
    }
}

impl<T: OperationValue> From<(Uuid, T)> for InternalOperationResponse<T> {
    fn from((id, t): (Uuid, T)) -> Self {
        Self(id, t)
    }
}

#[derive(Debug, Clone, RemoteMessage, Serialize, Deserialize)]
pub struct ExternalOperationResponse<T: OperationValue>(pub Uuid, pub T);

impl<T: OperationValue> ExternalMessage for ExternalOperationResponse<T> {}

impl<T: OperationValue> Identity for ExternalOperationResponse<T> {
    type Id = Uuid;
}
impl<T: OperationValue> RefIdentity for ExternalOperationResponse<T> {
    fn id_ref(&self) -> &Self::Id {
        &self.0
    }
}
impl<T: OperationValue> GetIdentity for ExternalOperationResponse<T> {
    fn id(&self) -> Self::Id {
        self.0
    }
}

impl<T: OperationValue> ExternalResponse<T> for ExternalOperationResponse<T> {
    fn into_inner(self) -> T {
        self.1
    }
}

pub trait RemoteService: RemoteActor + ClusterListener + Handler<ClusterLog> {
    fn id() -> String;
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember>;
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember>;
    fn handle_cluster_log(&mut self, msg: ClusterLog, _ctx: &mut Self::Context, actor_id: String) {
        log::info!("Cluster log message");
        match msg {
            ClusterLog::NewMember(ip, mut remote_addr) => {
                remote_addr.change_id(actor_id);
                log::info!("Telepathy member {ip} connected");
                remote_addr.do_send(InitMessage::new(
                    self.addr(),
                    Self::node_class(),
                    Self::id(),
                ));
                self.members_mut().insert(
                    remote_addr.clone(),
                    RemoteMember::new(ip, remote_addr, None),
                );
            }
            ClusterLog::MemberLeft(ip) => {
                log::info!("Telepathy member {ip} disconnected");
                let keys: Vec<_> = self
                    .members()
                    .keys()
                    .filter(|addr| addr.socket_addr == ip)
                    .cloned()
                    .collect();
                let members = self.members_mut();
                keys.iter().for_each(|k| {
                    members.remove(k);
                });
            }
        }
    }
    fn handle_init_message(&mut self, msg: InitMessage) {
        let InitMessage { addr, class, id } = msg;
        let remote_actor_id = Self::remote_actor_id().to_string();
        let members: Vec<RemoteAddr> = self
            .members()
            .iter()
            .filter(|(k, _)| k.socket_addr == addr)
            .map(|(k, _)| k.clone())
            .collect();
        members
            .iter()
            .for_each(|addr| match (self.members_mut().get_mut(addr), &id) {
                (_, id) if *id != remote_actor_id => {
                    log::info!("Init message from actor with different id")
                }
                (Some(member), _) => {
                    log::info!(
                        "Init message from {}. Class is {:?}",
                        member.socket_addr,
                        class
                    );
                    member.class = Some(class);
                }
                (None, _) => log::warn!("Init message from unknown node {addr:?} "),
            });
    }
    fn broadcast<W, M>(
        members: &HashMap<RemoteAddr, RemoteMember>,
        msg: W,
        recipient: BroadcastRecipient,
    ) where
        W: IntoExternal<External = M>,
        M: ExternalMessage + Clone + Debug,
    {
        #[allow(non_upper_case_globals)]
        const filter_by_class: &dyn Fn(&NodeClass, &RemoteMember) -> bool =
            &|class, m| m.class.as_ref().map(|c| *c == *class).unwrap_or(false);

        #[allow(non_upper_case_globals)]
        const filter_by_classes: &dyn Fn(&Vec<NodeClass>, &RemoteMember) -> bool = &|classes, m| {
            m.class
                .as_ref()
                .map(|c| classes.contains(c))
                .unwrap_or(false)
        };
        use BroadcastRecipient::*;
        let members: Vec<_> = match recipient {
            Class(c) => members
                .values()
                .filter(|m| filter_by_class(&c, m))
                .collect(),
            Classes(v) => members
                .values()
                .filter(|m| filter_by_classes(&v, m))
                .collect(),
            All => members.values().collect(),
        };
        let msg = msg.into();
        log::debug!("Sending {msg:?}");
        members.iter().for_each(|m| m.addr.do_send(msg.clone()))
    }
    fn addr(&self) -> SocketAddr;
    fn node_class() -> NodeClass;
    fn remote_actor_id() -> &'static str;
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, new)]
pub struct RemoteMember {
    socket_addr: SocketAddr,
    addr: RemoteAddr,
    class: Option<NodeClass>,
}

pub enum BroadcastRecipient {
    All,
    Class(NodeClass),
    Classes(Vec<NodeClass>),
}
