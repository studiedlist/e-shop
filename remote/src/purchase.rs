use super::prelude::*;
use actix::Addr;
use derive_new::new;
use std::any::{Any, TypeId};
use std::time::Duration;
use tokio::sync::oneshot::Sender;
use tokio::time::timeout;

use actix_telepathy::ResponseSubscribe;
use e_shop_core::cart::model::Purchase;

pub mod messages {
    use super::*;
    use e_shop_core::cart::messages::purchase::*;

    type Result<T> = std::result::Result<T, ServiceError>;

    messages_v2! {
        Get | GetResponse -> Result<Option<Purchase>> {
            request: InternalGetMessage | ExternalGetMessage,
            response: InternalGetResponse | ExternalGetResponse,
        }
        Add | AddResponse -> Result<()> {
            request: InternalAddMessage | ExternalAddMessage,
            response: InternalAddResponse | ExternalAddResponse,
        }
        ListByUserId | ListByUserIdResponse -> Result<Vec<Purchase>> {
            request: InternalListByUserIdMessage | ExternalListByUserIdMessage,
            response: InternalListByUserIdResponse | ExternalListByUserIdResponse,
        }
    }
}

use messages::*;

pub trait RequestHandler:
    Actor<Context = Context<Self>>
    + Handler<ExternalGetMessage>
    + Handler<ExternalAddMessage>
    + Handler<ExternalListByUserIdMessage>
{
}

impl<T> RequestHandler for T where
    T: Actor<Context = Context<Self>>
        + Handler<ExternalGetMessage>
        + Handler<ExternalAddMessage>
        + Handler<ExternalListByUserIdMessage>
{
}

pub trait ExternalRequest: ExternalMessage + Send + Sync + 'static {
    type InternalResponse: InternalMessage;
}

impl ExternalRequest for ExternalGetMessage {
    type InternalResponse = InternalGetResponse;
}

impl ExternalRequest for ExternalAddMessage {
    type InternalResponse = InternalAddResponse;
}

impl ExternalRequest for ExternalListByUserIdMessage {
    type InternalResponse = InternalListByUserIdResponse;
}

pub trait ExternalResponse: Message<Result = ()> + Send + Sync + 'static {}

impl ExternalResponse for ExternalGetResponse {}
impl ExternalResponse for ExternalAddResponse {}
impl ExternalResponse for ExternalListByUserIdResponse {}

#[derive(RemoteActor)]
#[remote_messages(
    ExternalGetMessage,
    ExternalAddMessage,
    ExternalListByUserIdMessage,
    InitMessage
)]
#[derive(new)]
pub struct RemotePurchaseRequestService<H: RequestHandler> {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    handler: Arc<Addr<H>>,
    addr: SocketAddr,
}

impl<H: RequestHandler> RemoteService for RemotePurchaseRequestService<H> {
    fn id() -> String {
        "RemotePurchaseRequestService".to_string()
    }
    fn remote_actor_id() -> &'static str {
        "RemotePurchaseResponseService"
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Backend
    }
}

impl<H: RequestHandler> Actor for RemotePurchaseRequestService<H> {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
        self.subscribe_system_async::<InternalGetResponse>(ctx);
        self.subscribe_system_async::<InternalAddResponse>(ctx);
        self.subscribe_system_async::<InternalListByUserIdResponse>(ctx);
    }
}

impl<H, M> Handler<M> for RemotePurchaseRequestService<H>
where
    H: RequestHandler + Handler<M>,
    M: ExternalRequest + Message,
    M::InternalResponse: Message,
    Self: Handler<M::InternalResponse>,
{
    type Result = ();

    fn handle(&mut self, msg: M, _: &mut Self::Context) -> Self::Result {
        log::debug!("Got external message");
        self.handler.do_send(msg);
    }
}

impl<H: RequestHandler> Handler<InternalGetResponse> for RemotePurchaseRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalGetResponse, _: &mut Self::Context) -> Self::Result {
        log::debug!("RemotePurchaseService");
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalAddResponse> for RemotePurchaseRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalAddResponse, _: &mut Self::Context) -> Self::Result {
        log::debug!("RemotePurchaseService");
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalListByUserIdResponse> for RemotePurchaseRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalListByUserIdResponse, _: &mut Self::Context) -> Self::Result {
        log::debug!("RemotePurchaseService");
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> ClusterListener for RemotePurchaseRequestService<H> {}

impl<H: RequestHandler> Handler<ClusterLog> for RemotePurchaseRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) -> Self::Result {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string());
    }
}

impl<H: RequestHandler> Handler<InitMessage> for RemotePurchaseRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}

#[derive(RemoteActor)]
#[remote_messages(
    ExternalGetResponse,
    ExternalAddResponse,
    ExternalListByUserIdResponse,
    InitMessage
)]
#[derive(new)]
pub struct RemotePurchaseResponseService {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    addr: SocketAddr,
    #[new(default)]
    response_notify: HashMap<TypeId, Sender<Box<dyn Any + Send + 'static>>>,
}

impl Actor for RemotePurchaseResponseService {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
    }
}

impl RemoteService for RemotePurchaseResponseService {
    fn id() -> String {
        "RemotePurchaseResponseService".to_string()
    }
    fn remote_actor_id() -> &'static str {
        "RemotePurchaseRequestService"
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Frontend
    }
}

impl<M> Handler<M> for RemotePurchaseResponseService
where
    M: ExternalResponse + Clone,
{
    type Result = ();

    fn handle(&mut self, msg: M, _: &mut Self::Context) -> Self::Result {
        if let Some(sender) = self.response_notify.remove(&TypeId::of::<M>()) {
            if let Err(e) = sender.send(Box::new(msg)) {
                log::error!("Failed to send {e:?} response via oneshot channel");
            }
        }
    }
}

impl Handler<ResponseSubscribe> for RemotePurchaseResponseService {
    type Result = ();

    fn handle(
        &mut self,
        ResponseSubscribe(id, tx): ResponseSubscribe,
        _: &mut Self::Context,
    ) -> Self::Result {
        self.response_notify.insert(id, tx);
    }
}

impl Handler<InitMessage> for RemotePurchaseResponseService {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) {
        self.handle_init_message(msg);
    }
}

impl Handler<InternalGetMessage> for RemotePurchaseResponseService {
    type Result = ResponseFuture<Result<Option<Purchase>, actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalGetMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get message {:?}", msg.0);
            for (m, _) in members {
                log::debug!("sent get message");
                let r = m.send::<_, ExternalGetResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalGetResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalGetResponse"),
                }
            }
            Ok(None)
        })
    }
}

impl Handler<InternalAddMessage> for RemotePurchaseResponseService {
    type Result = ResponseFuture<Result<(), actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalAddMessage, ctx: &mut Self::Context) -> Self::Result {
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get message {:?}", msg.0);
            for (m, _) in members {
                log::debug!("sent get message");
                let r = m.send::<_, ExternalAddResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalAddResponse>() {
                    Ok(_) => return Ok(()),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalGetResponse"),
                }
            }
            Ok(())
        })
    }
}

impl Handler<InternalListByUserIdMessage> for RemotePurchaseResponseService {
    type Result = ResponseFuture<Result<Vec<Purchase>, actix_service::ServiceError>>;

    fn handle(
        &mut self,
        msg: InternalListByUserIdMessage,
        ctx: &mut Self::Context,
    ) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get message {:?}", msg.0);
            for (m, _) in members {
                log::debug!("sent get message");
                let r = m.send::<_, ExternalListByUserIdResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalListByUserIdResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalGetResponse"),
                }
            }
            Ok(vec![])
        })
    }
}

impl ClusterListener for RemotePurchaseResponseService {}

impl Handler<ClusterLog> for RemotePurchaseResponseService {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string())
    }
}
