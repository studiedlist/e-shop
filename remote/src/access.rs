use super::prelude::*;
use actix::Addr;
use derive_new::new;
use e_shop_core::access::{
    messages::*,
    model::{Role, UserCredentials},
};
use std::any::{Any, TypeId};
use std::time::Duration;
use tokio::sync::oneshot::Sender;
use tokio::time::timeout;

use actix_telepathy::ResponseSubscribe;

pub mod messages {
    use super::*;

    type Result<T> = std::result::Result<T, ServiceError>;

    messages_v2! {
        Get | GetResponse -> Result<Option<UserCredentials>> {
            request: InternalGetMessage | ExternalGetMessage,
            response: InternalGetResponse | ExternalGetResponse,
        }
        GetByLogin | GetByLoginResponse -> Result<Option<UserCredentials>> {
            request: InternalGetByLoginMessage | ExternalGetByLoginMessage,
            response: InternalGetByLoginResponse | ExternalGetByLoginResponse,
        }
        Add | AddResponse -> Result<()> {
            request: InternalAddMessage | ExternalAddMessage,
            response: InternalAddResponse | ExternalAddResponse,
        }
        GetRoleByLogin | GetRoleByLoginResponse -> Result<Option<Role>> {
            request: InternalGetRoleByLoginMessage | ExternalGetRoleByLoginMessage,
            response: InternalGetRoleByLoginResponse | ExternalGetRoleByLoginResponse,
        }
    }
}

use messages::*;

pub trait RequestHandler:
    Actor<Context = Context<Self>>
    + Handler<ExternalGetMessage, Result = ResponseFuture<()>>
    + Handler<ExternalGetByLoginMessage, Result = ResponseFuture<()>>
    + Handler<ExternalAddMessage, Result = ResponseFuture<()>>
    + Handler<ExternalGetRoleByLoginMessage, Result = ResponseFuture<()>>
{
}

impl<T> RequestHandler for T where
    T: Actor<Context = Context<Self>>
        + Handler<ExternalGetMessage, Result = ResponseFuture<()>>
        + Handler<ExternalGetByLoginMessage, Result = ResponseFuture<()>>
        + Handler<ExternalAddMessage, Result = ResponseFuture<()>>
        + Handler<ExternalGetRoleByLoginMessage, Result = ResponseFuture<()>>
{
}

pub trait ResponseHandler:
    Actor<Context = Context<Self>>
    + Handler<ExternalGetResponse, Result = ResponseFuture<()>>
    + Handler<ExternalGetByLoginResponse, Result = ResponseFuture<()>>
    + Handler<ExternalAddResponse, Result = ResponseFuture<()>>
    + Handler<ExternalGetRoleByLoginResponse, Result = ResponseFuture<()>>
{
}

impl<T> ResponseHandler for T where
    T: Actor<Context = Context<Self>>
        + Handler<ExternalGetResponse, Result = ResponseFuture<()>>
        + Handler<ExternalGetByLoginResponse, Result = ResponseFuture<()>>
        + Handler<ExternalAddResponse, Result = ResponseFuture<()>>
        + Handler<ExternalGetRoleByLoginResponse, Result = ResponseFuture<()>>
{
}

pub trait ExternalRequest: ExternalMessage + Send + Sync + 'static {
    type Internal: InternalMessage;
}

impl ExternalRequest for ExternalGetMessage {
    type Internal = InternalGetResponse;
}

impl ExternalRequest for ExternalGetByLoginMessage {
    type Internal = InternalGetByLoginResponse;
}

impl ExternalRequest for ExternalAddMessage {
    type Internal = InternalAddResponse;
}

impl ExternalRequest for ExternalGetRoleByLoginMessage {
    type Internal = InternalGetRoleByLoginResponse;
}

pub trait ExternalResponse: Message<Result = ()> + Send + Sync + 'static {}

impl ExternalResponse for ExternalGetResponse {}
impl ExternalResponse for ExternalGetByLoginResponse {}
impl ExternalResponse for ExternalAddResponse {}
impl ExternalResponse for ExternalGetRoleByLoginResponse {}

#[derive(RemoteActor)]
#[remote_messages(
    ExternalGetMessage,
    ExternalGetByLoginMessage,
    ExternalAddMessage,
    ExternalGetRoleByLoginMessage,
    InitMessage
)]
#[derive(new)]
pub struct RemoteUserCredentialsRequestService<H: RequestHandler> {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    handler: Arc<Addr<H>>,
    addr: SocketAddr,
}

impl<H: RequestHandler> RemoteService for RemoteUserCredentialsRequestService<H> {
    fn id() -> String {
        "RemoteUserCredentialsRequestService".to_string()
    }
    fn remote_actor_id() -> &'static str {
        "RemoteUserCredentialsResponseService"
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Backend
    }
}

impl<H: RequestHandler> Actor for RemoteUserCredentialsRequestService<H> {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
        self.subscribe_system_async::<InternalGetResponse>(ctx);
        self.subscribe_system_async::<InternalGetByLoginResponse>(ctx);
        self.subscribe_system_async::<InternalAddResponse>(ctx);
        self.subscribe_system_async::<InternalGetRoleByLoginResponse>(ctx);
    }
}

impl<H: RequestHandler> ClusterListener for RemoteUserCredentialsRequestService<H> {}

impl<H: RequestHandler> Handler<ClusterLog> for RemoteUserCredentialsRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string())
    }
}

impl<H, M> Handler<M> for RemoteUserCredentialsRequestService<H>
where
    H: RequestHandler + Handler<M>,
    M: ExternalRequest + Message<Result = ()>,
    M::Internal: Message,
    Self: Handler<M::Internal>,
{
    type Result = ();

    fn handle(&mut self, msg: M, _: &mut Self::Context) -> Self::Result {
        log::debug!("Got external message");
        self.handler.do_send(msg);
    }
}

impl<H: RequestHandler> Handler<InternalGetResponse> for RemoteUserCredentialsRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalGetResponse, _: &mut Self::Context) {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H> Handler<InternalGetByLoginResponse> for RemoteUserCredentialsRequestService<H>
where
    H: RequestHandler,
{
    type Result = ();

    fn handle(&mut self, msg: InternalGetByLoginResponse, _: &mut Self::Context) {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalAddResponse> for RemoteUserCredentialsRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalAddResponse, _: &mut Self::Context) {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H> Handler<InternalGetRoleByLoginResponse> for RemoteUserCredentialsRequestService<H>
where
    H: RequestHandler,
{
    type Result = ();

    fn handle(&mut self, msg: InternalGetRoleByLoginResponse, _: &mut Self::Context) {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

#[derive(RemoteActor)]
#[remote_messages(
    ExternalGetResponse,
    ExternalGetByLoginResponse,
    ExternalAddResponse,
    ExternalGetRoleByLoginResponse,
    InitMessage
)]
#[derive(new)]
pub struct RemoteUserCredentialsResponseService<H: ResponseHandler> {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    handler: Arc<Addr<H>>,
    addr: SocketAddr,
    #[new(default)]
    response_notify: HashMap<TypeId, Sender<Box<dyn Any + Send + 'static>>>,
}

impl<H: ResponseHandler> Actor for RemoteUserCredentialsResponseService<H> {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
    }
}

impl<H: ResponseHandler> RemoteService for RemoteUserCredentialsResponseService<H> {
    fn id() -> String {
        "RemoteUserCredentialsResponseService".to_string()
    }
    fn remote_actor_id() -> &'static str {
        "RemoteUserCredentialsRequestService"
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Frontend
    }
}

impl<H, M> Handler<M> for RemoteUserCredentialsResponseService<H>
where
    H: ResponseHandler + Handler<M>,
    M: ExternalResponse + Clone,
{
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: M, _: &mut Self::Context) -> Self::Result {
        if let Some(sender) = self.response_notify.remove(&TypeId::of::<M>()) {
            if let Err(e) = sender.send(Box::new(msg.clone())) {
                log::error!("Failed to send {e:?} response via oneshot channel");
            }
        }
        let handler = self.handler.clone();
        Box::pin(async move { handler.send(msg).await.unwrap() })
    }
}

impl<H: ResponseHandler> ClusterListener for RemoteUserCredentialsResponseService<H> {}

impl<H: ResponseHandler> Handler<ClusterLog> for RemoteUserCredentialsResponseService<H> {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) -> Self::Result {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string())
    }
}

impl<H: RequestHandler> Handler<InitMessage> for RemoteUserCredentialsRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}

impl<H: ResponseHandler> Handler<InitMessage> for RemoteUserCredentialsResponseService<H> {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}

impl<H> Handler<ResponseSubscribe> for RemoteUserCredentialsResponseService<H>
where
    H: ResponseHandler,
{
    type Result = ();

    fn handle(&mut self, ResponseSubscribe(id, tx): ResponseSubscribe, _: &mut Self::Context) {
        self.response_notify.insert(id, tx);
    }
}

impl<H: ResponseHandler> Handler<InternalGetMessage> for RemoteUserCredentialsResponseService<H> {
    type Result = ResponseFuture<Result<Option<UserCredentials>, ServiceError>>;

    fn handle(&mut self, msg: InternalGetMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get message");
            for (m, _) in members {
                log::debug!("sent get message");
                let r = m.send::<_, ExternalGetResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r).await.unwrap().unwrap();
                match res.downcast::<ExternalGetResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalListResponse"),
                }
            }
            Ok(None)
        })
    }
}

impl<H> Handler<InternalGetByLoginMessage> for RemoteUserCredentialsResponseService<H>
where
    H: ResponseHandler,
{
    type Result = ResponseFuture<Result<Option<UserCredentials>, ServiceError>>;

    fn handle(&mut self, msg: InternalGetByLoginMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get by login message");
            for (m, _) in members {
                log::debug!("sent get by login message");
                let r = m.send::<_, ExternalGetByLoginResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r).await.unwrap().unwrap();
                match res.downcast::<ExternalGetByLoginResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalListResponse"),
                }
            }
            Ok(None)
        })
    }
}

impl<H: ResponseHandler> Handler<InternalAddMessage> for RemoteUserCredentialsResponseService<H> {
    type Result = ResponseFuture<Result<(), ServiceError>>;

    fn handle(&mut self, msg: InternalAddMessage, ctx: &mut Self::Context) -> Self::Result {
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending add message");
            for (m, _) in members {
                log::debug!("sent add message");
                let r = m.send::<_, ExternalAddResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r).await.unwrap().unwrap();
                match res.downcast::<ExternalAddResponse>() {
                    Ok(_) => return Ok(()),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalListResponse"),
                }
            }
            Ok(())
        })
    }
}

impl<H: ResponseHandler> Handler<InternalGetRoleByLoginMessage>
    for RemoteUserCredentialsResponseService<H>
{
    type Result = ResponseFuture<Result<Option<Role>, ServiceError>>;

    fn handle(
        &mut self,
        msg: InternalGetRoleByLoginMessage,
        ctx: &mut Self::Context,
    ) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending add message");
            for (m, _) in members {
                log::debug!("sent add message");
                let r = m.send::<_, ExternalGetRoleByLoginResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r).await.unwrap().unwrap();
                match res.downcast::<ExternalGetRoleByLoginResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => {
                        log::error!("Unable to downcast dyn Any to ExternalGetRoleByLoginResponse")
                    }
                }
            }
            Err(ServiceError::NotFound)
        })
    }
}
