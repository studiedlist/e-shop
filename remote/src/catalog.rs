use super::prelude::*;
use actix::Addr;
use actix_telepathy::ResponseSubscribe;
use derive_new::new;
use e_shop_core::catalog::{
    messages::{
        Add, AddResponse, CategoryEvent, Get, GetResponse, List, ListResponse, Remove,
        RemoveResponse, Update, UpdateResponse,
    },
    model::Category,
};
use std::any::{Any, TypeId};
use std::time::Duration;
use tokio::sync::oneshot::Sender;
use tokio::time::timeout;

pub type InternalCategoryEvent = InternalEvent<CategoryEvent>;
pub type ExternalCategoryEvent = ExternalEvent<CategoryEvent>;

pub mod messages {
    use super::*;

    type Result<T> = std::result::Result<T, ServiceError>;
    messages_v2! {
        Get | GetResponse -> Result<Option<Category>> {
            request: InternalGetMessage | ExternalGetMessage,
            response: InternalGetResponse | ExternalGetResponse,
        }
        List | ListResponse -> Result<Vec<Category>> {
            request: InternalListMessage | ExternalListMessage,
            response: InternalListResponse | ExternalListResponse,
        }
        Add | AddResponse -> Result<()> {
            request: InternalAddMessage | ExternalAddMessage,
            response: InternalAddResponse | ExternalAddResponse,
        }
        Update | UpdateResponse -> Result<()> {
            request: InternalUpdateMessage | ExternalUpdateMessage,
            response: InternalUpdateResponse | ExternalUpdateResponse,
        }
        Remove | RemoveResponse -> Result<()> {
            request: InternalRemoveMessage | ExternalRemoveMessage,
            response: InternalRemoveResponse | ExternalRemoveResponse,
        }
    }
}

use messages::*;

pub trait RequestHandler:
    Actor<Context = Context<Self>>
    + Handler<ExternalGetMessage, Result = ResponseFuture<()>>
    + Handler<ExternalListMessage, Result = ResponseFuture<()>>
    + Handler<ExternalAddMessage, Result = ResponseFuture<()>>
    + Handler<ExternalUpdateMessage, Result = ResponseFuture<()>>
    + Handler<ExternalRemoveMessage, Result = ResponseFuture<()>>
{
}

impl<T> RequestHandler for T where
    T: Actor<Context = Context<Self>>
        + Handler<ExternalGetMessage, Result = ResponseFuture<()>>
        + Handler<ExternalListMessage, Result = ResponseFuture<()>>
        + Handler<ExternalAddMessage, Result = ResponseFuture<()>>
        + Handler<ExternalUpdateMessage, Result = ResponseFuture<()>>
        + Handler<ExternalRemoveMessage, Result = ResponseFuture<()>>
{
}

trait ExternalRequest: ExternalMessage + Send + Sync + 'static {
    type Response: Message;
}

impl ExternalRequest for ExternalGetMessage {
    type Response = InternalGetResponse;
}
impl ExternalRequest for ExternalListMessage {
    type Response = InternalListResponse;
}
impl ExternalRequest for ExternalUpdateMessage {
    type Response = InternalUpdateResponse;
}
impl ExternalRequest for ExternalAddMessage {
    type Response = InternalAddResponse;
}
impl ExternalRequest for ExternalRemoveMessage {
    type Response = InternalRemoveResponse;
}

pub trait ResponseHandler:
    Actor<Context = Context<Self>>
    + Handler<ExternalGetResponse, Result = ResponseFuture<()>>
    + Handler<ExternalListResponse, Result = ResponseFuture<()>>
    + Handler<ExternalUpdateResponse, Result = ResponseFuture<()>>
    + Handler<ExternalAddResponse, Result = ResponseFuture<()>>
    + Handler<ExternalRemoveResponse, Result = ResponseFuture<()>>
{
}

impl<T> ResponseHandler for T where
    T: Actor<Context = Context<Self>>
        + Handler<ExternalGetResponse, Result = ResponseFuture<()>>
        + Handler<ExternalListResponse, Result = ResponseFuture<()>>
        + Handler<ExternalUpdateResponse, Result = ResponseFuture<()>>
        + Handler<ExternalAddResponse, Result = ResponseFuture<()>>
        + Handler<ExternalRemoveResponse, Result = ResponseFuture<()>>
{
}

pub trait ExternalResponse: Message<Result = ()> + Send + Sync + 'static {}

impl ExternalResponse for ExternalGetResponse {}
impl ExternalResponse for ExternalListResponse {}
impl ExternalResponse for ExternalUpdateResponse {}
impl ExternalResponse for ExternalAddResponse {}
impl ExternalResponse for ExternalRemoveResponse {}

pub trait Callback
where
    Self: Unpin + 'static,
{
}

#[derive(RemoteActor)]
#[remote_messages(
    ExternalGetMessage,
    ExternalListMessage,
    ExternalUpdateMessage,
    ExternalAddMessage,
    ExternalRemoveMessage,
    InitMessage
)]
#[derive(new)]
pub struct RemoteCategoryRequestService<H: RequestHandler> {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    handler: Arc<Addr<H>>,
    addr: SocketAddr,
}

impl<H: RequestHandler> RemoteService for RemoteCategoryRequestService<H> {
    fn id() -> String {
        "RemoteCategoryRequestService".to_string()
    }
    fn remote_actor_id() -> &'static str {
        "RemoteCategoryResponseService"
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Backend
    }
}

impl<H: RequestHandler> Actor for RemoteCategoryRequestService<H> {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
        self.subscribe_system_async::<InternalListResponse>(ctx);
        self.subscribe_system_async::<InternalGetResponse>(ctx);
    }
}

impl<H: RequestHandler> ClusterListener for RemoteCategoryRequestService<H> {}

impl<H: RequestHandler> Handler<ClusterLog> for RemoteCategoryRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) -> Self::Result {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string());
    }
}

impl<H, M> Handler<M> for RemoteCategoryRequestService<H>
where
    H: RequestHandler,
    M: ExternalRequest,
    H: Handler<M>,
    Self: Handler<M::Response>,
{
    type Result = ();

    fn handle(&mut self, msg: M, _: &mut Self::Context) -> Self::Result {
        log::debug!("Got external response");
        self.handler.do_send(msg);
    }
}

impl<H: RequestHandler> Handler<InternalGetResponse> for RemoteCategoryRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalGetResponse, _: &mut Self::Context) -> Self::Result {
        log::debug!("Sending get response");
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalListResponse> for RemoteCategoryRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalListResponse, _: &mut Self::Context) -> Self::Result {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalUpdateResponse> for RemoteCategoryRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalUpdateResponse, _: &mut Self::Context) -> Self::Result {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalAddResponse> for RemoteCategoryRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalAddResponse, _: &mut Self::Context) -> Self::Result {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalRemoveResponse> for RemoteCategoryRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalRemoveResponse, _: &mut Self::Context) -> Self::Result {
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InitMessage> for RemoteCategoryRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}

impl<H: ResponseHandler> Handler<InternalGetMessage> for RemoteCategoryResponseService<H> {
    type Result = ResponseFuture<Result<Option<Category>, actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalGetMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get message {:?}", msg.0);
            for (m, _) in members {
                log::debug!("sent get message");
                let r = m.send::<_, ExternalGetResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalGetResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalGetResponse"),
                }
            }
            Ok(None)
        })
    }
}

impl<H: ResponseHandler> Handler<InternalListMessage> for RemoteCategoryResponseService<H> {
    type Result = ResponseFuture<Result<Vec<Category>, actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalListMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending list message");
            for (m, _) in members {
                log::debug!("sent list message");
                let r = m.send::<_, ExternalListResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalListResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalListResponse"),
                }
            }
            Ok(vec![])
        })
    }
}

impl<H: ResponseHandler> Handler<InternalUpdateMessage> for RemoteCategoryResponseService<H> {
    type Result = ResponseFuture<Result<(), actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalUpdateMessage, ctx: &mut Self::Context) -> Self::Result {
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending update message");
            for (m, _) in members {
                log::debug!("sent update message");
                let r = m.send::<_, ExternalUpdateResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalUpdateResponse>() {
                    Ok(_) => return Ok(()),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalUpdateResponse"),
                }
            }
            Ok(())
        })
    }
}

impl<H: ResponseHandler> Handler<InternalAddMessage> for RemoteCategoryResponseService<H> {
    type Result = ResponseFuture<Result<(), actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalAddMessage, ctx: &mut Self::Context) -> Self::Result {
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending add message");
            for (m, _) in members {
                log::debug!("sent add message");
                let r = m.send::<_, ExternalAddResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalAddResponse>() {
                    Ok(_) => return Ok(()),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalAddResponse"),
                }
            }
            Ok(())
        })
    }
}

impl<H: ResponseHandler> Handler<InternalRemoveMessage> for RemoteCategoryResponseService<H> {
    type Result = ResponseFuture<Result<(), actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalRemoveMessage, ctx: &mut Self::Context) -> Self::Result {
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending remove message");
            for (m, _) in members {
                log::debug!("sent remove message");
                let r = m.send::<_, ExternalRemoveResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalRemoveResponse>() {
                    Ok(_) => return Ok(()),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalAddResponse"),
                }
            }
            Ok(())
        })
    }
}

#[derive(RemoteActor)]
#[remote_messages(
    ExternalGetResponse,
    ExternalListResponse,
    ExternalUpdateResponse,
    ExternalAddResponse,
    ExternalRemoveResponse,
    InitMessage
)]
#[derive(new)]
pub struct RemoteCategoryResponseService<H: ResponseHandler> {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    handler: Arc<Addr<H>>,
    addr: SocketAddr,
    #[new(default)]
    response_notify: HashMap<TypeId, Sender<Box<dyn Any + Send + 'static>>>,
}

impl<H: ResponseHandler> Actor for RemoteCategoryResponseService<H> {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
    }
}

impl<H: ResponseHandler> RemoteService for RemoteCategoryResponseService<H> {
    fn id() -> String {
        "RemoteCategoryResponseService".to_string()
    }
    fn remote_actor_id() -> &'static str {
        "RemoteCategoryRequestService"
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Frontend
    }
}

impl<H: ResponseHandler> ClusterListener for RemoteCategoryResponseService<H> {}

impl<H: ResponseHandler> Handler<ClusterLog> for RemoteCategoryResponseService<H> {
    type Result = ();

    fn handle(&mut self, m: ClusterLog, ctx: &mut Self::Context) {
        self.handle_cluster_log(m, ctx, Self::remote_actor_id().to_string());
    }
}

impl<H, M> Handler<M> for RemoteCategoryResponseService<H>
where
    H: ResponseHandler + Handler<M>,
    M: ExternalResponse + Clone,
{
    type Result = ResponseFuture<()>;

    fn handle(&mut self, msg: M, _: &mut Self::Context) -> Self::Result {
        log::debug!("Got external response");
        if let Some(sender) = self.response_notify.remove(&TypeId::of::<M>()) {
            if let Err(e) = sender.send(Box::new(msg.clone())) {
                log::error!("Failed to send {e:?} response via oneshot channel");
            }
        }
        let handler = self.handler.clone();
        Box::pin(async move { handler.send(msg).await.unwrap() })
    }
}

impl<H: ResponseHandler> Handler<ResponseSubscribe> for RemoteCategoryResponseService<H> {
    type Result = ();

    fn handle(
        &mut self,
        ResponseSubscribe(id, tx): ResponseSubscribe,
        _: &mut Self::Context,
    ) -> Self::Result {
        self.response_notify.insert(id, tx);
    }
}

impl<H: ResponseHandler> Handler<InitMessage> for RemoteCategoryResponseService<H> {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}
