use super::prelude::*;
use actix::Addr;
use derive_new::new;
use std::any::{Any, TypeId};
use std::time::Duration;
use tokio::sync::oneshot::Sender;
use tokio::time::timeout;

use e_shop_core::cart::model::Cart;

use actix_telepathy::ResponseSubscribe;

pub mod messages {
    use super::*;
    use e_shop_core::cart::messages::cart::*;

    type Result<T> = std::result::Result<T, ServiceError>;

    messages_v2! {
        Get | GetResponse -> Result<Option<Cart>> {
            request: InternalGetMessage | ExternalGetMessage,
            response: InternalGetResponse | ExternalGetResponse,
        }
        Add | AddResponse -> Result<()> {
            request: InternalAddMessage | ExternalAddMessage,
            response: InternalAddResponse | ExternalAddResponse,
        }
    }
}

use messages::*;

pub trait RequestHandler:
    Actor<Context = Context<Self>> + Handler<ExternalGetMessage> + Handler<ExternalAddMessage>
{
}

impl<T> RequestHandler for T where
    T: Actor<Context = Context<Self>> + Handler<ExternalGetMessage> + Handler<ExternalAddMessage>
{
}

pub trait ExternalRequest: ExternalMessage + Send + Sync + 'static {
    type InternalResponse: InternalMessage;
}

impl ExternalRequest for ExternalGetMessage {
    type InternalResponse = InternalGetResponse;
}

impl ExternalRequest for ExternalAddMessage {
    type InternalResponse = InternalAddResponse;
}

pub trait ExternalResponse: Message<Result = ()> + Send + Sync + 'static {}

impl ExternalResponse for ExternalGetResponse {}
impl ExternalResponse for ExternalAddResponse {}

#[derive(RemoteActor)]
#[remote_messages(ExternalGetMessage, ExternalAddMessage, InitMessage)]
#[derive(new)]
pub struct RemoteCartRequestService<H: RequestHandler> {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    handler: Arc<Addr<H>>,
    addr: SocketAddr,
}

impl<H: RequestHandler> RemoteService for RemoteCartRequestService<H> {
    fn id() -> String {
        "RemoteCartRequestService".to_string()
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Backend
    }
    fn remote_actor_id() -> &'static str {
        "RemoteCartResponseService"
    }
}

impl<H: RequestHandler> Actor for RemoteCartRequestService<H> {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
        self.subscribe_system_async::<InternalGetResponse>(ctx);
        self.subscribe_system_async::<InternalAddResponse>(ctx);
    }
}

impl<H, M> Handler<M> for RemoteCartRequestService<H>
where
    H: RequestHandler + Handler<M>,
    M: ExternalRequest + Message,
    M::InternalResponse: Message,
    Self: Handler<M::InternalResponse>,
{
    type Result = ();

    fn handle(&mut self, msg: M, _: &mut Self::Context) -> Self::Result {
        log::debug!("Got external message");
        self.handler.do_send(msg);
    }
}

impl<H: RequestHandler> Handler<InternalGetResponse> for RemoteCartRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalGetResponse, _: &mut Self::Context) -> Self::Result {
        log::debug!("RemoteCartService");
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> Handler<InternalAddResponse> for RemoteCartRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InternalAddResponse, _: &mut Self::Context) -> Self::Result {
        log::debug!("RemoteCartService");
        Self::broadcast(&self.members, msg, BroadcastRecipient::All);
    }
}

impl<H: RequestHandler> ClusterListener for RemoteCartRequestService<H> {}

impl<H: RequestHandler> Handler<ClusterLog> for RemoteCartRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) -> Self::Result {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string());
    }
}

impl<H: RequestHandler> Handler<InitMessage> for RemoteCartRequestService<H> {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}

impl Handler<InitMessage> for RemoteCartResponseService {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        self.handle_init_message(msg);
    }
}

#[derive(RemoteActor)]
#[remote_messages(ExternalGetResponse, ExternalAddResponse, InitMessage)]
#[derive(new)]
pub struct RemoteCartResponseService {
    #[new(default)]
    members: HashMap<RemoteAddr, RemoteMember>,
    addr: SocketAddr,
    #[new(default)]
    response_notify: HashMap<TypeId, Sender<Box<dyn Any + Send + 'static>>>,
}

impl Actor for RemoteCartResponseService {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        log::debug!("Started");
        self.register(ctx.address().recipient());
        self.subscribe_system_async::<ClusterLog>(ctx);
    }
}

impl RemoteService for RemoteCartResponseService {
    fn id() -> String {
        "RemoteCartResponseService".to_string()
    }
    fn members(&self) -> &HashMap<RemoteAddr, RemoteMember> {
        &self.members
    }
    fn members_mut(&mut self) -> &mut HashMap<RemoteAddr, RemoteMember> {
        &mut self.members
    }
    fn addr(&self) -> SocketAddr {
        self.addr
    }
    fn node_class() -> NodeClass {
        NodeClass::Frontend
    }
    fn remote_actor_id() -> &'static str {
        "RemoteCartRequestService"
    }
}

impl<M> Handler<M> for RemoteCartResponseService
where
    M: ExternalResponse + Clone,
{
    type Result = ();

    fn handle(&mut self, msg: M, _: &mut Self::Context) -> Self::Result {
        if let Some(sender) = self.response_notify.remove(&TypeId::of::<M>()) {
            if let Err(e) = sender.send(Box::new(msg)) {
                log::error!("Failed to send {e:?} response via oneshot channel");
            }
        }
    }
}

impl ClusterListener for RemoteCartResponseService {}

impl Handler<ClusterLog> for RemoteCartResponseService {
    type Result = ();

    fn handle(&mut self, msg: ClusterLog, ctx: &mut Self::Context) -> Self::Result {
        self.handle_cluster_log(msg, ctx, Self::remote_actor_id().to_string())
    }
}

impl Handler<ResponseSubscribe> for RemoteCartResponseService {
    type Result = ();

    fn handle(
        &mut self,
        ResponseSubscribe(id, tx): ResponseSubscribe,
        _: &mut Self::Context,
    ) -> Self::Result {
        self.response_notify.insert(id, tx);
    }
}

/*
impl Handler<InitMessage> for RemoteCartResponseService {
    type Result = ();

    fn handle(&mut self, msg: InitMessage, _: &mut Self::Context) -> Self::Result {
        let InitMessage { addr, class, id } = msg;
        let remote_actor_id = Self::remote_actor_id().to_string();
        let members: Vec<RemoteAddr> = self
            .members
            .iter()
            .filter(|(k, _)| k.socket_addr == addr)
            .map(|(k, _)| k.clone())
            .collect();
        members
            .iter()
            .for_each(|addr| match (self.members.get_mut(addr), &id) {
                (_, id) if *id != remote_actor_id => {
                    log::info!("Init message from actor with different id")
                }
                (Some(member), _) => {
                    log::info!(
                        "Init message from {}. Class is {:?}",
                        member.socket_addr,
                        class
                    );
                    member.class = Some(class);
                }
                (None, _) => log::warn!("Init message from unknown node {addr:?} "),
            });
    }
}
*/

impl Handler<InternalGetMessage> for RemoteCartResponseService {
    type Result = ResponseFuture<Result<Option<Cart>, actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalGetMessage, ctx: &mut Self::Context) -> Self::Result {
        use crate::ExternalResponse;
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get message {:?}", msg.0);
            for (m, _) in members {
                log::debug!("sent get message");
                let r = m.send::<_, ExternalGetResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalGetResponse>() {
                    Ok(res) => return Ok(res.into_inner().0),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalGetResponse"),
                }
            }
            Ok(None)
        })
    }
}

impl Handler<InternalAddMessage> for RemoteCartResponseService {
    type Result = ResponseFuture<Result<(), actix_service::ServiceError>>;

    fn handle(&mut self, msg: InternalAddMessage, ctx: &mut Self::Context) -> Self::Result {
        let msg = msg.into_external();
        let members = self.members.clone();
        let addr = ctx.address();
        Box::pin(async move {
            log::debug!("sending get message {:?}", msg.0);
            for (m, _) in members {
                log::debug!("sent get message");
                let r = m.send::<_, ExternalAddResponse, _>(msg.clone(), &addr);
                let res = timeout(Duration::from_secs(1), r)
                    .await
                    .map_err(|e| ServiceError::Other(Box::new(e)))?
                    .map_err(|e| ServiceError::Other(Box::new(e)))?;
                match res.downcast::<ExternalAddResponse>() {
                    Ok(_) => return Ok(()),
                    Err(_) => log::error!("Unable to downcast dyn Any to ExternalGetResponse"),
                }
            }
            Ok(())
        })
    }
}
